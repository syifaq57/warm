import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";
//Risk By


export default class AllUnitDashboard extends Component {
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            visibleDialogSubmit: false,
            isModalVisible: false,
            visible:true,
            

          
        };
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    render(){

        return(
            <Container style={styles.wrapper}>
                <Header
                    style={{ backgroundColor: colors.gray09, marginTop:Platform.OS === "ios" ? 0 : 20}}
                    androidStatusBarColor={colors.gray10}
                    >
                    <Left style={{flex:1, marginLeft:0}}>
                        <Button
                            transparent   
                            onPress={this.toggleModal}    
                            
                        >   
                            <Icon 
                                name="menu"
                                size={30}
                                color={colors.white}
                            />
                            
                        </Button>
                        <Modal 
                            style={dashstyles.modal}
                            animationIn={'slideInLeft'}
                            animationOut={'slideOutLeft'}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}
                            onBackButtonPress={this.toggleModal}
                            // androidStatusBarColor={colors.gray09}
                            >                            
                                <View style={{ flex: 1}}>
                                    <View style={dashstyles.sidebar}>
                                        <Sidebar/>
                                    </View>
                                </View>
                        </Modal>
                        
                        
                    </Left>
                    <View style={{flex:6}}>
                        <View style={{flex:1, width:"100%", justifyContent:"center"}}>
                            <Text style={{color:"white", fontWeight:"bold", fontSize:28, color:colors.bluegray}}>ERM</Text>
                        </View>   
                    </View>
                </Header>
                
                <ScrollView>
                    <Content>
                        <View>
                            <ButtonMenu/>
                        </View>
                        
                        
                        
                    </Content>
                </ScrollView>


            </Container>
        )
    }
}