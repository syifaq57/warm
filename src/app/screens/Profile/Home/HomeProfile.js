import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles from "../../../res/styles/Login";
import styles2 from "../../../res/styles/Form";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";
import firebase from "react-native-firebase";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
//Risk By


export default class HomeProfile extends Component {
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            visibleDialogSubmit: false,
            isModalVisible: false,
            visible:true,
            ButtonSave:true,
            data_user:[],
            // profile:{
            //     nama: 'Jhon Doe',
            //     username: '071010771',
            //     Approve: 'Approve 3'
            // }
            newPassword:'',
            oldPassword:'',
            encPassword:'',
            iconName:"notifications",
            isLoading:true
        };
    }

    loadDataUser(){
        this.setState({
            isLoading:true
        })
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('resp data',response)
                if (response.success == true) {
                    this.setState({
                        
                        nama:response.data.nama,
                        sebagai: response.data.sebagai,
                        karyawan: response.data.karyawan,
                        encPassword: response.data.password,
                        bagian_proyek: response.data.bagian_proyek
                    },function(){
                        this.loadNotif()
                        console.log('password', this.state.encPassword)
                        var sebagai = this.state.sebagai
                        if(sebagai == 'a1'||'a2'||'a3'){
                            
                            var statSplit = sebagai.replace('a', 'Approver ')
                            // var statSplit = sebagai.replace('a', 'Sebagai : Approver ')
                            
                        }if(sebagai == 'ct'){
                            // var statSplit = 'Sebagai : Creator'
                            var statSplit = 'Creator'
                        }
                        this.setState({
                            sebagai:statSplit,
                            isLoading:false
                        })
                    })
                }else{
                    
                    this.setState({
                        isLoading:false
                    })
                    
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingAll:false
                })
                console.log(error)
            })
    }

    loadNotif(){
        var url = GlobalConfig.SERVERHOST + 'api/api/getApproveDivision';
        var formData = new FormData();
        formData.append("username", this.state.username)
        formData.append("karyawan", this.state.karyawan)
        for(var i=0; i<this.state.bagian_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.bagian_proyek[i].id_proyek)
        }         
        formData.append("creator", this.state.creator)
        console.log('frm notif', formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if (response.success == true) {
                    if(response.data.length != 0){
                        
                        this.setState({
                            iconName:"notifications-active",
                            isLoading:false
                        })
                    }else{
                        this.setState({
                            iconName:"notifications",
                            isLoading:false
                        })
                    }
                    

                }else{
                    
                    this.setState({
                        iconName:"notifications",
                        isLoading:false
                    })
                }
            })
            .catch((error) => {
                console.log(error)
        })
    }

    getPlatfromCondition(){
        if(Platform.OS === "ios"){
            this.setState({
                visibleExitButton:true
            })
        }
    }

    componentDidMount(){
        this.getPlatfromCondition()
        AsyncStorage.getItem('username').then((value)=>{
            this.setState({
                username: value
            })
        })
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password: value
            },function(){
                this.loadDataUser()
                
            })
        })
        
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

      

    UpdatePassword(){
        this.setState({
            ButtonSave:false,
            isLoadingSave:true,
            
        })
        if(this.state.newPassword == this.state.confPassword){

        }else{
            
        }

        var url = GlobalConfig.SERVERHOST + 'api/api/gantiPass';
        var formData = new FormData();
        formData.append("password", this.state.encPassword)
        formData.append("pass_baru", this.state.newPassword)
        formData.append("pass_lama", this.state.oldPassword)
        formData.append("karyawan", this.state.karyawan)
        formData.append("username", this.state.username)
        formData.append("user", this.state.username)
        
        console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        password:this.state.newPassword
                    },function(){
                        AsyncStorage.setItem('password', this.state.newPassword).then(() => {
                            Alert.alert('Berhasil', 'Berhasil menyimpan data', [{
                                text: 'Okay'
                            }])
                            
                            this.setState({
                                isLoadingSave:false,
                                ButtonSave:true,
                            },function(){
                                this.loadDataUser()
                            })
    
                        })
                    })
                    
                    
                    
                    
                }
            })
            .catch((error) => {
                
                Alert.alert('Gagal mengubah data', 'Password Lama Salah ', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingSave:false,
                    ButtonSave:true
                },function(){
                    this.loadDataUser()
                })
                console.log(error)
            })

    }  

    navigateToScreen(route){
        this.props.navigation.navigate(route);
        // AsyncStorage.setItem('id_article', id).then(() => { 
        //   this.props.navigation.navigate(route);
        //})
      }  
      load(){
        
        this.componentDidMount()
        console.log('masuk')
    }


    deleteToken(){
        var url = GlobalConfig.SERVERHOST + 'api/api/deleteToken';
        var formData = new FormData();
        formData.append("username", this.state.username)
        console.log('delete token',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('datarespon',response)
                if (response.success == true) {
                    console.log('berhasil logout')
                    
                }
            })
            .catch((error) => {
                
                Alert.alert('Gagal Logout', ' ', [{
                    text: 'Okay'
                }])
                
                console.log(error)
            })
    }

    onLogOut(route){
        AsyncStorage.setItem("idlogin", "0")
        AsyncStorage.setItem("password", "")
        this.props.navigation.navigate(route);
        this.deleteToken()
        
    }  

    render(){

        return(
            <Container style={styles.wrapper}>
                <ImageBackground
                    source={require('../../../res/images/BGdashboard.jpg')} 
                    style={{width: '100%', height: '100%',backgroundColor:colors.lightBlack, paddingTop:0}}
                >
                    <StatusBar
                    translucent={true}
                    barStyle="light-content"
                    />
                    <Header
                        transparent
                        style={{ marginTop:Platform.OS === "ios" ? 0 :0, height:80, borderTopWidth:2, borderColor:'black'}}
                        // androidStatusBarColor={colors.gray10}
                        >
                        <View style={{flex:6, paddingVertical:Platform.OS === "ios" ? 0 :15, alignItems:'flex-start'}}>
                            <View style={{flex:1,justifyContent:"center", marginLeft:5, marginTop:10}}>
                                <Image
                                source={require('../../../res/images/LOGO.png')}
                                style={{width:145, height:50}}
                            />
                            </View>   
                        </View>
                        <View style={{justifyContent:'center', marginRight:5, marginBottom:-5, justifyContent:'flex-end'}}>
                            <Button
                                style={{marginLeft:10, justifyContent:'center'}}
                                transparent
                                onPress={()=>this.load()}
                                >
                                <Icon
                                    name="refresh"
                                    size={30}
                                    style={{color:'#000078', marginTop:5}}
                                />
                            </Button>
                        </View>
                        <View style={{justifyContent:'center', marginRight:10, marginBottom:-5, justifyContent:'flex-end'}}>
                            <Button
                                style={{marginLeft:-10, justifyContent:'center'}}
                                transparent
                                // onPress={()=>this.props.navigation.navigate("ListAprove")}
                                onPress={this.toggleModal}
                                >
                                <Icon2
                                    name="md-power"
                                    size={28}
                                    style={{color:'#000078', marginTop:5}}
                                />
                            </Button>
                        </View>
                        <Modal
                            style={{marginHorizontal:40,marginBottom:0}}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}
                            onBackButtonPress={this.toggleModal}
                            backdropOpacity={0.3}
                        >
                            <View style={{ flex:1, justifyContent:"flex-end", }}>
                                <View style={{borderTopLeftRadius:5, borderTopRightRadius:5,}}>
                                    {this.state.visibleExitButton?(
                                        <View></View>
                                        
                                    ):(
                                        <View style={{
                                        width:"100%",alignContent:"center",marginBottom:5, 
                                        marginTop:10, backgroundColor:'white',alignItems:'center', 
                                        padding:10, borderRadius:10, borderWidth:1, borderColor:colors.gray03
                                    }}>
                                        <TouchableOpacity
                                            onPress={() => BackHandler.exitApp()}
                                        >
                                            <View>
                                                <Text style={{fontSize:17, color:'black'}}>Exit App</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    )}
                                    
                                    <View style={{
                                        width:"100%",alignContent:"center",marginBottom:5, 
                                        marginTop:5, backgroundColor:'white',alignItems:'center', 
                                        padding:10, borderRadius:10, borderWidth:1, borderColor:colors.gray03
                                    }}>
                                        <TouchableOpacity
                                            onPress={()=>this.onLogOut("SplashScreen")}
                                        >
                                            <View>
                                                <Text style={{fontSize:17, color:'black'}}>Logout</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{
                                        alignSelf:'center',
                                        width:"70%",alignContent:"center",marginBottom:15, 
                                        marginTop:15, backgroundColor:'white',alignItems:'center', 
                                        padding:10, borderRadius:10, borderWidth:1, borderColor:colors.gray03
                                    }}>
                                        <TouchableOpacity
                                            onPress={this.toggleModal}
                                        >
                                            <View>
                                                <Text style={{fontSize:17, color:'black'}}>Cancel</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                
                                </View>
                            
                            </View>
                        </Modal>
                
                    </Header>
                    {this.state.isLoading?(
                        <View style={{justifyContent:'center', marginTop:10}}>
                            <ActivityIndicator size='large'/>
                        </View>
                        
                    ):(

                    <Content style={{marginHorizontal:10, marginVertical:'5%', paddingHorizontal:10}}>
                    <Card 
                            style={{ paddingHorizontal:15, borderRadius:10, backgroundColor:'rgba(255,255,255,0.1)'}}
                            >
                            <View style={{paddingVertical:5, flexDirection:'row'}}>
                                <View style={{justifyContent:'center', borderRadius:40}}>
                                    <Image
                                        source={require('../../../res/images/avatar.png')}
                                        style={{width:90, height:90, borderRadius:35}}
                                    />
                                </View>
                                <View style={{marginLeft:10,borderRadius:8, justifyContent:'center', width:'65%' }}>
                                    <View style={{justifyContent:'center', marginBottom:5, padding:10 }}>
                                        <View style={{borderBottomColor:colors.gray09, borderBottomWidth:1}}>
                                        <Text style={{color:colors.gray092}}>Nama</Text>
                                        </View>
                                        
                                        <Text style={{fontSize:13, marginBottom:5, color:"black"  }}>
                                            {this.state.nama}
                                        </Text>
                                        
                                        <View style={{borderBottomColor:colors.gray09, marginTop:5, borderBottomWidth:1}}>
                                            <Text style={{color:colors.gray092}}>Status</Text>
                                        </View>
                                        <Text style={{fontSize:13, color:'black'}}>
                                            {this.state.sebagai}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </Card>
                        <Card style={{ borderRadius:5, marginTop:20, marginHorizontal:10}}> 
                            <View style={{backgroundColor:colors.bgErm, borderTopRightRadius:5, borderTopLeftRadius:5,paddingVertical:13, alignItems:'center', justifyContent:'center'}}>
                                <Text style={{fontSize:16, fontWeight:'bold', color:colors.gray09}}>Ubah Password</Text>
                            </View>
                            <View style={{marginHorizontal:2, marginTop:10,paddingHorizontal:5 }}>
                                
                                <View style={{padding:10}}>
                                <View style={{marginLeft:5, marginBottom:5}}>
                                    <Text style={{fontSize:12}}>Username :</Text>
                                </View>
                                <View style={{marginBottom:10, }}>
                                    <TextInput
                                        style={{
                                            
                                            marginBottom: 5,
                                            paddingLeft:10,
                                            fontSize: 11,
                                            borderWidth:1,
                                            height:35,
                                            borderRadius:5,
                                            fontWeight:'bold',
                                            borderColor:colors.gray05,
                                            width:'100%',
                                        }}
                                        rowSpan={1}
                                        bordered
                                        editable={false}
                                        placeholderTextColor={colors.gray02}
                                        value={this.state.username}
                                        placeholder="Type Something ..."
                                        // onChangeText={text =>
                                        // this.setState({ ID_HAZARD : text })
                                        // }
                                    />
                                </View>
                                <View style={{marginLeft:5, marginBottom:5}}>
                                    <Text style={{fontSize:12}}>Password Lama :</Text>
                                </View>
                                <View style={{marginBottom:10, }}>
                                    <TextInput
                                        style={{
                                            
                                            marginBottom: 5,
                                            paddingLeft:10,
                                            fontSize: 11,
                                            borderWidth:1,
                                            height:35,
                                            borderRadius:5,
                                            fontWeight:'bold',
                                            borderColor:colors.gray05,
                                            width:'100%',
                                        }}
                                        rowSpan={1}
                                        bordered
                                        editable={true}
                                        placeholderTextColor={colors.gray02}
                                        value={this.state.data_user.username}
                                        secureTextEntry={true}
                                        placeholder="Password Lama ..."
                                        onChangeText={
                                            text =>this.setState({ oldPassword: text })
                                        }
                                        // onChangeText={text =>
                                        // this.setState({ ID_HAZARD : text })
                                        // }
                                    />
                                </View>
                                <View style={{marginLeft:5, marginBottom:5}}>
                                    <Text style={{fontSize:12}}>Password Baru :</Text>
                                </View>
                                <View style={{marginBottom:10, }}>
                                    <TextInput
                                        style={{
                                            
                                            marginBottom: 5,
                                            paddingLeft:10,
                                            fontSize: 11,
                                            borderWidth:1,
                                            height:35,
                                            borderRadius:5,
                                            fontWeight:'bold',
                                            borderColor:colors.gray05,
                                            width:'100%',
                                        }}
                                        rowSpan={1}
                                        bordered
                                        editable={true}
                                        placeholderTextColor={colors.gray02}
                                        value={this.state.data_user.username}
                                        secureTextEntry={true}
                                        placeholder="Password Baru..."
                                        onChangeText={
                                            text =>this.setState({ newPassword: text })
                                        }
                                    />
                                </View>
                                <View style={{marginLeft:5, marginBottom:5}}>
                                    <Text style={{fontSize:12}}>Konfirmasi Password Baru :</Text>
                                </View>
                                <View style={{marginBottom:10, }}>
                                    <TextInput
                                        style={{
                                            
                                            marginBottom: 5,
                                            paddingLeft:10,
                                            fontSize: 11,
                                            borderWidth:1,
                                            height:35,
                                            borderRadius:5,
                                            fontWeight:'bold',
                                            borderColor:colors.gray05,
                                            width:'100%',
                                        }}
                                        rowSpan={1}
                                        bordered
                                        editable={true}
                                        placeholderTextColor={colors.gray02}
                                        value={this.state.data_user.username}
                                        secureTextEntry={true}
                                        placeholder="Konfirmasi Password ..."
                                        onChangeText={
                                            text =>this.setState({ confPassword: text })
                                        }
                                    />
                                </View>
                                </View>
                                
                                {this.state.ButtonSave?
                                    <View style={{ marginLeft:60, marginRight:60, marginBottom:15, marginTop:0 }}>
                                        <Button
                                            block
                                            // onPress={() => this.onLoginPress()}
                                            onPress={() => this.UpdatePassword()}
                                            style={{backgroundColor:colors.gray092, height:35, width:100, alignSelf:'center', borderRadius:5}}
                                            >
                                                <Text style={{color:'white', fontWeight:'bold'}}>Simpan</Text>
                                        </Button>
                                    </View>:null
                                }
                                {this.state.isLoadingSave?(
                                    <ActivityIndicator/>
                                ):(
                                    <View/>
                                )}
                                
                            </View>
                        </Card>
                            <View style={{flex:2, paddingBottom:25, paddingTop:25, marginVertical:10}}>
                                <View style={{ justifyContent:"center", alignItems:"center"}}>
                                    <Text style={{fontSize:10}}>Version 1.0.1</Text>
                                </View>  
                            </View>  
                            <View style={{ paddingRight: 10, paddingLeft: 10, marginBottom:10, marginTop:0 }}>
                                <Button
                                    block
                                    // onPress={() => this.onLoginPress()}
                                    onPress={() => this.onLogOut("SplashScreen")}
                                    style={styles.logoutButton}
                                    >
                                        <Text style={styles.textButton}>LOG OUT</Text>
                                </Button>
                            </View> 
                            
                        </Content>
                    )}
                </ImageBackground>
                
               
                
            </Container>
        )
    }
}