import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";
import ButtonMenu from "../ItemDashboard/ButtonMenu";

//Risk By


export default class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            visibleDialogSubmit: false,
            isModalVisible: false,
            visible:true,
            data_user:[],
            isLoading:false,
            iconName:"notifications",
            visibleExitButton:false,
          
        };
    }
    
    loadDataUser(){
        this.setState({
            isLoading:true
        })
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        // console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        
                        nama:response.data.nama,
                        sebagai: response.data.sebagai,
                        karyawan: response.data.karyawan,
                        password: response.data.password,
                        bagian_proyek: response.data.bagian_proyek
                    },function(){
                        var sebagai = this.state.sebagai
                        if(sebagai == 'a1'||'a2'||'a3'){
                            
                            var statSplit = sebagai.replace('a', 'Approver ')
                            // var statSplit = sebagai.replace('a', 'Sebagai : Approver ')
                            
                        }if(sebagai == 'ct'){
                            // var statSplit = 'Sebagai : Creator'
                            var statSplit = 'Creator'
                        }

                        this.setState({
                            sebagai:statSplit,
                            isLoading:false
                            
                        },function(){
                            this.loadNotif()
                        })
                    })
                    // console.log('kry', this.state.sebagai)
                    // AsyncStorage.setItem('data_user', (JSON.stringify(response.data)))
                    // this.AsyncDataUser()

                }else{
                    
                    // this.setState({
                    //     isLoading:false
                    // })
                    
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Profile', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingAll:false
                })
                console.log(error)
            })
         
    }

    loadNotif(){
        var url = GlobalConfig.SERVERHOST + 'api/api/getApproveDivision';
        var formData = new FormData();
        formData.append("username", this.state.username)
        formData.append("karyawan", this.state.karyawan)
        for(var i=0; i<this.state.bagian_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.bagian_proyek[i].id_proyek)
        }         
        formData.append("creator", this.state.creator)
        // console.log('frm notif', formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if (response.success == true) {
                    if(response.data.length != 0){
                        
                        this.setState({
                            iconName:"notifications-active",
                            isLoading:false
                        })
                    }else{
                        this.setState({
                            iconName:"notifications",
                            isLoading:false
                        })
                    }
                    

                }else{
                    
                    this.setState({
                        iconName:"notifications",
                        isLoading:false
                    })
                }
            })
            .catch((error) => {
                console.log(error)
        })
    }

    deleteToken(){
        var url = GlobalConfig.SERVERHOST + 'api/api/deleteToken';
        var formData = new FormData();
        formData.append("username", this.state.username)
        console.log('delete token',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('datarespon',response)
                if (response.success == true) {
                    console.log('berhasil logout')
                    
                }
            })
            .catch((error) => {
                
                Alert.alert('Gagal Logout', ' ', [{
                    text: 'Okay'
                }])
                
                console.log(error)
            })
    }

    onLogOut(route){
        AsyncStorage.setItem("idlogin", "0")
        AsyncStorage.setItem("password", "")
        this.props.navigation.navigate(route);
        this.deleteToken()
        
    }  

    getPlatfromCondition(){
        if(Platform.OS === "ios"){
            this.setState({
                visibleExitButton:true
            })
        }
    }

    componentDidMount(){
        this.getPlatfromCondition()
        AsyncStorage.getItem('username').then((value)=>{
            this.setState({
                username: value
            })
        })
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password: value
            },function(){
                this.loadDataUser()
                
            })
        })

        this._onFocusListener = this.props.navigation.addListener(
            "didFocus",
            
            payload => {
                console.log('metu')
                this.backHandler =BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
                this.setState({ registerToken: "" });
            }
        );

        const didBlurSubscription = this.props.navigation.addListener(
            'didBlur',
            payload => {
            //   console.log("masuk blur")
              this.backHandler.remove();
            }
          );
        
    }

    handleBackPress = () => {
        this.exitApp(); // works best when the goBack is async
        return true;
    }
    

    exitApp() {
    Alert.alert(
        "Confirmation",
        "Exit WARM Mobile?",
        [
        {
            text: "No",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
        },
        { text: "Yes", onPress: () => BackHandler.exitApp() }
        ],
        { cancelable: false }
    );
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    navigateToScreen(route){
        this.props.navigation.navigate(route);
     
      }  

      load(){
        
        this.componentDidMount()
        console.log('masuk')
    }
    render(){

        return(
            <Container style={styles.wrapper}>
                <ImageBackground
                    source={require('../../../res/images/BGdashboard.jpg')} 
                    style={{width: '100%', height: '100%',backgroundColor:colors.lightBlack, paddingTop:0}}
                >
                <StatusBar
                    translucent={true}
                    barStyle="light-content"
                />
                    <Header
                        transparent
                        style={{ marginTop:Platform.OS === "ios" ? 0 :0, height:80, borderTopWidth:2, borderColor:'black'}}
                        // androidStatusBarColor={colors.gray10}
                        >
                        <View style={{flex:6, paddingVertical:Platform.OS === "ios" ? 0 :15, alignItems:'flex-start'}}>
                            <View style={{flex:1,justifyContent:"center", marginLeft:5, marginTop:10}}>
                                <Image
                                source={require('../../../res/images/LOGO.png')}
                                style={{width:145, height:50}}
                            />
                            </View>   
                        </View>
                        <View style={{justifyContent:'center', marginRight:5, marginBottom:-5, justifyContent:'flex-end'}}>
                            <Button
                                style={{marginLeft:10, justifyContent:'center'}}
                                transparent
                                onPress={()=>this.load()}
                                >
                                <Icon
                                    name="refresh"
                                    size={30}
                                    style={{color:'#000078', marginTop:5}}
                                />
                            </Button>
                        </View>
                        <View style={{justifyContent:'center', marginRight:10, marginBottom:-5, justifyContent:'flex-end'}}>
                            <Button
                                style={{marginLeft:-10, justifyContent:'center'}}
                                transparent
                                // onPress={()=>this.props.navigation.navigate("ListAprove")}
                                onPress={this.toggleModal}
                                >
                                <Icon2
                                    name="md-power"
                                    size={28}
                                    style={{color:'#000078', marginTop:5}}
                                />
                            </Button>
                        </View>
                        <Modal
                            style={{marginHorizontal:40,marginBottom:0}}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}
                            onBackButtonPress={this.toggleModal}
                            backdropOpacity={0.3}
                        >
                            <View style={{ flex:1, justifyContent:"flex-end", }}>
                                <View style={{borderTopLeftRadius:5, borderTopRightRadius:5,}}>
                                    {this.state.visibleExitButton?(
                                        <View></View>
                                        
                                    ):(
                                        <View style={{
                                        width:"100%",alignContent:"center",marginBottom:5, 
                                        marginTop:10, backgroundColor:'white',alignItems:'center', 
                                        padding:10, borderRadius:10, borderWidth:1, borderColor:colors.gray03
                                        }}>
                                            <TouchableOpacity
                                                onPress={() => BackHandler.exitApp()}
                                            >
                                                <View>
                                                    <Text style={{fontSize:17, color:'black'}}>Exit App</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    )}
                                    
                                    <View style={{
                                        width:"100%",alignContent:"center",marginBottom:5, 
                                        marginTop:5, backgroundColor:'white',alignItems:'center', 
                                        padding:10, borderRadius:10, borderWidth:1, borderColor:colors.gray03
                                    }}>
                                        <TouchableOpacity
                                            onPress={()=>this.onLogOut("SplashScreen")}
                                        >
                                            <View>
                                                <Text style={{fontSize:17, color:'black'}}>Logout</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{
                                        alignSelf:'center',
                                        width:"70%",alignContent:"center",marginBottom:15, 
                                        marginTop:15, backgroundColor:'white',alignItems:'center', 
                                        padding:10, borderRadius:10, borderWidth:1, borderColor:colors.gray03
                                    }}>
                                        <TouchableOpacity
                                            onPress={this.toggleModal}
                                        >
                                            <View>
                                                <Text style={{fontSize:17, color:'black'}}>Cancel</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                
                                </View>
                            
                            </View>
                        </Modal>
                
                    </Header>
                    {this.state.isLoading?(
                        <View style={{justifyContent:'center', marginTop:10}}>
                            <ActivityIndicator size='large'/>
                        </View>
                        
                    ):(

                    
                    <Content style={{ marginVertical:'5%'}}>
                        <View style={{paddingHorizontal:15}}>
                        <Card 
                            style={{ paddingHorizontal:15, borderRadius:10, backgroundColor:'rgba(255,255,255,0.1)'}}
                            >
                            <View style={{paddingVertical:5, flexDirection:'row'}}>
                                <View style={{justifyContent:'center', borderRadius:40}}>
                                    <Image
                                        source={require('../../../res/images/avatar.png')}
                                        style={{width:90, height:90, borderRadius:35}}
                                    />
                                </View>
                                <View style={{marginLeft:10,borderRadius:8, justifyContent:'center', width:'65%' }}>
                                    <View style={{justifyContent:'center', marginBottom:5, padding:10 }}>
                                        <View style={{borderBottomColor:colors.gray09, borderBottomWidth:1}}>
                                        <Text style={{color:colors.gray092}}>Nama</Text>
                                        </View>
                                        
                                        <Text style={{fontSize:13, marginBottom:5, color:"black"  }}>
                                            {this.state.nama}
                                        </Text>
                                        
                                        <View style={{borderBottomColor:colors.gray09, marginTop:5, borderBottomWidth:1}}>
                                            <Text style={{color:colors.gray092}}>Status</Text>
                                        </View>
                                        <Text style={{fontSize:13, color:'black'}}>
                                            {this.state.sebagai}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </Card>
                    </View>
                    {/* <View style={{ flex: 1, justifyContent: "center", alignItems: "center", marginTop:10}}>
                        <Image
                            source={require('../../../res/images/LOGO.png')}
                            style={{width:200, height:70}}
                        />
                    </View> */}

                    <View style={{flex:1, marginTop:20, paddingHorizontal:20, alignItems:'center'}}>
                        <View style={{flex:1, flexDirection:'row',}}>
                            <View style={{alignItems:'center', marginRight:'10%'}}>
                                <Card style={{borderRadius:10}}>
                                    <TouchableOpacity
                                        onPress={()=>(this.navigateToScreen("AllUnitDashboard"))}
                                    >
                                    <View style={{backgroundColor:'#5E9EA0', borderRadius:10, padding:15}}>
                                        <Image
                                            source={require('../../../res/images/chart.png')}
                                            style={{width:60, height:60,}}
                                        />
                                    </View>
                                </TouchableOpacity>
                                </Card>
                                <View style={{}}>
                                    <Text style={{}}>My Dashboard</Text>
                                </View>
                            </View>
                            
                            <View style={{alignItems:'center', marginLeft:'10%'}}>
                                <Card style={{borderRadius:10}}>
                                    <TouchableOpacity
                                        onPress={()=>(this.navigateToScreen("RiskRegister"))}
                                    >
                                    <View style={{backgroundColor:'#CD5D5C', borderRadius:10, padding:15}}>
                                        <Image
                                            source={require('../../../res/images/corporate.png')}
                                            style={{width:60, height:60,}}
                                        />
                                    </View>
                                </TouchableOpacity>
                                </Card>
                                <View style={{}}>
                                    <Text style={{}}>Risk Register</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{flexDirection:'row', marginTop:'15%'}}>
                            <View style={{alignItems:'center', marginRight:'10%'}}>
                                <Card style={{borderRadius:10}}>
                                    <TouchableOpacity
                                        onPress={()=>(this.navigateToScreen("ListAprove"))}
                                    >
                                    <View style={{backgroundColor:'#F08E6A', borderRadius:10, padding:15}}>
                                        <Image
                                            source={require('../../../res/images/list.png')}
                                            style={{width:60, height:60,}}
                                        />
                                    </View>
                                </TouchableOpacity>
                                </Card>
                                <View style={{}}>
                                    <Text style={{}}>List Approval</Text>
                                </View>
                            </View>
                            
                            <View style={{alignItems:'center', marginLeft:'10%',}}>
                                <Card style={{borderRadius:10}}>
                                    <TouchableOpacity
                                        onPress={()=>(this.navigateToScreen("HomeRiskStatus"))}
                                    >
                                    <View style={{backgroundColor:'#858E97', borderRadius:10, padding:15}}>
                                        <Image
                                            source={require('../../../res/images/status.png')}
                                            style={{width:60, height:60,}}
                                        />
                                    </View>
                                </TouchableOpacity>
                                </Card>
                                <View style={{}}>
                                    <Text style={{}}>Risk Status</Text>
                                </View>
                            </View>
                        </View>
                        
                    </View>
                    
                    
                    {/* <Card style={{ borderRadius:10}}>
                        <TouchableOpacity
                            onPress={()=>(this.navigateToScreen("AllUnitDashboard"))}>
                            <View style={{flexDirection:'row', backgroundColor:'#5E9EA0', borderRadius:10 }}>
                                <View style={{justifyContent:'center', marginVertical:15, marginLeft:5}}>
                                    <Image
                                        source={require('../../../res/images/chart.png')}
                                        style={{width:50, height:50, marginLeft:10}}
                                    />
                                </View>
                                
                                <View style={{width:"100%", justifyContent:'center', marginVertical:5, marginLeft:10}}>
                                    <Text style={{fontSize:22, color:"white", fontWeight:"bold"}}>My Dashboard</Text>
                                </View>
                               
                            </View>
                        </TouchableOpacity>
                    </Card>
                    <Card style={{ borderRadius:10}}>
                        <TouchableOpacity
                        onPress={()=>(this.navigateToScreen("RiskRegister"))}
                        >
                            <View style={{flexDirection:'row', backgroundColor:'#CD5D5C', borderRadius:10 }}>
                                <View style={{justifyContent:'center', marginVertical:15, marginLeft:5}}>
                                    <Image
                                        source={require('../../../res/images/corporate.png')}
                                        style={{width:50, height:50, marginLeft:10}}
                                    />
                                </View>
                                
                                <View style={{width:"100%", justifyContent:'center', marginVertical:5, marginLeft:10}}>
                                    <Text style={{fontSize:22, color:"white", fontWeight:"bold"}}>Risk Register</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </Card> */}

                    
                </Content>
                )}
                </ImageBackground>
                {/* )} */}
                
            </Container>
        )
    }
}