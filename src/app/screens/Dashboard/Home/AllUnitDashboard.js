import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    // BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";
import TableRiskStatus from "../ItemDashboard/TableRiskStatus";
import SwipePie from '../ItemDashboard/SwipePie';
import SwipePetaRisk from '../ItemDashboard/SwipePetaRisk';
import ButtonMenu from "../ItemDashboard/ButtonMenu";
import Dropdown from '../../../library/component/Dropdown';
import SubMenuAllunitDashboard from "../../../library/component/SubMenuAllunitDashboard";
import SearchableDropdown from 'react-native-searchable-dropdown';
//Risk By

var items = [];

export default class AllUnitDashboard extends Component {
    constructor(props){
        super(props)
        
       
        this.state={
            username: "",
            password: "",
            
            tipeRisk:[
                { 
                    id:1,
                    name:'Treated',
                    kode:'n'
                },
                {
                    id:2,
                    name:'Expected',
                    kode:'y'
                }
                     
            ],
            tipeIndex:[],
            Tahun:[],
            listTahun:[],
            tahunSelect:'',
            selectedItems:[],

            visibleDialogSubmit: false,
            isSelectVisible: false,
            visible:true,
            isLoadingAll:true,
            itemDepart:[],
            isLoadingSwipePie:false,
            
            departIndex:[],
            targetIndex:[],
            targetNameIndex:[],
            listPieNN:[],
            listPieNP:[],
            

            // targetNameIndex:'Treated'
          
        };
    }
    navigateToScreen(route){
        this.setState({
            isModalVisible:false
        })
        this.props.navigation.navigate(route);
        
      }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    searchModal = () => {
        this.setState({
            isSearchModal: !this.state.isSearchModal
        })
    }  

      loadDataUser(){
        this.setState({
            isLoadingAll:true
        })
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        // console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        
                        id_proyek:response.data.bagian_proyek,
                        karyawan: response.data.karyawan,
                        creator: response.data.creator,
                        company: response.data.company,
                        sebagai: response.data.sebagai,
                    },function(){
                        this.loadDepart()
                    })
                    // console.log('kry', this.state.sebagai)
                    // AsyncStorage.setItem('data_user', (JSON.stringify(response.data)))
                    // this.AsyncDataUser()

                }else{
                    
                    this.setState({
                        isLoadingAll:false
                    })
                    // Alert.alert('Error', 'Tidak bisa load data', [{
                    //     text: 'Okay'
                    // }])
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingAll:false
                })
                console.log(error)
            })
         
    }
    
      loadDepart(){
        // this.setState({
        //     isLoadingDepart:false,
        // })
        var url = GlobalConfig.SERVERHOST + 'api/api/my_dashboard_depart';
        var formData = new FormData();
        formData.append("karyawan", this.state.karyawan)
        formData.append("username", this.state.username)
        formData.append("company", this.state.company)
        for(var i=0; i<this.state.id_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.id_proyek[i].id_proyek)
        }
        
        console.log('yuuhuu',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                console.log('dta', response.data)
                if(response.success == true){
                    var depart = this.state.itemDepart
                    for(var i=0; i<response.data.unit_kerja.length; i++){
                        depart.push({id:response.data.unit_kerja[i].obj_id, name:response.data.unit_kerja[i].obj_name_en})
                    }
                    
                
                    this.setState({
                        itemDepart:depart,
                        isLoadingAll:false,
                        Id_departIndex:depart[0].id,
                        departIndex:depart[0].name,
                        
                        targetIndex:this.state.tipeRisk[0].kode,
                        targetNameIndex:this.state.tipeRisk[0].name
                    })
                    // console.log('dprt',this.state.departIndex)
                }
            })
            .catch((error) => {
                this.setState({
                    isLoadingAll:false
                })
                Alert.alert('Cannot Load Data', 'Cannot Load Depart', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    getYear(){
        var years = new Date();
        var year = years.getFullYear();
        
        this.setState({
            tahunSelect:year
        }, function(){
            // console.log('tahuuuuuuuuuunnn', this.state.tahunSelect)
            for(var i=2019; i<=this.state.tahunSelect; i++){
                var years =this.state.Tahun
                years.push({id:i, name: i.toString()})
                
            }
            items = years
            this.setState({
            Tahun:years,
            listTahun:this.state.Tahun.map((data)=>(data.value)),
            isLoading:false,
            
        })
        })
    }

    componentDidMount(){
        this.getYear()
        // AsyncStorage.getItem('username').then((value)=>{
        //     this.setState({
        //         username: value
        //     })
        // })
        // AsyncStorage.getItem('password').then((value)=>{
        //     this.setState({
        //         password: value
        //     },function(){
        //         this.loadDataUser()
                
        //     })
        // })
        
        AsyncStorage.getItem('data_user').then((value) =>{
            this.setState({
                id_proyek:JSON.parse(value).bagian_proyek,
                karyawan: JSON.parse(value).karyawan,
                creator: JSON.parse(value).creator,
                company: JSON.parse(value).company,
                sebagai: JSON.parse(value).sebagai,
            }, function(){
                console.log('id---pyk', this.state.karyawan)
                this.loadDepart()
            })
        })

        
        
        
        
    }  

    

    onFilter = () =>{
        
        this.setState({
            isLoadingSwipePie:true,
            isSearchModal:false
        },function(){
            this.setState({
                isLoadingSwipePie:false
            })
            
        })
    }

    


    render(){

        return(
            <Container style={styles.wrapper}>
                <Header
                    style={{ backgroundColor: '#AFDEF5', marginTop:Platform.OS === "ios" ? 0 : 15,}}
                    androidStatusBarColor={'#68C1EC'}
                    >
                    
                    <View style={{flex:6, flexDirection:"row"}}>
                        <TouchableOpacity
                            style={{ marginLeft:5, justifyContent:"center"}}   
                            onPress={()=>this.props.navigation.goBack()}
                        >   
                            <Icon2 
                            
                                name="md-arrow-back"
                                size={28}
                                color={colors.gray09}
                            />
                            
                            
                        </TouchableOpacity>
                        <View style={{flex:1, width:"100%", justifyContent:"center", marginLeft:15}}>
                            <Text style={{color:colors.gray09, fontWeight:"bold",  fontSize:22}}>My Dashboard</Text>
                        </View>   
                    </View>
                    <Right style={{flex:1, marginLeft:0, justifyContent:"center", marginRight:5}}>
                        
                        <TouchableOpacity
                            onPress={this.toggleModal}>
                            <Icon
                                    style={{alignSelf:"flex-end"}}
                                    name="apps"
                                    size={30}
                                    color={colors.gray09}
                                />
                        </TouchableOpacity>
                        <Modal
                            style={{marginHorizontal:0,marginBottom:0}}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}
                            onBackButtonPress={this.toggleModal}
                            onSwipeComplete={this.toggleModal}
                            swipeDirection="down"
                        >
                            <View style={{ flex:1, justifyContent:"flex-end", }}>
                                <View style={{backgroundColor:'white', borderTopLeftRadius:5, borderTopRightRadius:5,}}>
                                    <View style={{width:"100%",alignContent:"center", backgroundColor:colors.bgErm, borderTopLeftRadius:4, borderTopRightRadius:4, alignItems:'center', padding:13, borderBottomColor:colors.gray03, borderBottomWidth:1 }}>
                                        <Text style={{fontSize:17, fontWeight:"bold", color:colors.gray09}}>Dashboard</Text>
                                    </View>
                                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                                        <TouchableOpacity onPress={()=>(this.navigateToScreen("AllUnitDashboard"))} style={{flexDirection:"row"}}>
                                            <View style={{padding:10, flexDirection:"row"}}>
                                                <Image
                                                    source={require('../../../res/images/chart.png')}
                                                    style={{width:30, height:30, marginRight:10}}
                                                />
                                                <View style={{justifyContent:"center", width:"100%"}}>
                                                    <Text style={{fontWeight:"bold"}}>
                                                        My Dashboard
                                                    </Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                                    <TouchableOpacity onPress={()=>(this.navigateToScreen("RiskRegister"))} style={{flexDirection:"row"}}>
                                            <View style={{padding:10, flexDirection:"row"}}>
                                                <Image
                                                    source={require('../../../res/images/corporate.png')}
                                                    style={{width:30, height:30, marginRight:10}}
                                                />
                                                <View style={{justifyContent:"center", width:"100%"}}>
                                                    <Text style={{fontWeight:"bold"}}>
                                                        Risk Register
                                                    </Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                
                                </View>
                            
                            </View>
                        </Modal>
                        
                        
                    </Right>
                </Header>
                {/* {this.state.isSelectVisible?(
                    
                )} */}
                    <Content >
                    {this.state.isLoadingAll?(
                        <View style={{justifyContent:'center',marginTop:10,}}>
                            <ActivityIndicator />
                        </View>
                    ):(
                        
                        <View>

                        <View style={{flexDirection:'row', marginTop:15, marginBottom:10, marginRight:15, marginLeft:15}}>
                            
                            <Dropdown
                            // api="api/optionlist/search/sub_option" 
                            // bodyApi={[{"name":"group_id","value":"41"},{"name":"search","value":""}]}
                                items={this.state.tipeRisk}
                                height={110}
                                searchByApi={false}
                                displayData="name" 
                                selected={this.state.targetNameIndex}
                                selectedMethod={item=>this.setState({targetNameIndex:item.kode})} 
                                placeholder="Pilih Tipe Risk"
                            />
                            <View style={{marginLeft:10, paddingVertical:2, borderRadius:5}}>
                                <Button
                                    style={{backgroundColor:colors.bgErm, borderRadius:5,}}
                                    onPress={() => this.onFilter()}
                                >
                                    <Text style={{marginLeft:20, marginRight:20, color:colors.gray09, fontWeight:'bold', fontSize:16}}>Cari</Text>
                                </Button>  
                            </View>
                            
                        </View>

                        {/* <Card style={{ marginLeft:10, marginRight:10, marginTop:10, backgroundColor:colors.bgErm, borderRadius:5}}>
                            <View style={{flexDirection:'row', alignItems:'center', marginLeft:10, paddingVertical:10}}>
                                <View style={{flex:3, flexDirection:'column', marginRight:0}}>
                                    <View style={{width:'100%'}}>
                                        <Text style={{color:colors.gray09, fontSize:12}}>Depart/Project    :</Text>
                                    </View>
                                    <View style={{width:'100%'}}>
                                        <Text style={{color:colors.gray09,  fontSize:12}}>Tipe Risk              :</Text>
                                    </View>
                                </View>
                                <View style={{flex:5.5, flexDirection:'column',}}>
                                    <View style={{width:'100%'}}>
                                        <Text style={{color:colors.gray09,  fontSize:12}}>{this.state.departIndex}</Text>
                                    </View>
                                    <View style={{width:'100%'}}>
                                        <Text style={{color:colors.gray09,  fontSize:12}}>{this.state.targetNameIndex}</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, justifyContent:'center', borderLeftWidth:1, borderColor:colors.gray02}}>
                                    <TouchableOpacity
                                        onPress={this.searchModal}>
                                        <Icon2
                                                style={{alignSelf:'center'}}
                                                name="md-create"
                                                size={20}
                                                color={colors.gray09}
                                            />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </Card>
                        <Modal
                            isVisible={this.state.isSearchModal}
                            onBackdropPress={this.searchModal}
                            onBackButtonPress={this.searchModal}
                            >
                            
                            <Card style={{marginTop:10, marginLeft:10, marginRight:10, borderRadius:3,}}>
                            <View style={{flexDirection:'column', marginTop:0}}>
                                <View style={{
                                flexDirection:'row', 
                                width:"100%",
                                paddingVertical:10,
                                backgroundColor: colors.bgErm,
                                borderTopLeftRadius:2,
                                borderTopRightRadius:2,
                                
                                }}>
                                <View style={{ flex:4, width:'100%', marginLeft:25, justifyContent:'center'}}>
                                <Text style={{fontWeight:'bold', color:colors.gray09, fontSize:16}}>
                                Select Risiko
                                </Text>
                            </View>
                            <View style={{marginTop:1 , alignItems:'flex-end' }}>
                                <View style={{paddingRight: 20, paddingLeft: 40}}>
                                    <Button
                                        block
                                        // onPress={() => this.onLoginPress()}
                                        onPress={() => this.onFilter()}
                                        style={{alignItems:'center', height:35, borderRadius:3, backgroundColor:colors.erm2,  width:70}}
                                        >
                                            <Icon3
                                                name="file-search"
                                                size={27}
                                                color={colors.gray09}
                                            />
                                            <Text style={{color:colors.gray09,fontWeight:'bold' }}>Cari  </Text>
                                    </Button>
                                </View>
                            </View>
                            </View>
                                <View style={{width:'100%', marginLeft:25, marginTop:5}}>
                                    <Text>Periode</Text>
                                </View>
                                <View style={{marginHorizontal:20}}>         
                                <SearchableDropdown
                                    onItemSelect={(item) => {
                                    const items = this.state.selectedItems;
                                    items.push(item)
                                    this.setState({ selectedItems: items , tahunSelect:item.name });
                                    // alert(item.name)
                                    // console.log('thn',item.id)
                                    }}
                                    // defaultIndex= {2}
                                    containerStyle={{ padding: 0 }}
                                    onRemoveItem={(item, index) => {
                                    const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                                    this.setState({ selectedItems: items});
                                    }}
                                    itemStyle={{
                                    padding: 10,
                                    marginTop: 2,
                                    marginLeft:5,
                                    backgroundColor: '#ddd',
                                    borderColor: '#bbb',
                                    borderWidth: 1,
                                    borderRadius: 5,
                                    }}
                                    itemTextStyle={{ color: '#222' }}
                                    itemsContainerStyle={{ maxHeight: 140 }}
                                    items={items}
                                    
                                    textInputProps={
                                    {
                                        placeholder: "--Select Years--",
                                        style: {
                                            marginLeft:0,
                                            padding: 7,
                                            borderWidth: 1,
                                            borderColor: '#ccc',
                                            borderRadius: 5,
                                        },
                                        
                                    }
                                    }
                                    listProps={
                                    {
                                        nestedScrollEnabled: true,
                                    }
                                    }
                                />
                                </View>
                            </View>
                            <View style={{flexDirection:'column', marginTop:5}}>
                                <View style={{width:'100%', marginLeft:25}}>
                                    <Text>Depart/Project</Text>
                                </View>
                                <View style={{marginHorizontal:20}}>
                                    <SearchableDropdown
                                        onItemSelect={(item) => {
                                        const items = this.state.selectedItems;
                                        items.push(item)
                                        this.setState({ selectedItems: items, Id_departIndex: item.id, departIndex: item.name});
                                        // alert(item.id)
                                        }}
                                        onRemoveItem={(item, index) => {
                                        const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                                        this.setState({ selectedItems: items });
                                        }}
                                        itemStyle={{
                                        padding: 10, marginTop: 2,
                                        marginLeft:5, backgroundColor: '#ddd',
                                        borderColor: '#bbb', borderWidth: 1,
                                        borderRadius: 5,
                                        }}
                                        itemTextStyle={{ color: '#222' }}
                                        itemsContainerStyle={{ maxHeight: 140 }}
                                        items={this.state.itemDepart}
                                        // defaultIndex={2}
                                        
                                        textInputProps={
                                        {
                                            placeholder: "--Select Divisi--",
                                            underlineColorAndroid: "transparent",
                                            style: {
                                                marginLeft:0,
                                                padding: 7,
                                                borderWidth: 1,
                                                borderColor: '#ccc',
                                                borderRadius: 5,
                                            },  
                                        }
                                        }
                                        listProps={
                                        {
                                            nestedScrollEnabled: true,
                                        }
                                        }
                                    />
                                </View>
                            </View>
                            <View style={{flexDirection:'column', marginTop:5, marginBottom:20}}>
                                <View style={{width:'100%', marginLeft:25}}>
                                    <Text>Tipe Risk</Text>
                                </View>
                                <View style={{marginHorizontal:20}}>
                                    <SearchableDropdown
                                        onItemSelect={(item) => {
                                        const items = this.state.selectedItems;
                                        items.push(item)
                                        this.setState({ selectedItems: items, id_risk: item.id, targetNameIndex: item.name, targetIndex: item.kode});
                                        // alert(item.kode)
                                        }}
                                        onRemoveItem={(item, index) => {
                                        const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                                        this.setState({ selectedItems: items });
                                        }}
                                        itemStyle={{
                                        padding: 10, marginTop: 2,
                                        marginLeft:5, backgroundColor: '#ddd',
                                        borderColor: '#bbb', borderWidth: 1,
                                        borderRadius: 5,
                                        }}
                                        itemTextStyle={{ color: '#222' }}
                                        itemsContainerStyle={{ maxHeight: 140 }}
                                        items={this.state.tipeRisk}
                                        defaultIndex={2}
                                        
                                        textInputProps={
                                        {
                                            placeholder: "--Select Tipe--",
                                            underlineColorAndroid: "transparent",
                                            style: {
                                                marginLeft:0,
                                                padding: 7,
                                                borderWidth: 1,
                                                borderColor: '#ccc',
                                                borderRadius: 5,
                                            },  
                                        }
                                        }
                                        listProps={
                                        {
                                            nestedScrollEnabled: true,
                                        }
                                        }
                                    />
                                </View>
                            </View>
                            
                            
                        </Card>

                        </Modal> */}


                        {this.state.isLoadingSwipePie?(
                            <ActivityIndicator/>
                        ):(
                            <View style={{marginHorizontal:5}}>
                                <SwipePetaRisk
                                    target = {this.state.targetNameIndex}
                                    id_depart_index = {this.state.Id_departIndex}
                                    // id_proyek = {this.state.id_proyek}
                                    // karyawan = {this.state.karyawan}
                                    // creator = {this.state.creator}
                                    // company = {this.state.company}
                                    // sebagai = {this.state.sebagai}

                //                     
                                />
                                <SwipePie 
                                    id_proyek = {this.state.id_proyek}
                                    id_depart_index = {this.state.Id_departIndex}
                                    sebagai = {this.state.sebagai}
                                    company = {this.state.company}
                                    target = {this.state.targetNameIndex}
                                    tahun = {this.state.tahunSelect}
                                />
                                
                                {/* <TableRiskStatus/> */}
                            </View>
                            
                        )}
                        
                        
                       
                        </View>
                        )}
                        
                        
                        
                        
                    </Content>
                


            </Container>
        )
    }
}