import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    StyleSheet,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Body,
    Title,
    Button,
    CheckBox,
    Content,
    Card
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles2 from "../../../res/styles/Form";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";

export default class TabelPetaRisiko extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoading:true,

            search:'',

            start:0,
            length:10,
            color : '',
            dataTable:{
                ColBar22 : "",
                ColBar23 : "1",
                ColBar24 : "",
                ColBar25 : "3",
                    ColBar32 : "",
                    ColBar33 : "",
                    ColBar34 : "2",
                    ColBar35 : "1",
                ColBar42 : "1",
                ColBar43 : "",
                ColBar44 : "7",
                ColBar45 : "",
                    ColBar52 : "10",
                    ColBar53 : "",
                    ColBar54 : "",
                    ColBar55 : "",
            },
            // tablerisk:[
            //     {
            //         nama_dept : "Strategic Marketing Department",
            //         status : "Draft",
                    
            //     },
            //     {
            //         nama_dept : "Proyek Pembangunan T/L 150KV GI Rengat - GI Tembilahan (Section 5)	",
            //         status : "Submit"
            //     },
            //     {
            //         nama_dept : "Marketing Department",
            //         status : "Review"
            //     },
            //     {
            //         nama_dept : "Tender Administration Department",
            //         status : "Approve 1"
            //     },
            //     {
            //         nama_dept : "HCM & GA Infra 2 Division",
            //         status : "Approve 2"
            //     },
            //     {
            //         nama_dept : "HCM & GA Manager",
            //         status : "Revise"
            //     },

            // ]
            
            
        }
    }

    loadRiskStatus(){
        var url = GlobalConfig.SERVERHOST + 'api/api/get_status';
        var formData = new FormData();
        formData.append("start", this.state.start)
        formData.append("length", this.state.length)
        formData.append("search[value]", this.state.search)
        console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        tablerisk:response.data.data,
                        
                    }, function(){
                        this.setState({
                            isLoading:false
                        })
                    })
                    // console.log('kry', this.state.tablerisk)
                    // AsyncStorage.setItem('data_user', (JSON.stringify(response.data)))
                    // this.AsyncDataUser()

                }else{
                    

                    Alert.alert('Error', 'Tidak bisa load data', [{
                        text: 'Okay'
                    }])
                    this.setState({
                        isLoading:false
                    })
                }
            })
            .catch((error) => {
               
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoading:false
                })
                console.log(error)
            })
         
    }

    onNext(){
        var nilai = this.state.start + 10
        this.setState({
            isLoading:true,
            start:nilai
        }, function(){
            this.loadRiskStatus()
        })

    }
    onPrev(){
        var nilai = this.state.start - 10
        this.setState({
            isLoading:true,
            start:nilai
        }, function(){
            this.loadRiskStatus()
        })

    }
    onSearch(){
        this.setState({
            isLoading:true,
        }, function(){
            this.loadRiskStatus()
        })    
    }


    componentDidMount(){
        this.loadRiskStatus()
    }

    
    

    render(){
        return(
            
            <View style={styles.container}>
            {this.state.isLoading?(
                <View style={{justifyContent:'center',marginTop:10,}}>
                    <ActivityIndicator />
                    </View>
            ):(
                <View style={{alignItems:"center", marginTop:10, marginBottom:20}}>
                    <Card style={{marginTop:10, marginLeft:10, marginRight:10, padding:10, borderBottomLeftRadius:8, borderBottomRightRadius:8, borderTopLeftRadius:8, borderTopRightRadius:8}}>
                        <View style={{flexDirection:'column', marginRight:5}}>
                            <View style={{marginBottom:5, marginLeft:5}}>
                                <Text style={{fontWeight:'bold', fontSize:20}}>Risk Status </Text>
                            </View>
                            <View style={{marginRight:5, flexDirection:'row',}}>
                                <TextInput
                                    style={styles2.fontTextInput}
                                    rowSpan={1}
                                    bordered
                                    placeholderTextColor={colors.gray02}
                                    value={this.state.search}
                                    placeholder="Search ..."
                                    onChangeText={text =>
                                        this.setState({ search: text })
                                    }
                                    />
                                <View style={{marginLeft:5, marginBottom:10, paddingHorizontal:5}}>
                                    <Button
                                        onPress={()=>(this.onSearch())}
                                        style={{ backgroundColor:colors.gray09,  borderRadius:5, height:35, width:85, }}
                                        >
                                       <Text style={{color:colors.white, marginLeft:20, marginRight:20,  fontWeight:'bold'}}>Search</Text>
                                                    
                                    </Button>
                                </View>  
                            </View>
                            
                        </View>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:0,
                            backgroundColor: colors.bgErm,
                            borderBottomWidth:1
                           
                            }}>
                            <View style={{ flex:5, width:'100%', justifyContent:'center', borderRightWidth:1, borderLeftWidth:1, borderTopWidth:1}}>
                                <View style={{marginLeft:5, justifyContent:'center', alignItems:'center'}}>
                                    <Text style={{ color:colors.gray09, fontSize:15,marginVertical:10, }}>
                                        Nama Departemen
                                    </Text>
                                </View>
                            </View>
                            <View style={{ flex:3, width:'100%', alignItems:'center', justifyContent:'center', borderLeftWidth:0, borderRightWidth:1, borderTopWidth:1}}>
                                <View style={{marginLeft:5, justifyContent:'center', alignItems:'center'}}>
                                    <Text style={{ color:colors.gray09, fontSize:15,marginVertical:10, }}>
                                        Status
                                    </Text>
                                </View>
                            </View>
                        </View>
                        {this.state.tablerisk.map((data, index) => (
                            <View key={index} style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:0,
                            backgroundColor: 'white',
                            borderTopWidth:1
                            }}>
                            <View style={{ flex:5, width:'100%', justifyContent:'center',borderLeftWidth:1, borderRightWidth:1}}>
                                <View style={{marginLeft:5, marginLeft:5, justifyContent:'center'}}>
                                    <ScrollView horizontal={true}>
                                        <Text style={{ color:'#454545', fontSize:12,marginVertical:10, }}>
                                           {data.nama_departement}
                                        </Text>
                                    </ScrollView>
                                    
                                </View>
                                
                            </View>
                            <View style={{ flex:3, width:'100%', alignItems:'center', justifyContent:'center', borderRightWidth:1}}>
                                <View style={{backgroundColor:data.color, width:'50%', paddingVertical:5, borderRadius:2, alignItems:'center' }}>
                                    <Text style={{ color:'white', fontSize:13, fontWeight:'bold'}}>
                                        {data.status}
                                    </Text>
                                </View>    
                            </View>
                        </View>
                        ))}
                        <View style={{borderTopWidth:1, marginRight:10}}/>
                        <View style={{ alignItems:'flex-end', marginRight:10}}>
                            <View style={{flexDirection:'row', marginTop:10, paddingHorizontal:0,}}>
                                <Button
                                    onPress={()=>(this.onPrev())}
                                        style={{width:'22%', flexDirection:'row', alignItems:'center', marginRight:0, backgroundColor:colors.gray05,borderWidth:0.5, borderColor:'black', borderRadius:0, alignItems:'center', marginBottom: 5, paddingHorizontal:5, height:35}}
                                        >
                                        
                                        <View style={{width:'100%', marginLeft:2, alignItems:'center'}}>
                                            <Text style={{ fontSize:14}}>Previous</Text>
                                        </View>
                                            
                                </Button>
                                <Button
                                    onPress={()=>(this.onNext())}
                                        style={{width:'20%', flexDirection:'row', alignItems:'center', backgroundColor:colors.gray05, borderWidth:0.5, borderColor:'black', borderRadius:0, alignItems:'center', marginBottom: 5, paddingHorizontal:5, height:35}}
                                        >
                                        
                                        <View style={{width:'100%', alignItems:'center'}}>
                                            <Text style={{  fontSize:14}}>Next</Text>
                                        </View>
                                            
                                </Button>
                                
                            </View> 
                        </View>
                          
                        
                    </Card> 
                </View>
            )}
                
            </View>
            
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
     
      paddingVertical:0,
      paddingHorizontal:0,
      borderRadius:0,
      
    },
    
    Button:{
        backgroundColor:null
    },
    
  });