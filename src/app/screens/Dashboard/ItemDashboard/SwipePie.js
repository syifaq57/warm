import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    processColor,
    
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles2 from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Ionicons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";
import { BarChart, LineChart, PieChart } from 'react-native-charts-wrapper'
import Swiper from 'react-native-swiper';
import ItemDetailPie from "./ItemDetailPie";
import ItemDetailPieStatus from "./ItemDetailPieStatus";

export default class SwipePie extends Component {
    constructor(props){
        super(props);
        this.state={
            index:0,
            // isLoadingPieN:true,
            isLevelDownSide:false,
            isLevelUpSide:false,
            isStatusDownSide:false,
            isStatusUpSide:false,

            isTingkatDown:false,
            

            positif:'y',
            negative:'n',

            listPieNN:[],
            listPieNP:[],
            listPieSN:[],
            listPieSP:[],
            

            //pieTingkatrisiko
            values:[],
            
            colors:[processColor('#28B131'), processColor('#FFC600'), processColor('#FA7200'), processColor('#FF0000')],
            colors2:[processColor('#7CB5EC'), processColor('#434348')],
            // abaikan
            pie: {
                title: 'Favorite Food in Jogja',
                detail: { 
                time_value_list: [2017],
                legend_list: ['Hamburger', 'Steak', 'Pecel', 'Magelangan'],
                dataset: {
                    Hamburger: { '2017': 9 },
                    Steak: { '2017': 17 },
                    Pecel: { '2017': 29 },
                    Magelangan: { '2017': 45 }
                }
                }
            },//abaikan

            //pieTingkatrisiko
            values2:[],
            values3:[],
            values4:[]
            

        }
    }

    getRandomColor () {
        var letters = '0123456789ABCDEF'
        var color = '#'
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)]
        }
        return color
      }
    
      renderPieRisikoNegatif () {
        const time = this.state.pie.detail.time_value_list
        const legend = this.state.pie.detail.legend_list
        const dataset = this.state.pie.detail.dataset
    
        var dataSetsValue = []
        var dataStyle = {}
        var legendStyle = {}
        var descStyle = {}
        var xAxisStyle = {}
        var chooseStyle = {}
        var valueLegend = []
        var colorLegend = []
    
        legend.map((legendValue) => {
          time.map((timeValue) => {
            const datasetValue = dataset[legendValue]
            const datasetTimeValue = datasetValue[timeValue]
    
            valueLegend.push({ value: parseInt(datasetTimeValue), label: legendValue })
          })
          colorLegend.push(processColor(this.getRandomColor()))
        })
    
        const datasetObject = {
          values: this.state.values,
          label: '',
          config: {
            colors: this.state.colors,
            valueTextSize: 15,
            valueTextColor: processColor('black'),
            sliceSpace: 2,
            selectionShift: 13,
            valueFormatter: "#",
            
            
          }
        }
        dataSetsValue.push(datasetObject)
        // console.log(dataSetsValue)
        legendStyle = {
          enabled: false,
          textSize: 12,
          form: 'CIRCLE',
          position: 'BELOW_CHART_RIGHT',
          wordWrapEnabled: true
        }
        dataStyle = {
          dataSets: dataSetsValue
        }
        descStyle = {
          text: '',
          textSize: 10,
          textColor: 'black'
        }
        animation = {
            durationX: 2000,
            durationY: 2,
            easingX: 'y',
            easingY: 'y'
        }
        
    
        return (
          <PieChart
            
            drawEntryLabels={false}
            style={styles.bar}
            chartDescription={descStyle}
            data={dataStyle}
            legend={legendStyle}
            highlights={[{ x: 3 }]}
            holeRadius={5}
            // animation={animation}
            transparentCircleRadius={20}
            entryLabelTextSize={0}
            // zeroLine={zeroline}
            onSelect={()=>this.selectPieRisikoNegatif()}
            
            />
        )
      }

      selectPieRisikoNegatif(){
          console.log('wkwkwkwk', this.state.values[0].value)
      }

      renderPieRisikoPositif () {
        const time = this.state.pie.detail.time_value_list
        const legend = this.state.pie.detail.legend_list
        const dataset = this.state.pie.detail.dataset
    
        var dataSetsValue = []
        var dataStyle = {}
        var legendStyle = {}
        var descStyle = {}
       
        var valueLegend = []
        var colorLegend = []
    
        legend.map((legendValue) => {
          time.map((timeValue) => {
            const datasetValue = dataset[legendValue]
            const datasetTimeValue = datasetValue[timeValue]
    
            valueLegend.push({ value: parseInt(datasetTimeValue), label: legendValue })
          })
          colorLegend.push(processColor(this.getRandomColor()))
        })
    
        const datasetObject = {
          values: this.state.values2,
          label: '',
          config: {
            colors: this.state.colors,
            valueTextSize: 15,
            valueTextColor: processColor('black'),
            sliceSpace: 2,
            selectionShift: 13,
            valueFormatter: "#",
            
            
          }
        }
        dataSetsValue.push(datasetObject)
        // console.log(dataSetsValue)
        legendStyle = {
          enabled: false,
          textSize: 12,
          form: 'CIRCLE',
          position: 'BELOW_CHART_RIGHT',
          wordWrapEnabled: true
        }
        dataStyle = {
          dataSets: dataSetsValue
        }
        descStyle = {
          text: '',
          textSize: 10,
          textColor: 'black'
        }
    
        return (
          <PieChart
            drawEntryLabels={false}
            style={styles.bar}
            chartDescription={descStyle}
            data={dataStyle}
            legend={legendStyle}
            highlights={[{ x: 3 }]}
            holeRadius={5}
            transparentCircleRadius={20}
            entryLabelTextSize={0}
            
            />
        )
      }

      renderPieStatusNegatif () {
        const time = this.state.pie.detail.time_value_list
        const legend = this.state.pie.detail.legend_list
        const dataset = this.state.pie.detail.dataset
    
        var dataSetsValue = []
        var dataStyle = {}
        var legendStyle = {}
        var descStyle = {}
       
        var valueLegend = []
        var colorLegend = []
    
        legend.map((legendValue) => {
          time.map((timeValue) => {
            const datasetValue = dataset[legendValue]
            const datasetTimeValue = datasetValue[timeValue]
    
            valueLegend.push({ value: parseInt(datasetTimeValue), label: legendValue })
          })
          colorLegend.push(processColor(this.getRandomColor()))
        })
    
        const datasetObject = {
          values: this.state.values3,
          label: '',
          config: {
            colors: this.state.colors2,
            valueTextSize: 15,
            valueTextColor: processColor('black'),
            sliceSpace: 2,
            selectionShift: 13,
            valueFormatter: "#",
            
            
          }
        }
        dataSetsValue.push(datasetObject)
        // console.log(dataSetsValue)
        legendStyle = {
          enabled: false,
          textSize: 12,
          form: 'CIRCLE',
          position: 'BELOW_CHART_RIGHT',
          wordWrapEnabled: true
        }
        dataStyle = {
          dataSets: dataSetsValue
        }
        descStyle = {
          text: '',
          textSize: 10,
          textColor: 'black'
        }
    
        return (
          <PieChart
            drawEntryLabels={false}
            style={styles.bar}
            chartDescription={descStyle}
            data={dataStyle}
            legend={legendStyle}
            highlights={[{ x: 3 }]}
            holeRadius={5}
            transparentCircleRadius={20}
            
            
            />
        )
      }

      renderPieStatusPositif () {
        const time = this.state.pie.detail.time_value_list
        const legend = this.state.pie.detail.legend_list
        const dataset = this.state.pie.detail.dataset
    
        var dataSetsValue = []
        var dataStyle = {}
        var legendStyle = {}
        var descStyle = {}
       
        var valueLegend = []
        var colorLegend = []
    
        legend.map((legendValue) => {
          time.map((timeValue) => {
            const datasetValue = dataset[legendValue]
            const datasetTimeValue = datasetValue[timeValue]
    
            valueLegend.push({ value: parseInt(datasetTimeValue), label: legendValue })
          })
          colorLegend.push(processColor(this.getRandomColor()))
        })
    
        const datasetObject = {
          values: this.state.values4,
          label: '',
          config: {
            colors: this.state.colors2,
            valueTextSize: 15,
            valueTextColor: processColor('black'),
            sliceSpace: 2,
            selectionShift: 13,
            valueFormatter: "#",
            
            
          }
        }
        dataSetsValue.push(datasetObject)
        // console.log(dataSetsValue)
        legendStyle = {
          enabled: false,
          textSize: 12,
          form: 'CIRCLE',
          position: 'BELOW_CHART_RIGHT',
          wordWrapEnabled: true
        }
        dataStyle = {
          dataSets: dataSetsValue
        }
        descStyle = {
          text: '',
          textSize: 10,
          textColor: 'black'
        }
    
        return (
          <PieChart
            
            drawEntryLabels={false}
            style={styles.bar}
            chartDescription={descStyle}
            data={dataStyle}
            legend={legendStyle}
            highlights={[{ x: 3 }]}
            holeRadius={5}
            transparentCircleRadius={20}
            entryLabelTextSize={0}
            
            />
        )
      }

    onRunTingkatRisikoNegatif(){
        var url = GlobalConfig.SERVERHOST + 'api/api/get_risk_by_level';
        var formData = new FormData();
        formData.append("positif",'n')
        formData.append("target", this.state.target)
        for(var j=0; j<this.state.id_proyek.length; j++){
            formData.append("bagian_proyek["+j+"]",this.state.id_proyek[j].id_proyek)
        }
        // formData.append("muk_kode", this.state.muk_kode)
        
        formData.append("company", this.state.company)
        formData.append("tahun", this.state.tahun)
        formData.append("sebagai", this.state.sebagai)
    
        console.log('runtignkat',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                // console.log('dta', response.data)
                if(response.success == true){
                    var pie = this.state.listPieNN
                    for(var i=0; i<response.data.length; i++){
                        pie.push({
                            value:response.data[i].y,
                            id_dh:response.data[i].id_dh,
                            label:response.data[i].name,
                            color:response.data[i].color
                        })
                    }
                    
                
                    this.setState({
                        values:pie,
                        isLoadingPieN:false
                    })
                    // console.log('vluspie',pie)
                   
                }
                
            })
            .catch((error) => {
                this.setState({
                    isLoadingPieN:false
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
            
    }
    onRunTingkatRisikoPositif(){
        var url = GlobalConfig.SERVERHOST + 'api/api/get_risk_by_level';
        var formData = new FormData();
        formData.append("positif",'y')
        formData.append("target", this.state.target)
        for(var j=0; j<this.state.id_proyek.length; j++){
            formData.append("bagian_proyek["+j+"]",this.state.id_proyek[j].id_proyek)
        }
        // formData.append("muk_kode", this.state.muk_kode)
        formData.append("company", this.state.company)
        formData.append("tahun", this.state.tahun)
        formData.append("sebagai", this.state.sebagai)
    
        console.log('RskN 2',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                console.log('positif after', response.data)
                if(response.success == true){
                    var pieP = this.state.listPieNP
                    for(var i=0; i<response.data.length; i++){
                        pieP.push({
                            value:response.data[i].y,
                            id_dh:response.data[i].id_dh,
                            label:response.data[i].name,
                            color:response.data[i].color
                        })
                    }
                    
                
                    this.setState({
                        values2:pieP,
                        isLoadingPieP:false
                    })
                    console.log('vluspie positif',pieP)
                }
            })
            .catch((error) => {
                this.setState({
                    isLoadingPieP:false
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }
    
    onRunStatusNegatif(){
        var url = GlobalConfig.SERVERHOST + 'api/api/get_risk_by_status';
        var formData = new FormData();
        formData.append("positif", 'n')
        for(var j=0; j<this.state.id_proyek.length; j++){
            formData.append("bagian_proyek["+j+"]",this.state.id_proyek[j].id_proyek)
        }

        // formData.append("muk_kode", this.state.muk_kode)
        formData.append("company", this.state.company)
        formData.append("tahun", this.state.tahun)
        formData.append("sebagai", this.state.sebagai)
    
        // console.log('SN',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                // console.log('dta', response.data)
                if(response.success == true){
                    var pieSN = this.state.listPieSN
                    for(var i=0; i<response.data.length; i++){
                        pieSN.push({
                            value:response.data[i].y,
                            // status:response.data[i].status,
                            label:response.data[i].name,
                            color:response.data[i].color
                        })
                    }
                    
                
                    this.setState({
                        values3:pieSN,
                        isLoadingPieSN:false
                        
                    })
                    // console.log('vluspie',pieSN)
                   
                }
                
            })
            .catch((error) => {
                this.setState({
                    isLoadingPieSN:false
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                // console.log(error)
            })
            
    }
    onRunStatusPositif(){
        var url = GlobalConfig.SERVERHOST + 'api/api/get_risk_by_status';
        var formData = new FormData();
        formData.append("positif", 'y')
        for(var j=0; j<this.state.id_proyek.length; j++){
            formData.append("bagian_proyek["+j+"]",this.state.id_proyek[j].id_proyek)
        }
        // formData.append("muk_kode", this.state.muk_kode)
        formData.append("company", this.state.company)
        formData.append("tahun", this.state.tahun)
        formData.append("sebagai", this.state.sebagai)
    
        // console.log('RskN',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                // console.log('dta', response.data)
                if(response.success == true){
                    var pieSP = this.state.listPieSP
                    for(var i=0; i<response.data.length; i++){
                        pieSP.push({
                            value:response.data[i].y,
                            // status:response.data[i].status,
                            label:response.data[i].name,
                            color:response.data[i].color
                        })
                    }
                    
                
                    this.setState({
                        values4:pieSP,
                        isLoadingPieSP:false
                    })
                    console.log('vluspie',pieSP)
                }
            })
            .catch((error) => {
                this.setState({
                    isLoadingPieSP:false
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    componentDidMount(){
        // console.log('tes reload')
        this.setState({
            isLoadingPieN:true,
            isLoadingPieP:true,
            isLoadingPieSN:true,
            isLoadingPieSP:true,
            id_proyek:this.props.id_proyek,
            bagian_proyek:this.props.id_depart_index,
            company:this.props.company,
            sebagai:this.props.sebagai,
            target:this.props.target,
            tahun:this.props.tahun,
            muk_kode:this.props.id_depart_index,

        },function(){
            this.onRunTingkatRisikoNegatif()
            this.onRunTingkatRisikoPositif()
            this.onRunStatusNegatif()
            this.onRunStatusPositif()
        })
        
        
        // this.setState({
        //     listPieNN :this.props.listPieNN

        // },function(){
        //     console.log('listpie', this.state.listPieNN)
        // })
        
    }

    

    toggleLevelUpside = () => {
        
        this.setState({ isLevelUpSide: !this.state.isLevelUpSide });

      };
      toggleLevelDownside = () => {
        
        this.setState({ isLevelDownSide: !this.state.isLevelDownSide });

      };
      toggleStatusUpSide = () => {
        
        this.setState({ isStatusUpSide: !this.state.isStatusUpSide });

      };
      toggleStatusDownSide = () => {
        
        this.setState({ isStatusDownSide: !this.state.isStatusDownSide });

      };
    

    onPressLevelDown(data){
        this.setState({ 
            isLevelDownSide: !this.state.isLevelDownSide,
            id_dh:data
        })
        console.log('isok')
    }  
    onPressLevelUp(data){
        this.setState({ 
            isLevelUpSide: !this.state.isLevelUpSide,
            id_dh:data
        })
        console.log('isok')
    }  
    onPressStatusUp(data){
        
        
        this.setState({ 
            isStatusUpSide: !this.state.isStatusUpSide,
            status:data.toLowerCase()
        })
        console.log('isok')
    }  
    onPressStatusDown(data){

        this.setState({ 
            isStatusDownSide: !this.state.isStatusDownSide,
            status:data.toLowerCase()
        })
        console.log('isok')
    }  

    toggleTingkatDown = () => {
        this.setState({ isTingkatDown: !this.state.isTingkatDown });
      };
    toggleTingkatUp = () => {
        this.setState({ isTingkatUp: !this.state.isTingkatUp });
    };  
    toggleStatusDown = () => {
        this.setState({ isStatusDown: !this.state.isStatusDown });
    };     
    toggleStatusUp = () => {
        this.setState({ isStatusUp: !this.state.isStatusUp });
    };     
    
    render(){
        return(
            <Card style={{marginTop:10, marginLeft:10, marginRight:10,}}>                           
                
                
                <Swiper 
                    style={styles.wrapper} 
                    height='100%' 
                    showsButtons 
                    buttonWrapperStyle={{
                        backgroundColor: 'transparent',
                        flexDirection: 'row',
                        position: 'absolute',
                        top: -13, left: 0, 
                        flex: 1, 
                        
                        paddingHorizontal: -2, 
                        paddingVertical: 0, 
                        justifyContent: 'space-between',
                        paddingBottom:5,
                        }}
                    >
                 <View style={styles.slide}>
                    <View style={{flexDirection:'column', marginTop:0}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,  
                            paddingVertical:15                         
                        }}>
                            <View style={{ flex:4, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:15}}>
                                    RISK BY TINGKAT RISIKO DOWNSIDE
                                </Text>
                            </View>
                            
                        </View>
                    </View>
                    {this.state.isLoadingPieN?(
                        <ActivityIndicator/>
                    ):(
                        <View style={styles.container}>  
                        <View style={{flex:1, flexDirection:"row"}}>
                        
                        {this.renderPieRisikoNegatif()}
                        <View style={styles.legend}>
                        {this.state.values.map((data, index) => (
                            
                            <TouchableOpacity onPress={()=>this.onPressLevelDown(data.id_dh)} key={index} >
                            <View style={{flexDirection:"row", marginBottom:15}}>
                                <View style={{width:10,height:10,backgroundColor:data.color, alignSelf:"center"}}/>  
                                <View style={{marginLeft:5}}>
                                    <Text style={{fontSize:12}}>{data.label}</Text>
                                </View>    
                            </View>
                                
                            </TouchableOpacity>
                            
                        ))}
                        
                        </View>
                        </View>
                        
                    </View>
                    )}
                    
                 </View>
                

                 

                 <View style={styles.slide1}>
                    <View style={{flexDirection:'column', marginTop:0}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,  
                            paddingVertical:15                         
                        }}>
                            <View style={{ flex:4, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:15}}>
                                    RISK BY TINGKAT RISIKO UPSIDE
                                </Text>
                            </View>
                            
                        </View>
                    </View>
                    {this.state.isLoadingPieP?(
                        <ActivityIndicator/>
                    ):(
                        <View style={styles.container}>  
                            <View style={{flex:1, flexDirection:"row"}}>
                                {this.renderPieRisikoPositif()}
                                <View style={styles.legend2}>
                                {this.state.values2.map((data, index) => (
                                    <TouchableOpacity onPress={()=>this.onPressLevelUp(data.id_dh)} key={index} >
                                        <View style={{flexDirection:"row", marginBottom:15}}>
                                            <View style={{width:10,height:10,backgroundColor:data.color, alignSelf:"center"}}/>  
                                            <View style={{marginLeft:5}}>
                                                <Text style={{fontSize:12}}>{data.label}</Text>
                                            </View>    
                                        </View>
                                        
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </View>
                        
                    </View>
                    )}
                 </View>
                 <View style={styles.slide1}>
                    <View style={{flexDirection:'column', marginTop:0}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,  
                            paddingVertical:15                         
                        }}>
                            <View style={{ flex:4, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:15}}>
                                    RISK BY STATUS DOWNSIDE
                                </Text>
                            </View>
                            
                        </View>
                    </View>
                    {this.state.isLoadingPieSN?(
                        <ActivityIndicator/>
                    ):(
                        <View style={styles.container}>  
                        <View style={{flex:1, flexDirection:"row"}}>
                        
                        {this.renderPieStatusNegatif()}
                        <View style={styles.legend}>
                        {this.state.values3.map((data, index) => (
                            <TouchableOpacity key={index} onPress={()=>this.onPressStatusDown(data.label)} >
                            <View style={{flexDirection:"row", marginBottom:15}}>
                                <View style={{width:10,height:10,backgroundColor:data.color, alignSelf:"center"}}/>  
                                <View style={{marginLeft:10}}>
                                <Text styles={{fontSize:8}}>{data.label}</Text>
                                </View>    
                            </View>
                                
                            </TouchableOpacity>
                        ))}
                        </View>
                        </View>
                        
                    </View>
                    )}
                    
                 </View>

                 <View style={styles.slide1}>
                    <View style={{flexDirection:'column', marginTop:0}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,  
                            paddingVertical:15                         
                        }}>
                            <View style={{ flex:4, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:15}}>
                                    RISK BY STATUS UPSIDE
                                </Text>
                            </View>
                            
                        </View>
                    </View>
                    {this.state.isLoadingPieSP?(
                        <ActivityIndicator/>
                    ):(
                        <View style={styles.container}>  
                        <View style={{flex:1, flexDirection:"row"}}>
                        
                        {this.renderPieStatusPositif()}
                        <View style={styles.legend}>
                        {this.state.values4.map((data, index) => (
                            <TouchableOpacity key={index} onPress={()=>this.onPressStatusUp(data.label)} >
                            <View style={{flexDirection:"row", marginBottom:15}}>
                                <View style={{width:10,height:10,backgroundColor:data.color, alignSelf:"center"}}/>  
                                <View style={{marginLeft:10}}>
                                <Text styles={{fontSize:8}}>{data.label}</Text>
                                </View>    
                            </View>
                                
                            </TouchableOpacity>
                        ))}
                        </View>
                        </View>
                        
                    </View>
                    )}
                    
                 </View>

                </Swiper>

                {/* risikio level downside */}
                <Modal
                
                    style={{marginHorizontal:0,marginBottom:0,marginTop:0,}}
                    isVisible={this.state.isLevelDownSide}
                    onBackdropPress={this.toggleLevelDownside}
                    onBackButtonPress={this.toggleLevelDownside}
                >
                    <View style={{flex:1, justifyContent:'center',}}>
                            <View style={{backgroundColor:"white",height:"100%",  marginHorizontal:'0%', borderRadius:5}}>
                                    <View style={{backgroundColor:colors.bgErm, }}>
                                        <View style={{flexDirection:'row', marginLeft:-20, paddingVertical:15, marginTop:Platform.OS === "ios" ? 20 :0 }}>
                                            <View style={{flex:1, marginRight:15, justifyContent:'center'}}>
                                            <TouchableOpacity
                                                onPress={this.toggleLevelDownside}
                                                style={{alignItems:'flex-end'}}
                                            >
                                            <Icon
                                                name='md-arrow-back'
                                                size={25}
                                                color={colors.gray09}
                                                />
                                            </TouchableOpacity>                
                                    </View>
                                    <View style={{flex:6, justifyContent:'center'}}>
                                        <Text style={{color:colors.gray09, fontWeight:'bold', fontSize:17}}>Detail by Tingkat Risiko Downside</Text>
                                    </View>
                                    
                                </View>        
                            </View>
                            <ScrollView>
                                <ItemDetailPie
                                    id_depart_index = {this.props.id_depart_index}
                                    positif={this.state.negative}
                                    target={this.props.target}
                                    id_dh={this.state.id_dh}
                                    
                                />
                            </ScrollView>        
                        </View>
                    </View>
                </Modal>
                {/* risikio level upside */}
                <Modal
                    style={{marginHorizontal:0,marginBottom:0,marginTop:0,}}
                    isVisible={this.state.isLevelUpSide}
                    onBackdropPress={this.toggleLevelUpside}
                    onBackButtonPress={this.toggleLevelUpside}
                >
                    <View style={{flex:1, justifyContent:'center',}}>
                            <View style={{backgroundColor:"white",height:"100%",  marginHorizontal:'0%', borderRadius:5}}>
                                    <View style={{backgroundColor:colors.bgErm, }}>
                                        <View style={{flexDirection:'row', marginLeft:-20, paddingVertical:15, marginTop:Platform.OS === "ios" ? 20 :0 }}>
                                            <View style={{flex:1, marginRight:15, justifyContent:'center'}}>
                                            <TouchableOpacity
                                                onPress={this.toggleLevelUpside}
                                                style={{alignItems:'flex-end'}}
                                            >
                                            <Icon
                                                name='md-arrow-back'
                                                size={25}
                                                color={colors.gray09}
                                                />
                                            </TouchableOpacity>                
                                    </View>
                                    <View style={{flex:6, justifyContent:'center'}}>
                                        <Text style={{color:colors.gray09, fontWeight:'bold', fontSize:17}}>Detail by Tingkat Risiko Upside</Text>
                                    </View>
                                    
                                </View>        
                            </View>
                            <ScrollView>
                                <ItemDetailPie
                                    id_depart_index = {this.props.id_depart_index}
                                    positif={this.state.positif}
                                    target={this.props.target}
                                    id_dh={this.state.id_dh}
                                    
                                />
                            </ScrollView>        
                        </View>
                    </View>
                </Modal> 
                 {/* risikio Status downside */}
                <Modal
                    style={{marginHorizontal:0,marginBottom:0,marginTop:0,}}
                    isVisible={this.state.isStatusDownSide}
                    onBackdropPress={this.toggleStatusDownSide}
                    onBackButtonPress={this.toggleStatusDownSide}
                >
                    <View style={{flex:1, justifyContent:'center',}}>
                            <View style={{backgroundColor:"white",height:"100%",  marginHorizontal:'0%', borderRadius:5}}>
                                    <View style={{backgroundColor:colors.bgErm, }}>
                                        <View style={{flexDirection:'row', marginLeft:-20, paddingVertical:15, marginTop:Platform.OS === "ios" ? 20 :0 }}>
                                            <View style={{flex:1, marginRight:15, justifyContent:'center'}}>
                                            <TouchableOpacity
                                                onPress={this.toggleStatusDownSide}
                                                style={{alignItems:'flex-end'}}
                                            >
                                            <Icon   
                                                name='md-arrow-back'
                                                size={25}
                                                color={colors.gray09}
                                                />
                                            </TouchableOpacity>                
                                    </View>
                                    <View style={{flex:6, justifyContent:'center'}}>
                                        <Text style={{color:colors.gray09, fontWeight:'bold', fontSize:17}}>Detail by Status Risiko Downside</Text>
                                    </View>
                                    
                                </View>        
                            </View>
                            <ScrollView>
                                <ItemDetailPieStatus
                                    id_depart_index = {this.props.id_depart_index}
                                    positif={this.state.negative}
                                    
                                    status={this.state.status}
                                    
                                />
                            </ScrollView>        
                        </View>
                    </View>
                </Modal>   

                 {/* risikio Status upside */}
                 <Modal
                    style={{marginHorizontal:0,marginBottom:0,marginTop:0,}}
                    isVisible={this.state.isStatusUpSide}
                    onBackdropPress={this.toggleStatusUpSide}
                    onBackButtonPress={this.toggleStatusUpSide}
                >
                   <View style={{flex:1, justifyContent:'center',}}>
                            <View style={{backgroundColor:"white",height:"100%",  marginHorizontal:'0%', borderRadius:5}}>
                                    <View style={{backgroundColor:colors.bgErm, }}>
                                        <View style={{flexDirection:'row', marginLeft:-20, paddingVertical:15, marginTop:Platform.OS === "ios" ? 20 :0 }}>
                                            <View style={{flex:1, marginRight:15, justifyContent:'center'}}>
                                            <TouchableOpacity
                                                onPress={this.toggleStatusUpSide}
                                                style={{alignItems:'flex-end'}}
                                            >
                                            <Icon
                                                name='md-arrow-back'
                                                size={25}
                                                color={colors.gray09}
                                                />
                                            </TouchableOpacity>                
                                    </View>
                                    <View style={{flex:6, justifyContent:'center'}}>
                                        <Text style={{color:colors.gray09, fontWeight:'bold', fontSize:17}}>Detail by Status Risiko Upside</Text>
                                    </View>
                                    
                                </View>        
                            </View>
                            <ScrollView>
                                <ItemDetailPieStatus
                                    id_depart_index = {this.props.id_depart_index}
                                    positif={this.state.positif}
                                    
                                    status={this.state.status}
                                    
                                />
                            </ScrollView>        
                        </View>
                    </View>
                </Modal>                                 
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height:"50%",
        paddingVertical:10,
        paddingHorizontal:0,
        borderRadius:0,
        marginTop:0
      },
      bar: {
        flex:2,
        marginTop: 10,
        justifyContent:"center",
        alignSelf:"center",
        marginLeft:0,
        height: Dimensions.get('window').height / 3.2,
        width: Dimensions.get('window').width,
        
    },
    legend:{
        flex:1,
        flexDirection:"column",
        justifyContent:"center",
        marginTop:35,
        marginLeft:-10,
        marginBottom:10,
        marginRight:10,
      },
      legend2:{
        flex:1,
        flexDirection:"column",
        justifyContent:"center",
        marginTop:35,
        marginRight:20,
        marginLeft:0,
        marginBottom:10,
      },
    wrapper: {
    },
    slide1: {
      flex: 1,
      
      paddingHorizontal:0,
      backgroundColor: 'white',
      borderTopRightRadius:0,
      borderTopLeftRadius:0
    },
    slide2: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:8,
      borderTopLeftRadius:8
    },
   
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    }
  })

//   AppRegistry.registerComponent('SwipePie', () => SwipePie);