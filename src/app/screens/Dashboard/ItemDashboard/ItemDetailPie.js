import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";

import { TabView, SceneMap, TabBar } from 'react-native-tab-view';


export default class ItemDetailPie extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoadingDetail:false,
            visibleKosong:false,
            nowYear:'',
            idDampak:'',
            listDetail:[],
            DataNegatif:[
                {
                    Title : "Penghentian pekerjaan oleh masyarakat sekitar proyek",
                    Divisi : "Project",
                    JenisRisiko : "K3 & Lingkungan Sosial",
                    Target : "Progress proyek bulan depan",
                    UnitKerja : "Apartemen Solterra",
                    Kemungkinan : "3",
                    BesarAkibat : "3",
                },
                {
                    Title : "Keterlambatan penyelesaian proyek",
                    Divisi : "Project",
                    JenisRisiko : "Konstruksi dan Manajemen Proyek",
                    Target : "Progress proyek bulan depan",
                    UnitKerja : "Apartemen Solterra",
                    Kemungkinan : "2",
                    BesarAkibat : "3",
                },
                {
                    Title : "Penghentian pekerjaan oleh masyarakat sekitar proyek",
                    Divisi : "Project",
                    JenisRisiko : "K3 & Lingkungan Sosial",
                    Target : "Progress proyek bulan depan",
                    UnitKerja : "Apartemen Solterra",
                    Kemungkinan : "3",
                    BesarAkibat : "3",
                },
                {
                    Title : "Keterlambatan penyelesaian proyek",
                    Divisi : "Project",
                    JenisRisiko : "Konstruksi dan Manajemen Proyek",
                    Target : "Progress proyek bulan depan",
                    UnitKerja : "Apartemen Solterra",
                    Kemungkinan : "2",
                    BesarAkibat : "3",
                }
            
            ]
        }
    }

    

    loadDetailPie(){
        this.setState({
            isLoadingDetail:true
        })
        var url = GlobalConfig.SERVERHOST + 'api/detail/get_detail_risk_by_level';
        var formData = new FormData();
        formData.append("positif", this.state.positif)
        formData.append("id_dh", this.props.id_dh)
        formData.append("tahun", this.state.nowYear)
        formData.append("target", this.state.target)
        formData.append("company", this.state.company)
        formData.append("muk_kode", this.props.id_depart_index)
        formData.append("sebagai", this.state.sebagai)
        // for(var i=0; i<this.state.id_proyek.length; i++){
        //      formData.append("bagian_proyek["+i+"]", this.state.id_proyek[i].id_proyek)
        //  }
        
        console.log('tes detail',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                if(response.success == false){
                    this.setState({
                        visibleKosong:true
                    })
                }else{
                    console.log(response)
                var listRisk = this.state.listDetail
                    for(var i=0; i<response.length; i++){
                        listRisk.push({
                            nama_risiko:response[i].nama_risiko,
                            nama_main:response[i].nama_main,
                            nama_sub:response[i].nama_sub,
                            nama_kpi: response[i].nama_kpi,
                            nama_dept: response[i].nama_dept,
                            nama_divisi:response[i].nama_divisi,
                            kemungkinan:response[i].kemungkinan,
                            akibat:response[i].akibat,
                            total:response[i].total

                        })
                        // console.log('tes risk detail', listRisk)
                    }
                this.setState({
                    listDetail:listRisk,
                },function(){
                    this.setState({isLoadingDetail:false})
                    // console.log('tes respon data', this.state.listDetail.map((data)=>(data.nama_dept)))
                })
                }
                
                
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingDetail:false
                })
                console.log(error)
            })
         
    }


    loadDataUser(){
        
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        
                        id_proyek:response.data.bagian_proyek,
                        company:response.data.company,
                        sebagai:response.data.sebagai
                    },function(){
                        this.loadDetailPie()
                    })
                    console.log('tes idpry', this.state.id_proyek)
                    
                    // AsyncStorage.setItem('data_user', (JSON.stringify(response.data)))
                    // this.AsyncDataUser()

                }else{
                    
                    this.setState({
                        isLoadingAll:false
                    })
                    Alert.alert('Error', 'Tidak bisa load data', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingAll:false
                })
                console.log(error)
            })
         
    }
    
    componentDidMount(){
        var years = new Date();
        var year = years.getFullYear();
        this.setState({
            nowYear:year,
            column:this.props.column,
            row:this.props.row,
            positif:this.props.positif,
            target:this.props.target,
            idDampak:this.props.id_depart_index
        },function(){
            console.log('id Dampak w', this.state.idDampak)
            AsyncStorage.getItem('username').then((value)=>{
            this.setState({
                username: value
            })
        })
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password: value
            },function(){
                this.loadDataUser()
                
            })
        })
        })
        
        
        
        // this.loadDetail()
    }
    
    
    render(){
        return(
            <View style={{marginVertical:10}}>
            {this.state.visibleKosong?
                <View style={{alignItems:'center', marginTop:10}}>
                <Text style={{color:colors.gray07}}>
                    Data Kosong
                </Text>
            </View>:null 
            }
            
                {this.state.listDetail.map((data,index)=>(
                    <Card key={index} style={{marginTop:10, backgroundColor:'#D9F2FC', marginLeft:10, marginRight:10}}>                           
                        <ScrollView >
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:1, flexDirection:'column'}}>
                                
                                <ScrollView horizontal={true}>
                                <View style={{padding:10}}>
                                        <View style={{width:"100%"}}>
                                            <Text style={{ fontWeight:'bold'}}>	
                                                {data.nama_risiko}
                                            </Text>
                                        </View>
                                        <View style={{flexDirection:'row', marginBottom:10}}>
                                            
                                        </View>
                                        
                                        
                                        <View style={{width:'100%'}}>
                                            <Text style={{fontSize:12}}>Divisi / Project      : {data.nama_main}</Text>
                                        </View>
                                        <View style={{width:'100%'}}>
                                            <Text style={{fontSize:12}}>Jenis Risiko           : {data.nama_sub}</Text>
                                        </View>
                                        <View style={{width:'100%'}}>
                                            <Text style={{fontSize:12}}>Target                     : {data.nama_kpi}</Text>
                                        </View>
                                        <View style={{width:'100%'}}>
                                            <Text style={{fontSize:12}}>Depart / Project    : {data.nama_dept}</Text>
                                        </View>
                                        <View style={{width:'100%'}}>
                                            <Text style={{fontSize:12}}>Nama Divisi           : {data.nama_divisi}</Text>
                                        </View>
                                        {/* <View style={{width:'100%'}}>
                                            <Text>Unit Kerja           : {data.UnitKerja}</Text>
                                        </View> */}
                                        <View style={{marginTop:5,flexDirection:'row'}}>
                                            <View style={{ width:150}}>
                                                <View style={{marginTop:5, }}>
                                                        <Text style={{color:'#4F8732', fontSize:12, fontWeight:'bold' }}>Kemungkinan        : {data.kemungkinan}</Text>
                                                    </View>
                                                    <View style={{marginBottom:5}}>
                                                        <Text style={{color:'#4F8732', fontSize:12, fontWeight:'bold'}}>Besar Akibat          : {data.akibat}</Text>
                                                </View>
                                            </View>
                                            <View style={{justifyContent:'center'}}>
                                                <View style={{backgroundColor:'#4F8732', width:28, height:28, borderRadius:20, justifyContent:'center', alignItems:'center'}}>
                                                    <Text style={{color:'white', fontWeight:'bold'}}>{data.total}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        
                                    </View>
                                </ScrollView>
                                    
                                </View>
                                
                               
                            </View>
                            
                        </ScrollView>
                        
                    </Card>
                ))}
            </View>
            
            
        )
    }
}