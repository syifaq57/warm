import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    StyleSheet
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    
    CheckBox,
    Content
} from "native-base";
import Modal from "react-native-modal";
import styles from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import SubMenuAllunitDashboard from "../../../library/component/SubMenuAllunitDashboard";

export default class ButtonMenu extends Component{
    constructor(props){
        super(props);
        this.state={
            isModalVisible:false
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    render(){
        
        return(
            <View style={{ justifyContent: 'center', paddingHorizontal:10, flexDirection: "column", marginTop: 0, marginBottom: 5, }}>   
                <View style={{ justifyContent: 'center', width:"100%", marginBottom:0 }}>
                    <View
                       small
                        style={{
                            flexDirection:"row",
                            height:50,
                            borderWidth: 1,
                            backgroundColor: colors.gray09,
                            borderColor: colors.gray09,
                            borderRadius: 4,
                            justifyContent: 'flex-start',  
                            padding:10, width:"100%"                                             
                            }}
                        
                        >
                        <Image
                            source={require('../../../res/images/allunit.png')}
                            style={{width:30, height:30, marginRight:10}}
                        />
                        <View style={{width:"100%" ,flex:4, justifyContent:"center"}}>
                            <Text style={{fontSize:18, color:"white", fontWeight:"bold"}}>All unit Dashboard</Text>
                        </View>
                        
                        <Right style={{flex:1}}>
                            <TouchableOpacity
                                onPress={this.toggleModal}>
                            <Icon
                                    style={{alignSelf:"flex-end"}}
                                    name="apps"
                                    size={30}
                                    color="white"
                                />
                            </TouchableOpacity>
                            <Modal
                                style={stylesMenu.modal}
                                isVisible={this.state.isModalVisible}
                                onBackdropPress={this.toggleModal}
                                onBackButtonPress={this.toggleModal}
                                >
                                  <SubMenuAllunitDashboard navigation={this.props.navigation}/>
                            </Modal>
                        </Right>

                        
                    </View>
                    
                </View>
                
            </View>                                    
        )
    }
}

const stylesMenu = StyleSheet.create({
    modal: {
        
        marginHorizontal:0,
        marginBottom:0
    },
    
    
    
    
  });