import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    processColor,
    
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles2 from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";
import Swiper from 'react-native-swiper';
import ItemDetailRisiko from "../ItemDashboard/ItemDetailRisiko";
import SearchableDropdown from 'react-native-searchable-dropdown';

var items = [];

export default class SwipePie extends Component {
    constructor(props){
        super(props);
        this.state={
            l:'',
            c:'',
            positif:'positive',
            negative:'negative',

            isModalPositive:false,
            isModalDampak:false,

            index:0,
            isSearchRisk:false,
            isLoadingPieN:true,
            
            isloadingDetail:true,

            tableNegative:[],
            tablePositve:[],
            listDampak:[],
            dampakIndex:'',
            idDampakProps:'',
            selectedItems:[],

            
            colorBg:[],

            table1:{
                cb11:'',
                cb12:'',
                cb13:'',
                cb14:'',
                cb21:'',
                cb22:'',
                cb23:'',
                cb24:'',
                cb31:'',
                cb32:'',
                cb33:'',
                cb34:'',
                cb41:'',
                cb42:'',
                cb43:'',
                cb44:'',
            },
            table2:{
                cb11:'',
                cb12:'',
                cb13:'',
                cb14:'',
                cb21:'',
                cb22:'',
                cb23:'',
                cb24:'',
                cb31:'',
                cb32:'',
                cb33:'',
                cb34:'',
                cb41:'',
                cb42:'',
                cb43:'',
                cb44:'',
            },
            table3:{
                cb11:'',
                cb12:'',
                cb13:'',
                cb14:'',
                cb21:'',
                cb22:'',
                cb23:'',
                cb24:'',
                cb31:'',
                cb32:'',
                cb33:'',
                cb34:'',
                cb41:'',
                cb42:'',
                cb43:'',
                cb44:'',
            }
        }
    }

    toggleModal = () => {
        
        this.setState({ isModalVisible: !this.state.isModalVisible });

      };

    toggleModalPositive = () => {
        
        this.setState({ isModalPositive: !this.state.isModalPositive });

      };
    
    

    toggleModalSearch = () => {
      this.setState({
        isSearchRisk:!this.state.isSearchRisk
      })
  }

  toggleModalDampak = () => {
        
    this.setState({ isModalDampak: !this.state.isModalDampak });

  };
  
  

      

    componentDidMount(){
        this.setState({
            isLoadingSwipe1:true,
            isLoadingSwipe2:true,
            isLoadingSwipe3:true,
        })
        this.loadDampak()
        // AsyncStorage.getItem('username').then((value)=>{
        //     this.setState({
        //         username: value
        //     })
        // })
        // AsyncStorage.getItem('password').then((value)=>{
        //     this.setState({
        //         password: value,
        //         muk_code:this.props.id_depart_index,
        //         target:this.props.target
        //     },function(){
        //         this.loadDataUser()
                
        //     })
        // })
        AsyncStorage.getItem('data_user').then((value) =>{
            this.setState({
                id_proyek:JSON.parse(value).bagian_proyek,
                karyawan: JSON.parse(value).karyawan,
                creator: JSON.parse(value).creator,
                company: JSON.parse(value).company,
                sebagai: JSON.parse(value).sebagai,
                muk_code:this.props.id_depart_index,
                target:this.props.target
            }, function(){
                console.log('id---pyk', this.state.karyawan)
                this.loadPetaRisk()
                this.loadPetaRisk2()
                this.loadPetaRisk3()
            })
        })
        
        
                        
        
        
    }

    // loadDataUser(){
    //     // this.setState({
    //     //     isLoadingAll:true
    //     // })
    //     var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
    //     var formData = new FormData();
    //     formData.append("user", this.state.username)
    //     formData.append("pass", this.state.password)
    //     console.log('user tes',formData)
    //     fetch(url, {
    //         method: 'POST',
    //         body: formData
    //     }).then((response) => response.json())
    //         .then((response) => {
    //             console.log('datarespon',response)
    //             if (response.success == true) {
    //                 this.setState({
                        
    //                     id_proyek:response.data.bagian_proyek,
    //                     karyawan: response.data.karyawan,
    //                     creator: response.data.creator,
    //                     company: response.data.company,
    //                     sebagai: response.data.sebagai,
    //                 },function(){
                        
    //                     this.loadPetaRisk()
    //                     this.loadPetaRisk2()
    //                     this.loadPetaRisk3()
    //                 })
    //                 console.log('tes idpry', this.state.id_proyek)
                    
    //                 // AsyncStorage.setItem('data_user', (JSON.stringify(response.data)))
    //                 // this.AsyncDataUser()

    //             }else{
                    
    //                 this.setState({
    //                     isLoadingAll:false
    //                 })
    //                 Alert.alert('Error', 'Tidak bisa load data', [{
    //                     text: 'Okay'
    //                 }])
    //             }
    //         })
    //         .catch((error) => {
                
    //             Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
    //                 text: 'Okay'
    //             }])
    //             this.setState({
    //                 isLoadingAll:false
    //             })
    //             console.log(error)
    //         })
         
    // }

    
    loadDampak(){
        // this.setState({
        //     isLoadingDampak:true
        // })
        var url = GlobalConfig.SERVERHOST + 'api/api/getlistdampak';
        
        fetch(url, {
            method: 'GET'
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if(response.success == true){
                    var dampak = this.state.listDampak
                    for(var i=0; i<response.data.length; i++){
                        dampak.push({
                            id:response.data[i].id_dampak,
                            name:response.data[i].nama_dampak
                        })
                    }
                    this.setState({
                        listDampak:dampak,
                        dampakIndex:dampak[0].name,
                        idDampak:dampak[0].id
                        
                    })
                    console.log('resp', this.state.dampakIndex)
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                // this.setState({
                //     isLoadingDivisi:false
                // })
                console.log(error)
            })
         
    }

    loadPetaRisk(){
        // this.setState({
        //     isLoadingDepart:false,
        // })
        var url = GlobalConfig.SERVERHOST + 'api/api/get_heatmap_json_baru';
        var formData = new FormData();
        formData.append("positif", 'n')
        for(var i=0; i<this.state.id_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.id_proyek[i].id_proyek)
        }
        formData.append("target", this.state.target)
        // formData.append('muk_kode', this.state.muk_code)
        formData.append('sebagai', this.state.sebagai)
       
        
        console.log('heatmap1',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                
                this.setState({
                    table1:{
                        cb11:response.isi.cb11,
                        cb12:response.isi.cb12,
                        cb13:response.isi.cb13,
                        cb14:response.isi.cb14,
                        cb21:response.isi.cb21,
                        cb22:response.isi.cb22,
                        cb23:response.isi.cb23,
                        cb24:response.isi.cb24,
                        cb31:response.isi.cb31,
                        cb32:response.isi.cb32,
                        cb33:response.isi.cb33,
                        cb34:response.isi.cb34,
                        cb41:response.isi.cb41,
                        cb42:response.isi.cb42,
                        cb43:response.isi.cb43,
                        cb44:response.isi.cb44,
                    }
                    
                    
                },function(){
                    this.setState({
                        Colorcb11:this.state.table1.cb11[1].split('|')[0],
                        Colorcb12:this.state.table1.cb12[1].split('|')[0],
                        Colorcb13:this.state.table1.cb13[1].split('|')[0],
                        Colorcb14:this.state.table1.cb14[1].split('|')[0],
                        Colorcb21:this.state.table1.cb21[1].split('|')[0],
                        Colorcb22:this.state.table1.cb22[1].split('|')[0],
                        Colorcb23:this.state.table1.cb23[1].split('|')[0],
                        Colorcb24:this.state.table1.cb24[1].split('|')[0],
                        Colorcb31:this.state.table1.cb31[1].split('|')[0],
                        Colorcb32:this.state.table1.cb32[1].split('|')[0],
                        Colorcb33:this.state.table1.cb33[1].split('|')[0],
                        Colorcb34:this.state.table1.cb34[1].split('|')[0],
                        Colorcb41:this.state.table1.cb41[1].split('|')[0],
                        Colorcb42:this.state.table1.cb42[1].split('|')[0],
                        Colorcb43:this.state.table1.cb43[1].split('|')[0],
                        Colorcb44:this.state.table1.cb44[1].split('|')[0],
                    },
                    function(){
                        console.log('tes', '#'+this.state.Colorcb11)
                        this.setState({
                            isLoadingSwipe1:false
                        })
                    })
                    
                })
                
            })
            .catch((error) => {
                this.setState({
                    // isLoadingAll:false
                })
                Alert.alert('Cannot Load Data1', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingSwipe1:false
                })
                console.log(error)
            })
    }
    loadPetaRisk2(){
        // this.setState({
        //     isLoadingDepart:false,
        // })
        var url = GlobalConfig.SERVERHOST + 'api/api/get_heatmap_json_baru';
        var formData = new FormData();
        formData.append("positif", 'y')
        for(var i=0; i<this.state.id_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.id_proyek[i].id_proyek)
        }
        formData.append("target", this.state.target)
        // formData.append('muk_kode', this.state.muk_code)
        formData.append('sebagai', this.state.sebagai)
        console.log('yuuhuu',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                
                this.setState({
                    table2:{
                        cb11:response.isi.cb11,
                        cb12:response.isi.cb12,
                        cb13:response.isi.cb13,
                        cb14:response.isi.cb14,
                        cb21:response.isi.cb21,
                        cb22:response.isi.cb22,
                        cb23:response.isi.cb23,
                        cb24:response.isi.cb24,
                        cb31:response.isi.cb31,
                        cb32:response.isi.cb32,
                        cb33:response.isi.cb33,
                        cb34:response.isi.cb34,
                        cb41:response.isi.cb41,
                        cb42:response.isi.cb42,
                        cb43:response.isi.cb43,
                        cb44:response.isi.cb44,
                    }
                    
                    
                },function(){
                    this.setState({
                        Color2cb11:this.state.table2.cb11[1].split('|')[0],
                        Color2cb12:this.state.table2.cb12[1].split('|')[0],
                        Color2cb13:this.state.table2.cb13[1].split('|')[0],
                        Color2cb14:this.state.table2.cb14[1].split('|')[0],
                        Color2cb21:this.state.table2.cb21[1].split('|')[0],
                        Color2cb22:this.state.table2.cb22[1].split('|')[0],
                        Color2cb23:this.state.table2.cb23[1].split('|')[0],
                        Color2cb24:this.state.table2.cb24[1].split('|')[0],
                        Color2cb31:this.state.table2.cb31[1].split('|')[0],
                        Color2cb32:this.state.table2.cb32[1].split('|')[0],
                        Color2cb33:this.state.table2.cb33[1].split('|')[0],
                        Color2cb34:this.state.table2.cb34[1].split('|')[0],
                        Color2cb41:this.state.table2.cb41[1].split('|')[0],
                        Color2cb42:this.state.table2.cb42[1].split('|')[0],
                        Color2cb43:this.state.table2.cb43[1].split('|')[0],
                        Color2cb44:this.state.table2.cb44[1].split('|')[0],
                    },
                    function(){
                        console.log('tes', '#'+this.state.Colorcb11)
                        this.setState({
                            isLoadingSwipe2:false
                        })
                    })
                    
                })
                
            })
            .catch((error) => {
                this.setState({
                    isLoadingSwipe2:false
                })
                Alert.alert('Cannot Load Data2', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    loadPetaRisk3(){
        // this.setState({
        //     isLoadingDepart:false,
        // })
        var url = GlobalConfig.SERVERHOST + 'api/api/get_heatmap_json_baru';
        var formData = new FormData();
        formData.append("positif", 'n')
        formData.append("id_dampak", this.state.idDampak)
        // formData.append("target", this.state.target)
        for(var i=0; i<this.state.id_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.id_proyek[i].id_proyek)
        }
        // formData.append('muk_kode', this.state.muk_code)
        formData.append('sebagai', this.state.sebagai)
       
        
        console.log('peta3',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                console.log('resp peta3', response.isi)
                this.setState({
                    table3:{
                        cb11:response.isi.cb11,
                        cb12:response.isi.cb12,
                        cb13:response.isi.cb13,
                        cb14:response.isi.cb14,
                        cb21:response.isi.cb21,
                        cb22:response.isi.cb22,
                        cb23:response.isi.cb23,
                        cb24:response.isi.cb24,
                        cb31:response.isi.cb31,
                        cb32:response.isi.cb32,
                        cb33:response.isi.cb33,
                        cb34:response.isi.cb34,
                        cb41:response.isi.cb41,
                        cb42:response.isi.cb42,
                        cb43:response.isi.cb43,
                        cb44:response.isi.cb44,
                    }
                    
                    
                },function(){
                    this.setState({
                        Color3cb11:this.state.table3.cb11[1].split('|')[0],
                        Color3cb12:this.state.table3.cb12[1].split('|')[0],
                        Color3cb13:this.state.table3.cb13[1].split('|')[0],
                        Color3cb14:this.state.table3.cb14[1].split('|')[0],
                        Color3cb21:this.state.table3.cb21[1].split('|')[0],
                        Color3cb22:this.state.table3.cb22[1].split('|')[0],
                        Color3cb23:this.state.table3.cb23[1].split('|')[0],
                        Color3cb24:this.state.table3.cb24[1].split('|')[0],
                        Color3cb31:this.state.table3.cb31[1].split('|')[0],
                        Color3cb32:this.state.table3.cb32[1].split('|')[0],
                        Color3cb33:this.state.table3.cb33[1].split('|')[0],
                        Color3cb34:this.state.table3.cb34[1].split('|')[0],
                        Color3cb41:this.state.table3.cb41[1].split('|')[0],
                        Color3cb42:this.state.table3.cb42[1].split('|')[0],
                        Color3cb43:this.state.table3.cb43[1].split('|')[0],
                        Color3cb44:this.state.table3.cb44[1].split('|')[0],
                    },
                    function(){
                        this.setState({
                            isLoadingSwipe3:false
                        })
                    })
                    
                })
                
            })
            .catch((error) => {
                this.setState({
                    isLoadingSwipe3:false
                })
                Alert.alert('Cannot Load Data3', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }
    
    onSearch(){
        
        this.setState({
            isSearchRisk:false,
            isLoadingSwipe3:true,
        })
        
        this.loadPetaRisk3()
        
    }

 

    render(){
        return(
            <Card style={{marginTop:10, marginLeft:10, marginRight:10,}}>                           
                
                
                <Swiper 
                    style={styles.wrapper} 
                    height='100%' 
                    showsButtons 
                    buttonWrapperStyle={{
                        backgroundColor: 'transparent',
                        flexDirection: 'row',
                        position: 'absolute',
                        top: -13, left: 0, 
                        flex: 1, 
                        
                        paddingHorizontal: -2, 
                        paddingVertical: 0, 
                        justifyContent: 'space-between'
                        }}
                    >

                 <View style={styles.slide}>
                    <View style={{flexDirection:'column', marginTop:0}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,  
                            paddingVertical:15                         
                        }}>
                            <View style={{ flex:4, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:15}}>
                                    PETA RISIKO DOWNSIDE
                                </Text>
                            </View>
                            
                        </View>
                    </View>
                    {this.state.isLoadingSwipe1?(
                        <ActivityIndicator/>
                    ):(
                    <View style={styles.container}>  
                    <View style={{alignItems:"center", marginTop:30, marginBottom:50}}>
                        <View style={{ flex:1 , width:"65%", height:"50%", flexDirection:"column",  justifyContent:"center", alignItems:"center"}}>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, flexDirection:"column", backgroundColor:colors.gray06, borderColor:colors.gray05, borderWidth:1, borderRightWidth:0, width:50,height: 50}}>
                                    <View style={{flex:1, alignItems:"flex-end", margin:5}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>BA</Text>
                                    </View>
                                    <View style={{flex:1, margin:5, alignItems:"flex-start"}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>KM</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderRightWidth:1, borderBottomWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>1</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:0, borderTopWidth:1, borderLeftWidth:0,width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                            <Text style={{fontSize:14, fontWeight:"bold"}}>2</Text>
                                        </View>
                                    </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderBottomWidth:0,borderTopWidth:1,borderRightWidth:0, borderLeftWidth:1, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>3</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderBottomWidth:0, borderTopWidth:1, borderRightWidth:1, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>4</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1 ,borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>4</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb41, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderRightWidth:1, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:1,c:4
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table1.cb41[2]}
                                            </Text>
                                        </TouchableOpacity>       
                                    </View>                                                           
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb42, justifyContent:"center", borderLeftWidth:0, borderRightWidth:1, borderWidth:1, width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:2,c:4
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table1.cb42[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>             
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb43, justifyContent:"center", borderLeftWidth:0, borderRightWidth:0, borderWidth:1 ,width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:3,c:4
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb43[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>  
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb44, justifyContent:"center", borderWidth:1, borderTopWidth:1, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:4,c:4
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    > 
                                                <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table1.cb44[2]}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>  
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>3</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb31, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderRightWidth:0, borderTopWidth:0, borderRightWidth:1, width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:1,c:3
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb31[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>                              
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb32, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, borderRightWidth:0,  width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:2,c:3
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    >  
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table1.cb32[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb33, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:1, borderRightWidth:0 ,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:3,c:3
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    >   
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table1.cb33[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb34, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                        onPress={()=>
                                            this.setState({
                                                l:4,c:3
                                            },function(){
                                                console.log('sks')
                                                this.setState({ isModalVisible: !this.state.isModalVisible });
                                            }
                                        )}
                                    >     
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb34[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>2</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb21, justifyContent:"center", borderWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                        <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        >    
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb21[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb22, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        >    
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb22[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb23, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0 ,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb23[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb24, justifyContent:"center", borderWidth:1, borderLeftWidth:0, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        >  
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb24[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>1</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb11, justifyContent:"center", borderWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        >    
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb11[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb12, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        >
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb12[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb13, justifyContent:"center", borderWidth:1, borderTopWidth:0 ,borderLeftWidth:0,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        >
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb13[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Colorcb14, justifyContent:"center", borderWidth:1, borderLeftWidth:0, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalVisible: !this.state.isModalVisible });
                                                }
                                            )}
                                        >
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table1.cb14[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            
                            
                        </View>
                        </View>

                        <Modal
                            style={{marginHorizontal:0,marginBottom:0,marginTop:0,}}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}
                            onBackButtonPress={this.toggleModal}
                        >
                            <View style={{flex:1, justifyContent:'center',}}>
                            <View style={{backgroundColor:"white",height:"100%",  marginHorizontal:'0%', borderRadius:5}}>
                                    <View style={{backgroundColor:colors.bgErm, }}>
                                        <View style={{flexDirection:'row', marginLeft:-20, paddingVertical:15, marginTop:Platform.OS === "ios" ? 20 :0 }}>
                                            <View style={{flex:1, marginRight:15, justifyContent:'center'}}>
                                                    <TouchableOpacity
                                                        onPress={this.toggleModal}
                                                        style={{alignItems:'flex-end'}}
                                                    >
                                                    <Icon
                                                        name='md-arrow-back'
                                                        size={25}
                                                        color={colors.gray09}
                                                        />
                                                    </TouchableOpacity>                
                                            </View>
                                            <View style={{flex:6, justifyContent:'center'}}>
                                                <Text style={{color:colors.gray09, fontWeight:'bold', fontSize:17}}>Detail Risiko</Text>
                                            </View>
                                            
                                        </View>        
                                    </View>
                                    
                                        <ScrollView>
                                            <ItemDetailRisiko
                                                id_depart_index = {this.props.id_depart_index}
                                                column={this.state.c}
                                                row={this.state.l}
                                                positif={this.state.negative}
                                                target={this.props.target}
                                                idDampak={this.state.idDampakProps}
                                            />
                                        </ScrollView>
                                    
                                    
                                    
                                </View>
                            </View>
                        </Modal>
                        
                    </View>
                    )}
                    
                 </View>

                 <View style={styles.slide}>
                    <View style={{flexDirection:'column', marginTop:0}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,  
                            paddingVertical:15                         
                        }}>
                            <View style={{ flex:4, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:15}}>
                                    PETA RISIKO UPSIDE
                                </Text>
                            </View>
                            
                        </View>
                    </View>
                    {this.state.isLoadingSwipe2?(
                        <ActivityIndicator/>
                    ):(
                    <View style={styles.container}>  
                    <View style={{alignItems:"center", marginTop:30, marginBottom:50}}>
                        <View style={{ flex:1 , width:"65%", height:"50%", flexDirection:"column",  justifyContent:"center", alignItems:"center"}}>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, flexDirection:"column", backgroundColor:colors.gray06, borderColor:colors.gray05, borderWidth:1, borderRightWidth:0, width:50,height: 50}}>
                                    <View style={{flex:1, alignItems:"flex-end", margin:5}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>BA</Text>
                                    </View>
                                    <View style={{flex:1, margin:5, alignItems:"flex-start"}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>KM</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderRightWidth:1, borderBottomWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>1</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:0, borderTopWidth:1, borderLeftWidth:0,width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                            <Text style={{fontSize:14, fontWeight:"bold"}}>2</Text>
                                        </View>
                                    </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderBottomWidth:0,borderTopWidth:1,borderRightWidth:0, borderLeftWidth:1, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>3</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderBottomWidth:0, borderTopWidth:1, borderRightWidth:1, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>4</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1 ,borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>4</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb41, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderRightWidth:1, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        >  
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table2.cb41[2]}
                                            </Text>
                                        </TouchableOpacity>       
                                    </View>                                                           
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb42, justifyContent:"center", borderLeftWidth:0, borderRightWidth:1, borderWidth:1, width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        >  
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table2.cb42[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>             
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb43, justifyContent:"center", borderLeftWidth:0, borderRightWidth:0, borderWidth:1 ,width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb43[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>  
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb44, justifyContent:"center", borderWidth:1, borderTopWidth:1, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                                <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table2.cb44[2]}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>  
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>3</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb31, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderRightWidth:0, borderTopWidth:0, borderRightWidth:1, width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb31[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>                              
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb32, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, borderRightWidth:0,  width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        >    
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table2.cb32[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb33, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:1, borderRightWidth:0 ,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table2.cb33[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb34, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb34[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>2</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb21, justifyContent:"center", borderWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalDampak });
                                                }
                                            )}
                                        >     
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb21[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb22, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        >  
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb22[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb23, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0 ,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb23[2]}
                                            </Text>
                                        </TouchableOpacity> 
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb24, justifyContent:"center", borderWidth:1, borderLeftWidth:0, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb24[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>1</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb11, justifyContent:"center", borderWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb11[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb12, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb12[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb13, justifyContent:"center", borderWidth:1, borderTopWidth:0 ,borderLeftWidth:0,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb13[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color2cb14, justifyContent:"center", borderWidth:1, borderLeftWidth:0, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalPositive: !this.state.isModalPositive });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table2.cb14[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            
                            
                        </View>
                        </View>

                        <Modal
                            style={{marginHorizontal:0,marginBottom:0,marginTop:0,}}
                            isVisible={this.state.isModalPositive}
                            onBackdropPress={this.toggleModalPositive}
                            onBackButtonPress={this.toggleModalPositive}
                        >
                            <View style={{flex:1, justifyContent:'center',}}>
                                <View style={{backgroundColor:"white",height:"100%",  marginHorizontal:'0%', borderRadius:5}}>
                                    <View style={{backgroundColor:colors.bgErm, }}>
                                        <View style={{flexDirection:'row', marginLeft:-20, paddingVertical:15, marginTop:Platform.OS === "ios" ? 20 :0 }}>
                                            <View style={{flex:1, marginRight:15, justifyContent:'center'}}>
                                                    <TouchableOpacity
                                                        onPress={this.toggleModalPositive}
                                                        style={{alignItems:'flex-end'}}
                                                    >
                                                    <Icon
                                                        name='md-arrow-back'
                                                        size={25}
                                                        color={colors.gray09}
                                                        />
                                                    </TouchableOpacity>                
                                            </View>
                                            <View style={{flex:6, justifyContent:'center'}}>
                                                <Text style={{color:colors.gray09, fontWeight:'bold', fontSize:17}}>Detail Risiko</Text>
                                            </View>
                                            
                                        </View>        
                                    </View>
                                    <ScrollView>
                                    
                                    <ItemDetailRisiko
                                        id_depart_index = {this.props.id_depart_index}
                                        column={this.state.c}
                                        row={this.state.l}
                                        positif={this.state.positif}
                                        target={this.props.target}
                                        idDampak={this.state.idDampakProps}
                                    />
                                    </ScrollView>
                                    
                                </View>
                            </View>
                        </Modal>
                        
                    </View>
                    )}
                    
                 </View>
                    
                 <View style={styles.slide}>
                    <View style={{flexDirection:'column', marginTop:0}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,  
                            paddingVertical:15                         
                        }}>
                            <View style={{ flex:4, width:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:15}}>
                                    PETA RISIKO By DAMPAK
                                </Text>
                            </View>
                            
                        </View>
                    </View>
                    {/* <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                    <View style={{width:"72%",  marginTop:5}}>
                        <SearchableDropdown
                            onItemSelect={(item) => {
                            const items = this.state.selectedItems;
                            items.push(item)
                            this.setState({ selectedItems: items});
                                        // alert(item.kode)
                                    }}
                            onRemoveItem={(item, index) => {
                            const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                            this.setState({ selectedItems: items });
                            }}
                            itemStyle={{
                                padding: 10,
                                marginTop: 2,
                                marginLeft:5,
                                backgroundColor: '#ddd',
                                borderColor: '#bbb',
                                borderWidth: 1,
                                borderRadius: 5,
                            }}
                            itemTextStyle={{ color: '#222', fontSize:12 }}
                            itemsContainerStyle={{ maxHeight: 140 }}
                            items={this.state.listDampak}
                                
                            textInputProps={
                                {
                                placeholder: "--Select Dampak--",
                                style: {
                                    marginLeft:0,
                                    padding: 5,
                                    borderWidth: 1,
                                    borderColor: '#ccc',
                                    borderRadius: 5,
                                },                            
                                }
                            }
                            listProps={
                                {
                                    nestedScrollEnabled: true,
                                }
                            }
                        />    
                    </View>      
                    <View style={{marginLeft:5, paddingVertical:5, marginTop:5, justifyContent:'center', paddingHorizontal:5}}>
                        <Button
                            onPress={()=>(this.onSearch())}
                            style={{ backgroundColor:'#4C7FF0', borderRadius:5, height:40 }}
                            >
                            <View style={{flexDirection:'row', paddingHorizontal:5}}>
                                <Icon2
                                    style={{alignSelf:'center',}}
                                    name="search"
                                    size={17}
                                    color='white'
                                />
                                <View style={{marginLeft:3}}>
                                <Text style={{color:'white', fontSize:16}}>Cari</Text>
                                </View>
                            </View>
                                        
                        </Button>
                    </View>   
                    </View> */}
                    
                    <View 
                        
                        style={{
                            flexDirection:'row',
                            backgroundColor:colors.bgErm,
                            marginRight:50,
                            marginLeft:50, 
                            marginVertical:10,
                            // borderColor: colors.gray05, 
                            borderRadius:5, 
                            // borderWidth:1,
                            paddingVertical:10
                            
                        }}>

                        <View style={{flex:5, alignItems:'flex-start', justifyContent:'center', marginLeft:10, width:'100%'}}>
                            <Text style={{color:colors.gray09, fontSize:11}}>{this.state.dampakIndex}</Text>
                        </View>
                        <TouchableOpacity
                            onPress={this.toggleModalSearch}
                         style={{flex:1, borderLeftWidth:1, borderColor:colors.gray02, alignItems:'center', width:'100%'}}>
                            <Icon
                                name="md-create"
                                size={20}
                                color={colors.gray09}
                            />
                        </TouchableOpacity>
                    </View>
                    
                    
                    {this.state.isLoadingSwipe3?(
                        <ActivityIndicator/>
                    ):(
                    
                    <View style={styles.container}>  
                    <View style={{alignItems:"center", marginTop:5, marginBottom:10}}>
                        <View style={{ flex:1 , width:"65%", height:"50%", flexDirection:"column",  justifyContent:"center", alignItems:"center"}}>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, flexDirection:"column", backgroundColor:colors.gray06, borderColor:colors.gray05, borderWidth:1, borderRightWidth:0, width:50,height: 50}}>
                                    <View style={{flex:1, alignItems:"flex-end", margin:5}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>BA</Text>
                                    </View>
                                    <View style={{flex:1, margin:5, alignItems:"flex-start"}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>KM</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderRightWidth:1, borderBottomWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>1</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:0, borderTopWidth:1, borderLeftWidth:0,width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                            <Text style={{fontSize:14, fontWeight:"bold"}}>2</Text>
                                        </View>
                                    </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderBottomWidth:0,borderTopWidth:1,borderRightWidth:0, borderLeftWidth:1, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>3</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderBottomWidth:0, borderTopWidth:1, borderRightWidth:1, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>4</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1 ,borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>4</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb41, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderRightWidth:1, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table3.cb41[2]}
                                            </Text>
                                        </TouchableOpacity>       
                                    </View>                                                           
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb42, justifyContent:"center", borderLeftWidth:0, borderRightWidth:1, borderWidth:1, width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table3.cb42[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>             
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb43, justifyContent:"center", borderLeftWidth:0, borderRightWidth:0, borderWidth:1 ,width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb43[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>  
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb44, justifyContent:"center", borderWidth:1, borderTopWidth:1, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:4
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        >
                                                <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table3.cb44[2]}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>  
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>3</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb31, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderRightWidth:0, borderTopWidth:0, borderRightWidth:1, width:50,height:50}}>                            
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        >    
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb31[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>                              
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb32, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, borderRightWidth:0,  width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        >   
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table3.cb32[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb33, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:1, borderRightWidth:0 ,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        >   
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                                {this.state.table3.cb33[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb34, justifyContent:"center", borderWidth:1, borderLeftWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:3
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        >      
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb34[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>2</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb21, justifyContent:"center", borderWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        >   
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb21[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb22, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb22[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb23, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0 ,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb23[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb24, justifyContent:"center", borderWidth:1, borderLeftWidth:0, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:2
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb24[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                            </View>
                            <View style={{flex:1, flexDirection:"row", paddingHorizontal:0, marginBottom:0}}>
                                <View style={{flex:1, borderColor:colors.gray05, borderWidth:1, borderTopWidth:0, borderRightWidth:0, width:50,height:50}}>
                                    <View style={{flex:1, margin:5, alignItems:"center", justifyContent:"center"}}>
                                        <Text style={{fontSize:14, fontWeight:"bold"}}>1</Text>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb11, justifyContent:"center", borderWidth:1, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:1,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb11[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>   
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb12, justifyContent:"center", borderWidth:1, borderTopWidth:0, borderLeftWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:2,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb12[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb13, justifyContent:"center", borderWidth:1, borderTopWidth:0 ,borderLeftWidth:0,width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:3,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb13[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flex:1, borderColor:colors.gray05,backgroundColor:'#'+this.state.Color3cb14, justifyContent:"center", borderWidth:1, borderLeftWidth:0, borderTopWidth:0, width:50,height:50}}>
                                    <View style={{alignItems:"center"}}>
                                    <TouchableOpacity 
                                            onPress={()=>
                                                this.setState({
                                                    l:4,c:1
                                                },function(){
                                                    console.log('sks')
                                                    this.setState({ isModalDampak: !this.state.isModalDampak });
                                                }
                                            )}
                                        > 
                                            <Text style={{fontSize:20, color:'white', fontWeight:"bold"}}>
                                            {this.state.table3.cb14[2]}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            
                            
                        </View>
                </View>
                <Modal
                    style={{marginHorizontal:0,marginBottom:0,marginTop:0,}}
                    isVisible={this.state.isModalDampak}
                    onBackdropPress={this.toggleModalDampak}
                    onBackButtonPress={this.toggleModalDampak}
                        >
                            <View style={{flex:1, justifyContent:'center',}}>
                            <View style={{backgroundColor:"white",height:"100%",  marginHorizontal:'0%', borderRadius:5}}>
                                <View style={{backgroundColor:colors.bgErm, }}>
                                        <View style={{flexDirection:'row', marginLeft:-20, paddingVertical:15, marginTop:Platform.OS === "ios" ? 20 :0 }}>
                                            <View style={{flex:1, marginRight:15, justifyContent:'center'}}>
                                                    <TouchableOpacity
                                                        onPress={this.toggleModalDampak}
                                                        style={{alignItems:'flex-end'}}
                                                    >
                                                    <Icon
                                                        name='md-arrow-back'
                                                        size={25}
                                                        color={colors.gray09}
                                                        />
                                                    </TouchableOpacity>                
                                            </View>
                                            <View style={{flex:6, justifyContent:'center'}}>
                                                <Text style={{color:colors.gray09, fontWeight:'bold', fontSize:17}}>Detail Risiko</Text>
                                            </View>
                                            
                                        </View>        
                                    </View>
                                    <ScrollView>
                                    {/* <ItemDetailRisiko
                                        id_depart_index = {this.props.id_depart_index}
                                        column={this.state.c}
                                        row={this.state.l}
                                        positif={this.state.positif}
                                        target={this.props.target}
                                        idDampak={this.state.idDampakProps}
                                    /> */}
                                    <ItemDetailRisiko
                                        id_depart_index = {this.props.id_depart_index}
                                        column={this.state.c}
                                        row={this.state.l}
                                        positif={this.state.positif}
                                        target={this.props.target}
                                        idDampak={this.props.idDampakProps}
                                    />
                                    </ScrollView>
                                    
                                </View>
                            </View>
                        </Modal>
                        
                    </View>
                    )}
                    
                 </View>

                </Swiper>
                <Modal
                        
                        isVisible={this.state.isSearchRisk}
                        onBackdropPress={this.toggleModalSearch}
                        onBackButtonPress={this.toggleModalSearch}
                        >
                        <Card  style={{marginTop:10, marginLeft:10, marginBottom:10, marginRight:10, borderRadius:3,}}>
                            
                                <View style={{
                                flexDirection:'row', 
                                width:"100%",
                                paddingVertical:10,
                                backgroundColor: colors.bgErm,
                                borderTopLeftRadius:2,
                                borderTopRightRadius:2,
                                
                                }}>
                                <View style={{ flex:4, width:'100%', marginLeft:25, justifyContent:'center'}}>
                                    <Text style={{ color:colors.gray09, fontSize:16}}>
                                        SELECT DAMPAK
                                    </Text>
                                </View>
                                <View style={{flex:1, marginRight:25}}>
                                    <TouchableOpacity style={{flexDirection:'row'}}
                                        onPress={()=>(this.onSearch())}
                                        >
                                        <Icon3
                                            name='file-search'
                                            size={27} 
                                            color='white'
                                        />
                                        <View style={{width:'100%', justifyContent:'center'}}>
                                            <Text style={{fontWeight:'bold', color:colors.gray09, fontSize:17}}>Cari</Text>
                                        </View>
                                        
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                            <View style={{width:"80%",  marginTop:5, marginBottom:10}}>
                            <SearchableDropdown
                            onItemSelect={(item) => {
                            const items = this.state.selectedItems;
                            items.push(item)
                            this.setState({ selectedItems: items, idDampakProps:item.id, idDampak:item.id, dampakIndex:item.name});
                                        // alert(item.kode)
                                    }}
                            onRemoveItem={(item, index) => {
                            const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                            this.setState({ selectedItems: items });
                            }}
                            itemStyle={{
                                padding: 10,
                                marginTop: 2,
                                marginLeft:5,
                                backgroundColor: 'white',
                                borderColor: '#bbb',
                                borderWidth: 1,
                                borderRadius: 5,
                            }}
                            itemTextStyle={{ color: '#222', fontSize:12 }}
                            itemsContainerStyle={{ maxHeight: 140 }}
                            items={this.state.listDampak}
                                
                            textInputProps={
                                {
                                placeholder: "--Select Dampak--",
                                style: {
                                    backgroundColor:'white',
                                    marginLeft:0,
                                    padding: 5,
                                    borderWidth: 1,
                                    borderColor: '#ccc',
                                    borderRadius: 5,
                                },                            
                                }
                            }
                            listProps={
                                {
                                    nestedScrollEnabled: true,
                                }
                            }
                        />    
                    </View>      
                    {/* <View style={{marginLeft:5, paddingVertical:5, marginTop:5, justifyContent:'center', paddingHorizontal:5}}>
                        <Button
                            onPress={()=>(this.onSearch())}
                            style={{ backgroundColor:'#4C7FF0', borderRadius:5, height:40 }}
                            >
                            <View style={{flexDirection:'row', paddingHorizontal:5}}>
                                <Icon2
                                    style={{alignSelf:'center',}}
                                    name="search"
                                    size={17}
                                    color='white'
                                />
                                <View style={{marginLeft:3}}>
                                <Text style={{color:'white', fontSize:16}}>Cari</Text>
                                </View>
                            </View>
                                        
                        </Button>
                    </View>    */}
                    </View>
                            
                            
                            
                        </Card>
                    </Modal>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical:0,
        paddingHorizontal:0,
        borderRadius:0,
      },
      bar: {
        flex:2,
        marginTop: 10,
        justifyContent:"center",
        alignSelf:"flex-end",
        marginLeft:0,
        height: Dimensions.get('window').height / 3.2,
        width: Dimensions.get('window').width,
        
    },
    legend:{
        flex:1,
        flexDirection:"column",
        justifyContent:"center",
        marginTop:35,
        marginLeft:0,
        marginBottom:10,
      },
      legend2:{
        flex:1,
        flexDirection:"column",
        justifyContent:"center",
        marginTop:35,
        marginRight:20,
        marginLeft:0,
        marginBottom:10,
      },
    wrapper: {
    },
    slide1: {
      flex: 1,
      
      paddingHorizontal:0,
      backgroundColor: 'white',
      borderTopRightRadius:0,
      borderTopLeftRadius:0
    },
    slide2: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:8,
      borderTopLeftRadius:8
    },
   
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    }
  })

//   AppRegistry.registerComponent('SwipePie', () => SwipePie);