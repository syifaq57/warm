import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content, Textarea
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles2 from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";


import Swiper from 'react-native-swiper';
import ItemSubDetail from "./ItemSubDetail";

export default class SwipeSubDetail extends Component {
    constructor(props){
        super(props);
        this.state={
            index:0,
            isStatusDownSide:false,
            
            sebab:[],
            dampak:[],
            desc:'',
            desc_pengendalian:[],
            pengendalian:[]
        }
    }
    componentDidMount(){
        this.setState({
            sebab :this.props.sebab,
            dampak:this.props.dampak,
            PN : this.props.positive_negative,
        }, function(){
            // console.log('sebabksksksssks', this.state.PN)
        })
        
    }

      toggleStatusDownSide = () => {
        this.setState({ isStatusDownSide: !this.state.isStatusDownSide,  });
      };

      navigateToScreen(route){
        
             this.props.navigation.navigate(route);
       }
    
    render(){
        return(
            
            <Card style={{marginTop:10, marginLeft:10, marginRight:10}}>        
                 <Swiper 
                    
                    height='100%' 
                    showsButtons
                    showsPagination={false} 
                    buttonWrapperStyle={{
                        backgroundColor: 'transparent',
                        flexDirection: 'row',
                        position: 'absolute',
                        top: -15,
                        left: 0,
                        flex: 1,
                        paddingHorizontal: 0,
                        paddingVertical: 0,
                        justifyContent: 'space-between',
                        alignItems: 'flex-start'}}
                    >
                 
                    <View style={styles.slide1}>
                        <View style={{
                                flexDirection:'row', 
                                width:"100%",
                                paddingVertical:12,
                                backgroundColor: colors.bgErm,
                                borderTopLeftRadius:0,
                                borderTopRightRadius:0,
                                
                                }}>

                                <View style={{ flex:4, width:'100%',justifyContent:'center'}}>
                                    <Text style={{fontSize:14, alignSelf:'center'}}>
                                        Faktor Negative (Penyebab)          
                                    </Text>
                                </View>
                        </View>

                        {this.state.sebab.map((data,index)=>(
                        <View key={index} style={{backgroundColor:colors.green04, marginLeft:0, marginTop:0, marginBottom:10, marginRight:0, width:'95%'}}>
                            <View style={{marginVertical:10, marginHorizontal:10}}>
                                <Text style={{fontSize:15, fontWeight:'bold'}}>Faktor Negatif Yang Menyebabkan Dampak</Text>
                            </View> 
                           
                            
                            <View style={{marginVertical:0, marginBottom:5, marginHorizontal:12}}>
                            {this.props.positive_negative == 'negative'?(
                                <Text style={{marginLeft:5}}>Faktor Negatif</Text>
                            ):(
                                <Text style={{marginLeft:5}}>Faktor Pendukung</Text>
                            )}
                            
                                <Textarea
                                        style={{
                                            paddingLeft:10,
                                            marginBottom: 10,
                                            fontSize: 11,
                                            borderWidth:1,
                                            borderRadius:5,
                                            borderColor:colors.gray05,
                                            backgroundColor:colors.white,
                                            height:60,
                                        }}
                                        // disabled
                                        rowSpan={3}
                                        bordered
                                        placeholderTextColor={colors.gray02}
                                        value={data.penyebab}
                                        // placeholder="Type Something ..."
                                        onChangeText={text =>
                                            this.setState({ TINDAKAN_PERBAIKAN: text })
                                        }
                                        editable={false}
                                    />
                           
                                <Text style={{marginLeft:5}}>Deskripsi</Text>
                                <Textarea
                                        style={{
                                            paddingLeft:10,
                                            marginBottom: 5,
                                            fontSize: 11,
                                            borderWidth:1,
                                            borderRadius:5,
                                            borderColor:colors.gray05,
                                            backgroundColor:colors.white
                                        }}
                                        // disabled
                                        rowSpan={3}
                                        bordered
                                        placeholderTextColor={colors.gray02}
                                        value={data.desc}
                                        // placeholder="Type Something ..."
                                        onChangeText={text =>
                                            this.setState({ TINDAKAN_PERBAIKAN: text })
                                        }
                                        editable={false}
                                    />
                            </View> 
                            <View style={{marginVertical:5, marginVertical:20, borderWidth:1, borderColor:colors.gray01}}></View>
                            <View style={{paddingHorizontal:10}}>
                            
                            {data.pengendalian.map((data2, index2)=>(
                                <View key={index2} style={{backgroundColor:colors.gray07, borderRadius:5, padding:15, marginBottom:10}}>
                                <Text style={{fontWeight:'bold'}}>Rencana Penanganan Risiko</Text>
                                <View>
                                    <Textarea
                                        style={{
                                            paddingLeft:10,
                                            marginBottom: 10,
                                            fontSize: 11,
                                            borderWidth:1,
                                            borderRadius:5,
                                            borderColor:colors.gray05,
                                            backgroundColor:'white'
                                        }}
                                        // disabled
                                        rowSpan={3}
                                        bordered
                                        placeholderTextColor={colors.gray02}
                                        value={data2.desc}
                                        // placeholder="Type Something ..."
                                        onChangeText={text =>
                                            this.setState({ TINDAKAN_PERBAIKAN: text })
                                        }
                                        editable={false}
                                    />
                                    
                                    <View style={{alignItems:'center'}}>
                                    {data2.periode != null?(
                                        <View style={{alignItems:'center'}}>
                                            <View style={{flexDirection:'row', marginBottom:15,}}>
                                                <View style={{borderRadius:0, marginRight:5, backgroundColor:'white', paddingHorizontal:10 ,paddingVertical:2,}}>
                                                    <Text style={{fontWeight:'bold'}}>Periodik</Text>
                                                </View>
                                                <View style={{borderRadius:0, marginLeft:5, backgroundColor:'white', paddingHorizontal:10 ,paddingVertical:2,}}>
                                                    {data2.periode.includes('m')?(
                                                        <View transparent> 
                                                        {data2.setiap == 'd'?(
                                                            <Text style={{fontWeight:'bold'}}>Daily</Text>
                                                        ):(
                                                            <Text style={{fontWeight:'bold'}}>Monthly</Text>
                                                        )}
                                                        </View>
                                                    ):(
                                                        <Text style={{fontWeight:'bold'}}>Quarterly</Text>
                                                    )}
                                                </View>
                                            </View>
                                            
                                            <View style={{flexDirection:'row',marginBottom:10,}}>
                                                <View style={{flexDirection:'row',}}>
                                                    <Text style={{fontSize:12}}>Tiap Tanggal(Remininder) :</Text>
                                                    <Text style={{fontSize:12, fontWeight:'bold'}}>{data2.periode.replace('m-', ' ')}</Text>
                                                </View>
                                                
                                            </View>
                                        </View>
                                    ):(
                                        <View style={{alignItems:'center'}}>
                                            <View style={{borderRadius:0, flexDirection:'row', marginBottom:15, backgroundColor:'white', paddingHorizontal:10 ,paddingVertical:2,}}>
                                                <Text style={{fontWeight:'bold'}}>Non-Periodik</Text>
                                            </View>
                                            <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
                                                <View style={{marginRight:50}}>
                                                    <Text style={{fontSize:8}}>Pada Tanggal :</Text>
                                                    <Text style={{fontSize:12, fontWeight:'bold'}}>{data2.tgl_begda}</Text>
                                                </View>
                                                <View style={{marginLeft:50}}>
                                                    <Text style={{fontSize:8}}>Sampai Tanggal :</Text>
                                                    <Text style={{fontSize:12, fontWeight:'bold'}}>{data2.tgl_endda}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    )}
                                   
                                   
                                    </View>
                                </View>
                            </View>
                            ))}
                            </View>
                            
                            
                            
                            
                        </View>
                        ))}
                        
                    </View>
                    <View style={styles.slide2}>
                        <View style={{
                                flexDirection:'row', 
                                width:"100%",
                                paddingVertical:12,
                                backgroundColor: colors.bgErm,
                                borderTopLeftRadius:0,
                                borderTopRightRadius:0,
                                
                                }}>

                                <View style={{ flex:4, width:'100%',justifyContent:'center'}}>
                                    <Text style={{fontSize:14, alignSelf:'center'}}>
                                        Dampak Risiko Treated          
                                    </Text>
                                </View>
                        </View>
                        {this.state.dampak.map((data,index)=>(
                        <View key={index} style={{backgroundColor:colors.green04, marginLeft:0, marginTop:0, marginBottom:10, marginRight:0, width:'95%'}}>
                            <View style={{marginVertical:10, marginHorizontal:10}}>
                                <Text style={{fontSize:15, fontWeight:'bold'}}>Dampak Yang Terjadi Oleh Risiko</Text>
                            </View> 
                            <View style={{marginVertical:0, marginBottom:5, marginHorizontal:12}}>
                            <Text style={{marginLeft:5}}>Dampak Risiko</Text>
                                <Textarea
                                        style={{
                                            paddingLeft:10,
                                            marginBottom: 10,
                                            fontSize: 11,
                                            borderWidth:1,
                                            borderRadius:5,
                                            borderColor:colors.gray05,
                                            backgroundColor:colors.white,
                                            height:40,
                                        }}
                                        // disabled
                                        rowSpan={3}
                                        bordered
                                        placeholderTextColor={colors.gray02}
                                        value={data.nama_dampak}
                                        // placeholder="Type Something ..."
                                        onChangeText={text =>
                                            this.setState({ TINDAKAN_PERBAIKAN: text })
                                        }
                                        editable={false}
                                    />
                           
                                <Text style={{marginLeft:5}}>Deskripsi</Text>
                                <Textarea
                                        style={{
                                            paddingLeft:10,
                                            marginBottom: 5,
                                            fontSize: 11,
                                            borderWidth:1,
                                            borderRadius:5,
                                            borderColor:colors.gray05,
                                            backgroundColor:colors.white
                                        }}
                                        // disabled
                                        rowSpan={3}
                                        bordered
                                        placeholderTextColor={colors.gray02}
                                        value={data.desc}
                                        // placeholder="Type Something ..."
                                        onChangeText={text =>
                                            this.setState({ TINDAKAN_PERBAIKAN: text })
                                        }
                                        editable={false}
                                    />
                            </View> 
                            <View style={{marginVertical:5, marginVertical:20, borderWidth:1, borderColor:colors.gray01}}></View>
                            <View style={{paddingHorizontal:10, marginHorizontal:5}}>
                            {this.props.positive_negative == 'negative'?(
                                <Text style={{marginLeft:10}}>Potensi Kerugian</Text>
                            ):(
                                <Text style={{marginLeft:10}}>Potensi Keuntungan</Text>
                            )}
                                <View style={{backgroundColor:colors.gray07, borderRadius:5, marginVertical:5, paddingLeft:15, paddingVertical:10, marginBottom:10}}>
                                <Text>{data.cost}</Text>
                                </View>
                            
                            </View>
                            <View style={{marginVertical:5, marginVertical:20, borderWidth:1, borderColor:colors.gray01}}></View>
                            {data.mitigasi != undefined?(
                                <View>
                                    {data.mitigasi.map((data2, index2)=>(
                                    <View key={index2} style={{paddingHorizontal:10, marginHorizontal:5, marginBottom:10}}>
                                        <Text style={{marginLeft:10}}>Corrective Action</Text>
                                        <View style={{backgroundColor:colors.gray07, borderRadius:5, marginVertical:5, paddingLeft:15, paddingVertical:10, marginBottom:10}}>
                                            <Text>{data2.deskripsi}</Text>
                                        </View>
                                    </View>  
                                    ))}
                                </View>
                            ):(
                                <View style={{paddingHorizontal:10, marginHorizontal:5, marginBottom:10}}>
                                    <Text style={{marginLeft:10}}>Corrective Action</Text>
                                    <View style={{backgroundColor:colors.gray07, borderRadius:5, marginVertical:5, paddingLeft:15, paddingVertical:10, marginBottom:10}}>
                                        <Text> - </Text>
                                    </View>
                                </View>  
                            )}
                            
                            
                        </View>
                        ))}
                        
                        
                    </View>
                </Swiper>
                
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    
    slide1: {
      flex: 1,
      
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:0,
      borderTopLeftRadius:0
    },
    slide2: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:8,
      borderTopLeftRadius:8
    },
   
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    }
  })
