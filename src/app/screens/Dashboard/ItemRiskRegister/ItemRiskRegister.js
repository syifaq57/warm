import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Body,
    Title,
    Button,
    CheckBox,
    Content,
    Card,
    Textarea
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles2 from "../../../res/styles/Form";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/FontAwesome';
import dashstyles from "../../../res/styles/Dashboard";
import SearchableDropdown from 'react-native-searchable-dropdown';
import modal from 'react-native-modal';
import SwipeRisk from './SwipeRisk';



var items = [];
var idProyek=[];

export default class ItemRiskRegister extends Component {
    constructor(props){
        super(props);
        this.state={
            Tahun:[],
            listTahun:[],
            tahunSelect:'',

            isLoading:true,
            isLoadingDepart:false,
            isVisibleDepart:false,
            isLoadingList:false,
            isVisibleButton:false,

            isLoadingRisk:false,
            isVisibleRisk:false,
            isVisibleApprove:false,

            itemDivisi:[],
            itemDepart:[],

            newListDivisi:[],
            selectedItems: [],
            data_user:[],
            id_proyek:'',
            id_bagian_proyek:'',
            nama_bagian_proyek:'',
            stat:'',

            id_divisi:'',
            creator:'',
            riskNegatif:[],
            riskPositif:[],

            approve: '',
            comments:'',
            
            risk : [],

            isModalVisible:false,
            isModalVisibleRevise:false
        
        // listCoba:["a", "b"]
        }
    }

     loadDivisi(){
        var url = GlobalConfig.SERVERHOST + 'api/api/getdivisi';
        var formData = new FormData();
        for(var i=0; i<this.state.id_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.id_proyek[i].id_proyek)
        }
        
        // console.log('data1',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                // console.log(response.data)
                if(response.success == true){
                    var divisi = this.state.itemDivisi
                    for(var i=0; i<response.data.length; i++){
                        divisi.push({id:response.data[i].obj_id, name:response.data[i].obj_name_en})
                    }
                    
                    this.setState({
                        isLoading:false,
                    })
                    // console.log('yuhu',this.state.itemDivisi)
                }
            })
            .catch((error) => {
                this.setState({
                    isLoading:false,
                })
                Alert.alert('Cannot load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }
    
    loadDepart(){
        // this.setState({
        //     isLoadingDepart:false,
        // })
        var url = GlobalConfig.SERVERHOST + 'api/api/getBagianProyek';
        var formData = new FormData();
        formData.append("karyawan", this.state.karyawan_status)
        for(var i=0; i<this.state.id_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.id_proyek[i].id_proyek)
        }
        formData.append("id_divisi", this.state.id_divisi)
        console.log('yuuhuu load depart',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                // console.log('dta', response.data)
                if(response.success == true){
                    
                    var depart = this.state.itemDepart
                    for(var i=0; i<response.data.length; i++){
                        
                        if(response.data[i].obj_level == "PROJECT"){
                            depart.push({id:response.data[i].id_project, name:response.data[i].nama_project})
                        }if(response.data[i].obj_level == "DEPARTMENT" || response.data[i].obj_level == "DEPARTEMENT"){
                            depart.push({id:response.data[i].id_bagian, name:response.data[i].nama_bagian})
                        }if(response.data[i].obj_level == "ANAK USAHA"){
                            depart.push({id:response.data[i].id_project, name:response.data[i].nama_project})
                        }
                        
                    }
                    this.setState({
                        itemDepart:depart,
                        isLoadingDepart:false,
                        isVisibleDepart: true,
                    })
                    console.log('dprt',this.state.itemDepart)
                }
            })
            .catch((error) => {
                this.setState({
                    isLoadingDepart:false,
                    isVisibleDepart: false,
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    AsyncDataUser(){
        // AsyncStorage.getItem('username').then((value)=>{
        //     this.setState({
        //         username: value
        //     })
        // })
        // AsyncStorage.getItem('password').then((value)=>{
        //     this.setState({
        //         password: value
        //     })
        // })
        // this.setState({isLoading:true})
        // AsyncStorage.getItem('data_user').then((value)=>{
            
        //     this.setState({
        //         data_user:JSON.parse(value),
        //         isLoading:false,
               
        //     })
        //     this.setState({
        //         id_proyek:this.state.data_user.bagian_proyek,
        //         // username: this.state.data_user.username,
        //         // password: this.state.data_user.password,
        //         karyawan_status: this.state.data_user.karyawan,
        //         creator: this.state.data_user.creator,
        //         sebagai: this.state.data_user.sebagai,
        //         // status: this.state.data_user.bagian_proyek
                
        //     })
           
               
        //         // console.log('ter'+"[0]", this.state.id_proyek[0].id_proyek)
            
            
        //     // console.log('kry', this.state.id_proyek[0].id_proyek)
        //     this.loadDivisi()
        // })
       
            
    }

    onSearch(){
        AsyncStorage.setItem('tahun_select', this.state.tahunSelect.toString())
        this.setState({
            isLoadingRisk:true,
            riskNegatif:[],
            riskPositif:[]
        })
        var url = GlobalConfig.SERVERHOST + 'api/api/cariRiskPriority';
        var formData = new FormData();
        formData.append("username", this.state.username)
        formData.append("karyawan", this.state.karyawan)
        formData.append("bagian_proyek[0]", this.state.id_proyek[0].id_proyek)
        formData.append("periode", this.state.tahunSelect)
        formData.append("id_divisi", this.state.id_divisi)
        formData.append("id_bagian_proyek", this.state.id_bagian_proyek)
        formData.append("creator", this.state.creator)
        


        console.log('yuuhussssu',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                console.log('dta', response.data)
                if(response.success == true){
                    var risk = this.state.riskNegatif
                    for(var i=0; i<response.data.length; i++){
                        risk.push(
                            {
                                nama_risiko:response.data[i].nama_risiko,
                                nama_main:response.data[i].nama_main,
                                nama_sub:response.data[i].nama_sub,
                                nama_target:response.data[i].nama_target,
                                l_current:response.data[i].l_current,
                                c_current:response.data[i].c_current,
                                positive_negative:response.data[i].positive_negative,
                                id_target:response.data[i].id_target,
                                id_risiko:response.data[i].id_risiko,
                                id_risk_register:response.data[i].id_risk_register,
                                nilai_target:response.data[i].nilai_target,
                                satuan_target:response.data[i].satuan_target,

                            }
                            )
                        
                    }
                    var stat = this.state.id_proyek.find(statp => statp.id_proyek === this.state.id_bagian_proyek)
                    if(stat.stat == 'a1'||'a2'||'a3'){
                        
                        var statSplit = stat.stat.replace('a', 'Status : Approve ')
                        
                    }if(stat.stat == ''){
                        var statSplit = 'Status : Submit'
                    }
                    
                    var sebagaiSplit = stat.role.replace('a', '')

                    // console.log('split',statSplit)
                    this.setState({
                        approve : statSplit,
                        sebagai2 : sebagaiSplit
                    })
                    
                    var filterRiskN = risk.filter(function(riskN){
                        return riskN.positive_negative == 'negative'
                    })
                    var filterRiskP = risk.filter(function(riskN){
                        return riskN.positive_negative == 'positive'
                    })
                    this.setState({
                        riskNegatif: filterRiskN,
                        riskPositif: filterRiskP
                    })
                    // console.log('PN', this.state.riskNegatif)
                    // console.log('PP', this.state.riskPositif)

                    
                    
                }
                var stat = this.state.id_proyek.find(statp => statp.id_proyek === this.state.id_bagian_proyek)
                if (stat.role == 'a1' ){
                    // console.log('submit a1')
                    if(this.state.approve == 'Status : Submit'){
                        
                        this.setState({
                            isVisibleButton:true
                        })
                    }else{
                        this.setState({
                            isVisibleButton:false
                        })
                    }
                }
                if (stat.role == 'a2' ){
                    if(this.state.approve == 'Status : Approve 1'){
                        this.setState({
                            isVisibleButton:true
                        })
                    }else{
                        this.setState({
                            isVisibleButton:false
                        })
                    }
                }
                if (stat.role == 'a3' ){
                    if(this.state.approve == 'Status : Approve 2'){
                        this.setState({
                            isVisibleButton:true
                        })
                    }else{
                        this.setState({
                            isVisibleButton:false
                        })
                    }
                }
                this.setState({
                    isLoadingRisk:false,
                    isVisibleRisk:true,
                })
            })
            .catch((error) => {
                this.setState({
                    isLoadingRisk:false,
                })
                // Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                //     text: 'Okay'
                // }])
                console.log(error)
            })
        
    }

    loadDataUser(){
        
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        // console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        
                        id_proyek:response.data.bagian_proyek,
                        karyawan: response.data.karyawan,
                        creator: response.data.creator,
                        // sebagai: this.state.data_user.sebagai,
                    },function(){
                        this.loadDivisi()
                    })
                    // console.log('kry', this.state.karyawan)
                    // AsyncStorage.setItem('data_user', (JSON.stringify(response.data)))
                    // this.AsyncDataUser()

                }else{
                    

                    Alert.alert('Error', 'Tidak bisa load data', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error) => {
               
                // Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                //     text: 'Okay'
                // }])
                console.log(error)
            })
         
    }
    loadApprove(){
        
        
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        // console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        
                        id_proyek:response.data.bagian_proyek,
                        karyawan: response.data.karyawan,
                        creator: response.data.creator,
                        // sebagai: this.state.data_user.sebagai,
                    })
                    // console.log('kry', this.state.karyawan)
                    // AsyncStorage.setItem('data_user', (JSON.stringify(response.data)))
                    // this.AsyncDataUser()

                }else{
                    

                    Alert.alert('Error', 'Tidak bisa load data', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error) => {
               
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
         
    }

    onPressApprove(){
        this.setState({
            isVisibleRisk:false,
            isLoadingRisk:true
        })
        
        var url = GlobalConfig.SERVERHOST + 'api/api/approve_revise';
        var formData = new FormData();
        formData.append("aksi", 'approve')
        formData.append("approver", this.state.sebagai2)
        formData.append("id_bagian_proyek", this.state.id_bagian_proyek )
        formData.append("username", this.state.username)
        formData.append("comments", 'mobile approve')
        
        // console.log('data1',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                // console.log('dta', response.data)
                if(response.success == true){
                    Alert.alert('Berhasil','Berhasil Menyimpan Data',[{
                        text: 'Okay'
                    }])
                    this.loadApprove()
                    this.onSearch()
                    
                    
                }
            })
            .catch((error) => {
                this.setState({
                    isVisibleRisk:true,
                    isLoadingRisk: false,
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }
    onPressRevise(){
        this.setState({
            isVisibleRisk:false,
            isLoadingRisk:true
        })
        
        var url = GlobalConfig.SERVERHOST + 'api/api/approve_revise';
        var formData = new FormData();
        formData.append("aksi", 'revise')
        formData.append("approver", this.state.sebagai2)
        formData.append("id_bagian_proyek", this.state.id_bagian_proyek )
        formData.append("username", this.state.username)
        formData.append("comments", this.state.comments)
        formData.append("appr", "")
        
        console.log('data1',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                console.log('dta', response.data)
                if(response.success == true){
                    Alert.alert('Berhasil','Berhasil Menyimpan Data',[{
                        text: 'Okay'
                    }])
                    this.loadApprove()
                    this.onSearch()
                    
                    
                }
            })
            .catch((error) => {
                this.setState({
                    isVisibleRisk:true,
                    isLoadingRisk: false,
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }
    navigateToScreen(route){
        this.props.navigation.navigate(route);
        
      }

    getYear(){
        var years = new Date();
        var year = years.getFullYear();
        
        this.setState({
            tahunSelect:year
        }, function(){
            // console.log('tahuuuuuuuuuunnn', this.state.tahunSelect)
            for(var i=2019; i<=this.state.tahunSelect; i++){
                var years =this.state.Tahun
                years.push({id:i, name: i.toString()})
                
            }
            items = years
            this.setState({
            Tahun:years,
            listTahun:this.state.Tahun.map((data)=>(data.value)),
            isLoading:false,
            
        })
        })
    }

    componentDidMount(){
        AsyncStorage.getItem('username').then((value)=>{
            this.setState({
                username: value
            })
        })
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password: value
            },function(){
                this.loadDataUser()
                
            })
        })
        
        // this.AsyncDataUser()
        
        this.getYear();
        // console.log('tahun', items)
        
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

      toggleModalRevise = () => {
        this.setState({ isModalVisibleRevise: !this.state.isModalVisibleRevise });
      };  

    render(){
        return(
            
            <View style={styles2.container}>
            {this.state.isLoading?(
                <View style={{justifyContent:'center',marginTop:10,}}>
                    <ActivityIndicator size='large'/>
                    </View>
                ):(
                <View>
                    <Card style={{marginTop:10, paddingBottom:15, marginLeft:10, marginRight:10,borderBottomLeftRadius:8, borderBottomRightRadius:8, borderTopLeftRadius:8, borderTopRightRadius:8}}>
                        <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:10,
                            backgroundColor: colors.bgErm,
                            borderTopLeftRadius:5,
                            borderTopRightRadius:5,
                            
                            }}>
                            <View style={{ flex:4, width:'100%', marginLeft:25, justifyContent:'center'}}>
                                <Text style={{fontWeight:'bold', color:colors.gray09, fontSize:16}}>
                                Select Risk Register
                                </Text>
                            </View>
                            <View style={{marginTop:1 , alignItems:'flex-end' }}>
                                <View style={{paddingRight: 20, paddingLeft: 40}}>
                                    <Button
                                        block
                                        // onPress={() => this.onLoginPress()}
                                        onPress={() => this.onSearch()}
                                        style={{alignItems:'center', height:35, borderRadius:3, backgroundColor:colors.erm2,  width:70}}
                                        >
                                            <Icon2
                                                name="file-search"
                                                size={27}
                                                color={colors.gray09}
                                            />
                                            <Text style={{color:colors.gray09,fontWeight:'bold' }}>Cari  </Text>
                                    </Button>
                                </View>
                            </View>
                            
                        </View>
                        <View style={{flexDirection:'column', marginTop:10}}>
                            <View style={{width:'100%', marginLeft:25}}>
                                <Text>Periode</Text>
                            </View>
                            <View style={{marginHorizontal:20}}>         
                            <SearchableDropdown
                                onItemSelect={(item) => {
                                const items = this.state.selectedItems;
                                items.push(item)
                                this.setState({ selectedItems: items , tahunSelect:item.id, idcoba:'fuk'});
                                // alert(item.name)
                                // console.log('thn',item.id)
                                }}
                                containerStyle={{ padding: 0 }}
                                onRemoveItem={(item, index) => {
                                const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                                this.setState({ selectedItems: items });
                                }}
                                itemStyle={{
                                padding: 10,
                                marginTop: 2,
                                marginLeft:5,
                                backgroundColor: '#ddd',
                                borderColor: '#bbb',
                                borderWidth: 1,
                                borderRadius: 5,
                                }}
                                itemTextStyle={{ color: '#222' }}
                                itemsContainerStyle={{ maxHeight: 140 }}
                                items={items}
                                
                                textInputProps={
                                {
                                    placeholder: "--Select Years--",
                                    style: {
                                        marginLeft:0,
                                        padding: 7,
                                        borderWidth: 1,
                                        borderColor: '#ccc',
                                        borderRadius: 5,
                                    },
                                    
                                }
                                }
                                listProps={
                                {
                                    nestedScrollEnabled: true,
                                }
                                }
                            />
                            </View>
                        </View>
                        <View style={{flexDirection:'column', marginTop:5}}>
                            <View style={{width:'100%', marginLeft:25}}>
                                <Text>Divisi</Text>
                            </View>
                            <View style={{marginHorizontal:20}}>             
                            <SearchableDropdown
                                onItemSelect={(item) => {
                                const items = this.state.selectedItems;
                                items.push(item)
                                this.setState({ selectedItems: items, id_divisi: item.id, isLoadingDepart:true});
                                this.loadDepart()
                                // alert(item.id)
                                console.log('tt', this.state.id_divisi)
                                }}
                                onRemoveItem={(item, index) => {
                                const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                                this.setState({ selectedItems: items });
                                }}
                                itemStyle={{
                                padding: 10, marginTop: 2,
                                marginLeft:5, backgroundColor: '#ddd',
                                borderColor: '#bbb', borderWidth: 1,
                                borderRadius: 5,
                                }}
                                itemTextStyle={{ color: '#222' }}
                                itemsContainerStyle={{ maxHeight: 140 }}
                                items={this.state.itemDivisi}
                                // defaultIndex={2}
                                
                                textInputProps={
                                {
                                    placeholder: "--Select Divisi--",
                                    underlineColorAndroid: "transparent",
                                    style: {
                                        marginLeft:0,
                                        padding: 7,
                                        borderWidth: 1,
                                        borderColor: '#ccc',
                                        borderRadius: 5,
                                    },  
                                }
                                }
                                listProps={
                                {
                                    nestedScrollEnabled: true,
                                }
                                }
                            />
                            </View>
                        </View>
                        {this.state.isLoadingDepart?(
                            <ActivityIndicator/>
                        ):(<View/>)}
                        {this.state.isVisibleDepart?
                            <View style={{flexDirection:'column', marginTop:5}}>
                            <View style={{width:'100%', marginLeft:25}}>
                                <Text>Depart/Project</Text>
                            </View>
                            <View style={{marginHorizontal:20}}>
                                <SearchableDropdown
                                    onItemSelect={(item) => {
                                    const items = this.state.selectedItems;
                                    items.push(item)
                                    this.setState({ selectedItems: items, id_bagian_proyek: item.id, nama_bagian_proyek: item.name});
                                    // alert(item.id)
                                    }}
                                    onRemoveItem={(item, index) => {
                                    const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
                                    this.setState({ selectedItems: items });
                                    }}
                                    itemStyle={{
                                    padding: 10, marginTop: 2,
                                    marginLeft:5, backgroundColor: '#ddd',
                                    borderColor: '#bbb', borderWidth: 1,
                                    borderRadius: 5,
                                    }}
                                    itemTextStyle={{ color: '#222' }}
                                    itemsContainerStyle={{ maxHeight: 140 }}
                                    items={this.state.itemDepart}
                                    // defaultIndex={2}
                                    
                                    textInputProps={
                                    {
                                        placeholder: "--Select Divisi--",
                                        underlineColorAndroid: "transparent",
                                        style: {
                                            marginLeft:0,
                                            padding: 7,
                                            borderWidth: 1,
                                            borderColor: '#ccc',
                                            borderRadius: 5,
                                        },  
                                    }
                                    }
                                    listProps={
                                    {
                                        nestedScrollEnabled: true,
                                    }
                                    }
                                />
                            </View>
                        </View>:null
                        }
                        {/* <View style={{marginTop:10, alignItems:'flex-end' }}>
                                <View style={{paddingRight: 20, paddingLeft: 40}}>
                                    <Button
                                        block
                                        // onPress={() => this.onLoginPress()}
                                        onPress={() => this.onSearch()}
                                        style={{alignItems:'center', height:35, width:100, borderRadius:3, backgroundColor:'#4C7FF0',  width:70}}
                                        >
                                            <Icon2
                                                name="file-search"
                                                size={27}
                                                color='white'
                                            />
                                            <Text style={{color:'white',fontWeight:'bold' }}>Cari  </Text>
                                    </Button>
                                </View>
                            </View> */}
                       
                        
                    </Card>
                    {this.state.isLoadingRisk?(
                        <ActivityIndicator/>
                    ):(
                        <View/>
                        
                    )}
                    
                    {this.state.isVisibleRisk?
                        
                        <View>
                            <View style={{flex:1, marginTop:10, alignItems:'center'}}>
                                <View style={{flexDirection:'row', justifyContent:'center', marginBottom:10}}>

                                    <View style={{borderRadius:5, borderWidth:1, paddingVertical:5, borderColor:colors.gray02, width:'40%', justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:15, color:colors.greenDefault}}>
                                            {this.state.approve}
                                        </Text>
                                    </View>
                                    
                                    
                                </View>
                                {this.state.isVisibleButton?
                                    <View style={{flexDirection:'row', marginBottom:10}}>
                                
                                    <Button 
                                        style={{backgroundColor:'#4C7FF0', borderRadius:5, width:'30%', paddingHorizontal:10}}
                                  
                                        onPress={this.toggleModal}
                                    >
                                        <Icon
                                            name='check'
                                            size={20}
                                            color='white'
                                        />
                                        <View style={{width:'100%', justifyContent:'center', marginLeft:5}}>
                                            <Text style={{fontWeight:'bold', color:'white', fontSize:15}}>Approve</Text>
                                        </View>
                                    
                                    </Button>
                                    <Modal
                                        isVisible={this.state.isModalVisible}
                                        onBackdropPress={this.toggleModal}
                                        onBackButtonPress={this.toggleModal}
                                    >
                                    <View style={{ borderRadius:8}}>
                                        <View style={{backgroundColor:'#337AB7', justifyContent:'center',borderTopRightRadius:8, borderTopLeftRadius:8}}>
                                            <View style={{flexDirection:'row', padding:10}}>
                                                <Icon2
                                                    name='check'
                                                    size={20}
                                                    color='white'
                                                />
                                                <Text style={{color:'white', fontWeight:'bold'}}>Approve</Text>
                                            </View>
                                        </View>
                                        <View style={{backgroundColor:'white', borderBottomRightRadius:8, borderBottomLeftRadius:8}}>
                                            <View style={{marginVertical:15, marginLeft:15}}>
                                                <Text style={{fontSize:18}}>Approve Risk Register dan Risk Priority pada unit kerja ini?</Text>
                                            </View>
                                            <View style={{ marginBottom:20, marginTop:5, flexDirection:'row', alignItems:'center'}}>
                                                <View style={{flex:3}}></View>
                                                <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                                                    <Button style={{backgroundColor:'#337AB7', height:40, width:70, alignItems:'center', borderRadius:5}}
                                                        onPress={()=>this.onPressApprove()}
                                                    >
                                                        <View style={{marginLeft:6, marginRight:5}}>
                                                            <Text style={{color:'white', textAlign:'center' }}>Approve</Text>
                                                        </View>
                                                    </Button>
                                                </View>
                                                <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                                                    <Button style={{backgroundColor:'#C4C4C4', height:40, width:70, alignItems:'center', borderRadius:5}}
                                                        onPress={this.toggleModal}
                                                    >
                                                    <View style={{marginLeft:10, marginRight:0}}>
                                                            <Text style={{color:'white', }}>Cancel</Text>
                                                        </View>
                                                    </Button>
                                                </View>
                                                
                                                
                                            </View>
                                            
                                        </View>
                                    </View>
                                    

                                    </Modal>
                                    <Button 
                                        // onPress={() => Alert.alert(
                                        //     "Revise","Revise Risk Register dan Risk Priority pada unit kerja ini?",
                                        //     [
                                        //         {text: 'Revise', onPress:() => this.onPressApprove() },
                                        //         {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), 
                                        //             style: 'cancel'},
                                        //     ],
                                        //         { cancelable: true })
                                        //     }
                                        onPress={this.toggleModalRevise}
                                        style={{backgroundColor:'#C22323', borderRadius:5, width:'30%', marginLeft:30,   paddingHorizontal:10}}>
                                    
                                    <Icon
                                        name='close'
                                        size={20}
                                        color='white'
                                    />
                                    <View style={{width:'100%', justifyContent:'center', marginLeft:5}}>
                                        <Text style={{fontWeight:'bold', color:'white', fontSize:15}}>Revise</Text>
                                    </View>
                                    
                                    </Button>
                                    <Modal
                                        isVisible={this.state.isModalVisibleRevise}
                                        onBackdropPress={this.toggleModalRevise}
                                        onBackButtonPress={this.toggleModalRevise}
                                    >
                                    <View style={{ borderRadius:8}}>
                                        <View style={{backgroundColor:'#C22323', justifyContent:'center',borderTopRightRadius:8, borderTopLeftRadius:8}}>
                                            <View style={{flexDirection:'row', padding:10}}>
                                                <Icon
                                                    name='close'
                                                    size={20}
                                                    color='white'
                                                />
                                                <Text style={{color:'white', marginLeft:5, fontWeight:'bold'}}>Revise</Text>
                                            </View>
                                        </View>
                                        <View style={{backgroundColor:'white', borderBottomRightRadius:8, borderBottomLeftRadius:8}}>
                                            
                                            <View style={{borderBottomWidth:1, borderBottomColor:colors.gray05}}>
                                                <View style={{marginVertical:15, marginLeft:15}}>
                                                    <Text style={{fontSize:18}}>Revise risk register dan risk priority pada unit kerja ini ?</Text>
                                                </View>
                                            </View>
                                            <View style={{marginHorizontal:15}}>
                                            <View>
                                                <Text style={[{marginTop:10},styles2.fontLabel]}>Komentar</Text>
                                            </View>
                                            <View>
                                            <Textarea
                                                style={styles2.textArea}
                                                rowSpan={3}
                                                bordered
                                                placeholderTextColor={colors.gray02}
                                                // value={this.state.INCIDENT_DESC}
                                                placeholder="Type Something ..."
                                                onChangeText={text =>
                                                    this.setState({ comments: text })
                                                }
                                                />
                                            </View>
                                            </View>
                                            

                                            <View style={{ marginBottom:20, marginTop:5, flexDirection:'row', alignItems:'center'}}>
                                                <View style={{flex:3}}></View>
                                                <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                                                    <Button style={{ backgroundColor:'#C22323', height:40, width:70, alignItems:'center', borderRadius:5}}
                                                        onPress={()=>this.onPressRevise()}
                                                    >
                                                        <View style={{flexDirection:'row',marginLeft:6, marginRight:5}}>
                                                         <View
                                                            style={{justifyContent:'center'}}
                                                         >
                                                            <Icon
                                                                name='close'
                                                                size={15}
                                                                color='white'
                                                            />
                                                         </View>
                                                            <Text style={{color:'white', textAlign:'center' }}>Revise</Text>
                                                        </View>
                                                    </Button>
                                                </View>
                                                <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                                                    <Button style={{backgroundColor:'#C4C4C4', borderColor:colors.gray05, height:40, width:70, alignItems:'center', borderRadius:5}}
                                                        onPress={this.toggleModalRevise}
                                                    >
                                                    <View style={{marginLeft:10, marginRight:0}}>
                                                            <Text style={{color:'white', }}>Cancel</Text>
                                                        </View>
                                                    </Button>
                                                </View>
                                                
                                                
                                            </View>
                                            
                                        </View>
                                    </View>
                                    

                                    </Modal>
                                </View>:null
                                }
                                
                            </View>
                            <SwipeRisk
                                navigation ={this.props.navigation}
                                dataN ={this.state.riskNegatif}
                                dataP ={this.state.riskPositif}
                                /> 
                        </View>:null
                    }
                    

                </View>
                )}
                
                
              
                
            </View>
            
        )
    }
    
}

