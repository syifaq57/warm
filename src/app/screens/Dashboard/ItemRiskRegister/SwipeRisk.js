import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles2 from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";


import Swiper from 'react-native-swiper';

export default class TabBarRiskRegister extends Component {
    constructor(props){
        super(props);
        this.state={
            index:0,
            DataNegatif:[
                // {
                //     Title : "Penghentian pekerjaan oleh masyarakat sekitar proyek",
                //     Divisi : "Project",
                //     JenisRisiko : "K3 & Lingkungan Sosial",
                //     Target : "Progress proyek bulan depan",
                //     UnitKerja : "Apartemen Solterra",
                //     Kemungkinan : "3",
                //     BesarAkibat : "3",
                // },
                // {
                //     Title : "Keterlambatan penyelesaian proyek",
                //     Divisi : "Project",
                //     JenisRisiko : "Konstruksi dan Manajemen Proyek",
                //     Target : "Progress proyek bulan depan",
                //     UnitKerja : "Apartemen Solterra",
                //     Kemungkinan : "2",
                //     BesarAkibat : "3",
                // },
                // {
                //     Title : "Penghentian pekerjaan oleh masyarakat sekitar proyek",
                //     Divisi : "Project",
                //     JenisRisiko : "K3 & Lingkungan Sosial",
                //     Target : "Progress proyek bulan depan",
                //     UnitKerja : "Apartemen Solterra",
                //     Kemungkinan : "3",
                //     BesarAkibat : "3",
                // },
                // {
                //     Title : "Keterlambatan penyelesaian proyek",
                //     Divisi : "Project",
                //     JenisRisiko : "Konstruksi dan Manajemen Proyek",
                //     Target : "Progress proyek bulan depan",
                //     UnitKerja : "Apartemen Solterra",
                //     Kemungkinan : "2",
                //     BesarAkibat : "3",
                // },
                // {
                //     Title : "Keterlambatan penyelesaian proyek",
                //     Divisi : "Project",
                //     JenisRisiko : "Konstruksi dan Manajemen Proyek",
                //     Target : "Progress proyek bulan depan",
                //     UnitKerja : "Apartemen Solterra",
                //     Kemungkinan : "2",
                //     BesarAkibat : "3",
                // },
                // {
                //     Title : "Keterlambatan penyelesaian proyek",
                //     Divisi : "Project",
                //     JenisRisiko : "Konstruksi dan Manajemen Proyek",
                //     Target : "Progress proyek bulan depan",
                //     UnitKerja : "Apartemen Solterra",
                //     Kemungkinan : "2",
                //     BesarAkibat : "3",
                // },
                
            
            ],
            Datacoba:[]
        }
    }

    componentDidMount(){
        this.setState({
            DataCoba :this.props.data
        })
        console.log('dk', this.state.Datacoba)
        // AsyncStorage.getItem('tahun_select').then((value)=>{
        //     this.setState({
        //         tahun_select:value
        //     },function(){
        //         console.log('tahun_select', this.state.tahun_select)
        //     })
        // })
        
    }
    navigateToScreen(route, id_target, id_risiko, postive_negative){
        // AsyncStorage.setItem('id_target', id_target).then(
        //     AsyncStorage.setItem('id_risk', id_risk).then(
        //         this.props.navigation.navigate(route)
        //     )
        // )
        this.props.navigation.navigate(route)
        AsyncStorage.setItem('id_target', id_target)
        AsyncStorage.setItem('id_risk', id_risiko)
        AsyncStorage.setItem('postive_negative', postive_negative)
        
        
        
    }

    // onPressItemsub(id_target, id_risk)
    
    render(){
        return(
            <Card style={{marginTop:10, marginLeft:10, marginRight:10, borderTopLeftRadius:10, borderTopRightRadius:10}}>                           
                 <Swiper 
                    style={styles.wrapper} 
                    height='100%' 
                    showsButtons 
                    buttonWrapperStyle={{
                        backgroundColor: 'transparent',
                        flexDirection: 'row',
                        position: 'absolute',
                        top: -13,
                        left: 0,
                        flex: 1,
                        paddingHorizontal: 5,
                        paddingVertical: 0,
                        justifyContent: 'space-between',
                        alignItems: 'flex-start'}}
                    >
                 
                <View style={{marginTop:0, marginBottom:100}}>
                <View style={styles.slide1}>
                    <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:15,
                            backgroundColor: colors.bgErm,
                            borderTopLeftRadius:0,
                            borderTopRightRadius:0,
                            
                            }}>

                            <View style={{ flex:4, width:'100%',justifyContent:'center'}}>
                                <Text style={{ color:colors.gray09, fontSize:18, alignSelf:'center'}}>
                                    RISIKO NEGATIVE
                                </Text>
                            </View>
                    </View>  
                       
                </View>
                {this.props.dataN.map((data,index)=>(
                    <Card key={index} style={{marginTop:10, backgroundColor:'#D7ECCC', marginLeft:10, marginRight:10}}>                           
                    <TouchableOpacity onPress={()=>(this.navigateToScreen('ItemSubDetail', data.id_target, data.id_risiko, 'negative'))}>
                        {/* onPress={()=>(this.navigateToScreen('ItemSubDetail'))}> */}
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:1, flexDirection:'column'}}>
                                    <View style={{padding:10}}>
                                        <View style={{width:"100%"}}>
                                            <Text style={{ fontWeight:'bold'}}>	
                                                {data.nama_risiko}
                                            </Text>
                                        </View>
                                        <View style={{flexDirection:'row', marginBottom:10}}>
                                            
                                        </View>
                                        <View style={{width:'100%', marginBottom:5}}>
                                            <Text style={{fontSize:12}}>Divisi / Project</Text>
                                            <Text style={{fontSize:14}}>{data.nama_main}</Text>
                                        </View>
                                        <View style={{width:'100%', marginBottom:5}}>
                                            <Text style={{fontSize:12}}>Jenis Risiko</Text>
                                            <Text style={{fontSize:14}}>{data.nama_sub}</Text>
                                        </View>
                                        <View style={{width:'100%', marginBottom:5}}>
                                            <Text style={{fontSize:12}}>Target</Text>
                                            <Text style={{fontSize:14}}>{data.nama_target}</Text>
                                        </View>
                                        <View style={{width:'100%'}}>
                                            <Text style={{fontSize:12}}>Nilai Target</Text>
                                            <View style={{flexDirection:'row'}}>
                                                <Text style={{fontSize:14}}>{data.nilai_target} </Text><Text>{data.satuan_target}</Text>
                                            </View>
                                        </View>
                                        <View style={{marginTop:10}}>
                                                <Text style={{fontSize:13, color:'#4F8732', fontSize:14, fontWeight:'bold' }}>Kemungkinan : {data.l_current}</Text>
                                            </View>
                                            <View style={{}}>
                                                <Text style={{fontSize:13, color:'#4F8732', fontSize:14, fontWeight:'bold'}}>Besar Akibat   : {data.c_current}</Text>
                                        </View>
                                    </View>
                                </View>
                                
                               
                            </View>
                            
                        </TouchableOpacity>
                        
                    </Card>
                ))}
                </View>

                

                {/* positf */}
                <View style={{marginTop:0, marginBottom:80}}>
                <View style={styles.slide1}>
                    <View style={{
                            flexDirection:'row', 
                            width:"100%",
                            paddingVertical:15,
                            backgroundColor: colors.bgErm,
                            borderTopLeftRadius:0,
                            borderTopRightRadius:0,
                            
                            }}>

                            <View style={{ flex:4, width:'100%',justifyContent:'center'}}>
                                <Text style={{ color:colors.gray09,  fontSize:18, alignSelf:'center'}}>
                                    RISIKO POSITIVE
                                </Text>
                            </View>
                    </View>  
                       
                </View>
                {this.props.dataP.map((data,index)=>(
                    <Card key={index} style={{marginTop:10, backgroundColor:'#D7ECCC', marginLeft:10, marginRight:10}}>                           
                    <TouchableOpacity onPress={()=>(this.navigateToScreen('ItemSubDetail', data.id_target, data.id_risiko, 'positive'))}>
                            <View style={{flexDirection:'row'}}>
                                <View style={{flex:1, flexDirection:'column'}}>
                                    <View style={{padding:10}}>
                                        <View style={{width:"100%"}}>
                                            <Text style={{ fontWeight:'bold'}}>	
                                                {data.nama_risiko}
                                            </Text>
                                        </View>
                                        <View style={{flexDirection:'row', marginBottom:10}}>
                                            
                                        </View>
                                        <View style={{width:'100%', marginBottom:5}}>
                                            <Text style={{fontSize:12}}>Divisi / Project</Text>
                                            <Text style={{fontSize:14}}>{data.nama_main}</Text>
                                        </View>
                                        <View style={{width:'100%', marginBottom:5}}>
                                            <Text style={{fontSize:12}}>Jenis Risiko</Text>
                                            <Text style={{fontSize:14}}>{data.nama_sub}</Text>
                                        </View>
                                        <View style={{width:'100%', marginBottom:5}}>
                                            <Text style={{fontSize:12}}>Target</Text>
                                            <Text style={{fontSize:14}}>{data.nama_target}</Text>
                                        </View>
                                        <View style={{width:'100%'}}>
                                            <Text style={{fontSize:12}}>Nilai Target</Text>
                                            <View style={{flexDirection:'row'}}>
                                                <Text style={{fontSize:14}}>{data.nilai_target} </Text><Text>{data.satuan_target}</Text>
                                            </View>
                                        </View>
                                        {/* <View style={{width:'100%'}}>
                                            <Text>Unit Kerja           : {data.UnitKerja}</Text>
                                        </View> */}
                                        <View style={{marginTop:10}}>
                                                <Text style={{color:'#4F8732', fontSize:14, fontWeight:'bold' }}>Kemungkinan : {data.l_current}</Text>
                                            </View>
                                            <View style={{}}>
                                                <Text style={{color:'#4F8732', fontSize:14, fontWeight:'bold'}}>Besar Akibat   : {data.c_current}</Text>
                                        </View>
                                    </View>
                                </View>
                                
                               
                            </View>
                            
                        </TouchableOpacity>
                        
                    </Card>
                ))}
                </View>
                </Swiper>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    slide1: {
      flex: 1,
      
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:0,
      borderTopLeftRadius:0
    },
    slide2: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:8,
      borderTopLeftRadius:8
    },
   
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    }
  })
