import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions,
    StyleSheet
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content,
    Textarea,
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../../library/network/GlobalConfig'
import Modal from "react-native-modal";
import styles from "../../../res/styles/Login";
import colors from "../../../res/colors";
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/Ionicons';
import dashstyles from "../../../res/styles/Dashboard";
import Sidebar from "../../../library/component/Sidebar";
import Swiper from 'react-native-swiper';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';

import SwipeSubDetail from './SwipeSubDetail'
import { functions } from "react-native-firebase";
export default class ItemSubDetail extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoadingDetail:false,
            visibleKosong:false,
            nowYear:'',
            idDampak:'',
            showSwiper:false,
            dampak:[],
            sebab:[],
            desc_pengendalian:[],
            list_pengnedalian:[],
            DataNegatif:[
                {
                    "status": "berhasil",
                    "isi": [
                        {
                            "l_inherent": "3",
                            "c_inherent": "3",
                            "id_risk_register": "3514",
                            "id_target": "932",
                            "id_risiko": "260",
                            "deskripsi_1": "10% < Kemungkinan terjadinya risiko ? 50%\n\n",
                            "deskripsi_2": "",
                            "l_current": "3",
                            "c_current": "3",
                            "l_target": "2",
                            "c_target": "2",
                            "risk_priority": "0",
                            "is_corporate_risk": "0",
                            "periode_risk": "2020-01",
                            "log_date": "2020-01-24 00:00:00.000000",
                            "id_bagian_proyek": "10004683",
                            "periode": "m",
                            "submitted": "1",
                            "is_closed": "n",
                            "positive_negative": "negative",
                            "kode_main": "3",
                            "nama_main": "Corporate Office",
                            "kode_sub": "11",
                            "nama_sub": "Finansial",
                            "kode_risiko": "153",
                            "nama_risiko": "Realisasi cashflow tidak sesuai dengan rencana",
                            "desc": "",
                            "indicator": "",
                            "keywords": "",
                            "related_risk": "",
                            "nama_target": "Cashflow positif",
                            "l_real": "3",
                            "c_real": "3",
                            "nilai_target": "0",
                            "nilai_real": null
                        }
                    ],
                    "dampak": [
                        {
                            "id": "3125",
                            "id_risk_register": "3514",
                            "id_dampak": "1",
                            "desc": "Posisi kas akhir lebih kecil jika selisih antara kas keluar lebih besar dibandingkan kas masuk\n\n",
                            "cost": "0",
                            "id_risiko": "260"
                        },
                        {
                            "id": "3323",
                            "id_risk_register": "3514",
                            "id_dampak": "3",
                            "desc": "Proses penagihan tertunda yang berakibat terhadap proses pencairan termin",
                            "cost": "0",
                            "id_risiko": "260"
                        },
                        {
                            "id": "3324",
                            "id_risk_register": "3514",
                            "id_dampak": "6",
                            "desc": "Beban Keuangan bertambah",
                            "cost": "0",
                            "id_risiko": "260"
                        }
                    ],
                    "sebab": [
                        {
                            "id_sebab_risiko": "4557",
                            "id_risk_register": "3514",
                            "desc": "Monthly Certificate beserta backup nya tidak lengkap ketika proses invoicing yang mengakibatkan proses penagihan terlambat",
                            "id_penyebab": "841",
                            "id_risiko": "260"
                        },
                        {
                            "id_sebab_risiko": "4558",
                            "id_risk_register": "3514",
                            "desc": "Terlambatnya pembayaran akibat owner tidak memiliki dana (Belum financial close, KI tidak dapat ditarik)\n",
                            "id_penyebab": "843",
                            "id_risiko": "260"
                        }
                    ],
                    "mitigasi": [],
                    "pengendalian": [
                        {
                            "id": "4910",
                            "id_sebab_risiko": "4557",
                            "desc": "Bersama Production Control Division memastikan kelengkapan administrasi penagihan\n",
                            "periode": "m-15",
                            "tgl_begda": null,
                            "tgl_endda": null,
                            "setiap": "m",
                            "pic": "10000175",
                            "biaya": "",
                            "biaya_real": "",
                            "obj_name_en": "SVP - Finance Division"
                        },
                        {
                            "id": "4911",
                            "id_sebab_risiko": "4558",
                            "desc": "Memastikan owner memiliki sumber pendanaan yang memadai, memasukan pasal denda keterlambatan pembayaran di kontrak\n",
                            "periode": null,
                            "tgl_begda": "2020-01-01",
                            "tgl_endda": "2020-03-31",
                            "setiap": null,
                            "pic": "10000175",
                            "biaya": "",
                            "biaya_real": "",
                            "obj_name_en": "SVP - Finance Division"
                        }
                    ],
                    "nilai_target": {
                        "nilai": "0",
                        "kriteria": "",
                        "satuan": "",
                        "satuan_target": null
                    },
                    "satuan_target": "",
                    "option_satuan": "",
                    "internal_control": [
                        {
                            "id": "1783",
                            "id_risk_register": "3514",
                            "desc": "Monitoring Progress pekerjaan dan Berkas MC beserta back up MC secara berkala. \n",
                            "id_risiko": "260"
                        },
                        {
                            "id": "1784",
                            "id_risk_register": "3514",
                            "desc": "Meminta jaminan pembayaran/ meminta pembayaran uang muka pekerjaan \n",
                            "id_risiko": "260"
                        }
                    ]
                }
            
            ]
        }
    }

    

    loadDetail(){
       
        this.setState({
            isLoadingDetail:true
        })
        var url = GlobalConfig.SERVERHOST + 'api/api/detail_risk_register_v2';
        var formData = new FormData();
        formData.append("tahun", this.state.tahun_select)
        formData.append("target", this.state.id_target)
        formData.append("risk", this.state.id_risk)
        formData.append("positive_negative", this.state.postive_negative)
        
        console.log('tes  ssdetail',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('bug')
                if(response.status == "berhasil"){
                    this.setState({
                        nama_risiko:response.isi[0].nama_risiko,
                        deskripsi_1:response.isi[0].deskripsi_1,
                        deskripsi_2:response.isi[0].deskripsi_2,
                        l_current:response.isi[0].l_current,
                        c_current:response.isi[0].c_current,

                    },function(){ 
                        var dampak = this.state.dampak
                        for(var i=0; i<response.dampak.length; i++){
                            dampak.push({
                                cost:response.dampak[i].cost,
                                desc:response.dampak[i].desc,
                                mitigasi:response.dampak[i].mitigasi,
                            })
                        }
                        this.setState({
                            dampak:dampak,
                        },function(){
                            console.log('gk gk',this.state.dampak)
                            this.setState({isLoadingDetail:false})
                           
                        })

                        
                        var sebab = this.state.sebab
                        for(var i=0; i<response.sebab.length; i++){
                            sebab.push({
                                // cost:response.sebab[i].cost,
                                desc:response.sebab[i].desc,
                                id_sebab_risiko:response.sebab[i].id_sebab_risiko,
                                pengendalian:response.sebab[i].pengendalian,
                                penyebab:response.sebab[i].penyebab,
                            })
                            // console.log('tes sebab detail', response)
                        }
                        this.setState({
                            sebab:sebab,
                        })
                    })
                   
                }else{
                    
                }
                
                
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingDetail:false
                })
                console.log(error)
            })
         
    }
    componentDidMount(){
        
        AsyncStorage.getItem('tahun_select').then((value)=>{
            this.setState({
                tahun_select:value
            },function(){
                console.log('tahun_select', value)
            })
        })
        AsyncStorage.getItem('postive_negative').then((value)=>{
            this.setState({
                postive_negative:value
            },function(){
                console.log('postive_negative', value)
            })
        })
        AsyncStorage.getItem('id_risk').then((value)=>{
            this.setState({
                id_risk:value
            },function(){
                console.log('idrisk', value)
            })
        })
        AsyncStorage.getItem('id_target').then((value)=>{
            this.setState({
                id_target:value
            },function(){
                this.loadDetail()
            })
        })
    }
    setSwiperVisible(visible){
        this.setState({showSwiper: visible});
      }
    
    render(){
        return(
            <Container>
            <Header
                style={{ backgroundColor: '#AFDEF5', marginTop:Platform.OS === "ios" ? 0 : 15,}}
                androidStatusBarColor={'#68C1EC'}>
                <View style={{flex:6, flexDirection:"row"}}>
                <TouchableOpacity
                    style={{ marginLeft:5, justifyContent:"center"}}   
                    onPress={()=>this.props.navigation.goBack()}
                    >
                    <Icon
                    
                        name="md-arrow-back"
                        size={25}
                        color={colors.gray09}
                    />
                </TouchableOpacity>
                <View style={{flex:1, width:"100%", justifyContent:"center", marginLeft:15}}>
                    <Text style={{color:colors.gray09, fontWeight:"bold",  fontSize:21}}>Detail Risk Register</Text>
                </View>   
                </View>
            </Header>
            <Content>
            <View style={{paddingHorizontal:10}}>  
                        <Card style={{marginTop:10, backgroundColor:colors.bgErm, paddingHorizontal:10, paddingBottom:10, paddingTop:10}}>
                            <Text style={{ fontSize:16, color:colors.black}}>{this.state.nama_risiko}</Text>
                        </Card>
                        <Card style={{marginTop:10, backgroundColor:colors.green04, paddingHorizontal:10, paddingBottom:10}}>   
                            <View style={{marginTop:2}}>
                                <Text style={{marginTop:5, marginLeft:3,  color:colors.black}}>Penjelasan KM Treated</Text>
                                <View>
                                    <Textarea
                                        style={{
                                            paddingLeft:10,
                                            marginBottom: 5,
                                            fontSize: 11,
                                            borderWidth:1,
                                            borderRadius:5,
                                            borderColor:colors.gray05,
                                            backgroundColor:'white'
                                        }}
                                        // disabled
                                        rowSpan={3}
                                        bordered
                                        placeholderTextColor={colors.gray02}
                                        value={this.state.deskripsi_1}
                                        // placeholder="Type Something ..."
                                        onChangeText={text =>
                                            this.setState({ TINDAKAN_PERBAIKAN: text })
                                        }
                                        editable={false}

                                    />
                                </View>
                                {/* <Text style={{marginTop:5, marginLeft:3, fontSize:12, color:colors.gray09}}>Penjelasan DM Treated</Text>
                                <View>
                                    <Textarea
                                        style={{
                                            paddingLeft:10,
                                            marginBottom: 5,
                                            fontSize: 11,
                                            borderWidth:1,
                                            borderRadius:5,
                                            borderColor:colors.gray05,
                                            backgroundColor:'white',
                                        }}
                                        disabled
                                        rowSpan={3}
                                        bordered
                                        placeholderTextColor={colors.gray02}
                                        value={this.state.deskripsi_2}
                                        // placeholder="Type Something ..."
                                        onChangeText={text =>
                                            this.setState({ TINDAKAN_PERBAIKAN: text })
                                        }
                                        editable={false}

                                    />
                                </View> */}
                            </View>
                        </Card>
                        <Card style={{marginTop:10, backgroundColor:colors.green04, paddingHorizontal:10, paddingBottom:10, paddingTop:10}}>
                            <View style={{marginHorizontal:'10%', flexDirection:'row'}}>
                                <Card style={{flexDirection:'row', backgroundColor:'white', paddingHorizontal:10 ,paddingVertical:5, marginRight:'5%'}}>
                                    <Text>KM Treated : </Text><Text style={{fontWeight:'bold'}}>{this.state.l_current}</Text>
                                </Card>
                                <Card style={{flexDirection:'row', backgroundColor:'white', paddingHorizontal:10 ,paddingVertical:5, marginLeft:'5%'}}>
                                    <Text>DM Treated : </Text><Text style={{fontWeight:'bold'}}>{this.state.c_current}</Text>
                                </Card>
                            </View>
                        </Card>
                    </View>
                    <SwipeSubDetail 
                        dampak = {this.state.dampak}
                        sebab = {this.state.sebab}
                        positive_negative= {this.state.postive_negative}
                    />
            </Content>
           
            </Container>
            
            
            
            
        )
    }
}
const styles3 = StyleSheet.create({
    wrapper: {
    },
    slide1: {
      flex: 1,
      
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:0,
      borderTopLeftRadius:0
    },
    slide2: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: 'white',
      borderTopRightRadius:8,
      borderTopLeftRadius:8
    },
   
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    }
  })