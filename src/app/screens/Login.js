import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    TouchableOpacity,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Icon,
    CheckBox,
    Card
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
import Icon2 from "react-native-vector-icons/FontAwesome";
// import LinearGradient from "react-native-linear-gradient";

import GlobalConfig from '../../app/library/network/GlobalConfig'
import styles from "../res/styles/Login";
import colors from "../res/colors";
import { Colors } from "react-native/Libraries/NewAppScreen";
// import NotifService from '../../../notifService';
import firebase from "react-native-firebase";
import moment from "moment";

export default class Login extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            username: "",
            password: "",
            visibleDialogSubmit: false,
            secureText:true,
            secureIcon:'eye-slash'
               
       
        };  
    }

    static navigationOptions = {
        header: null
    };

    exitApp() {
        Alert.alert(
            'Confirmation',
            'Exit SHE Mobile?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false }
        );
    }

    handleBackPress = () => {
        this.exitApp(); // works best when the goBack is async
        return true;
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        console.log("before fcmToken: ", fcmToken);
        if (!fcmToken) {
          fcmToken = await firebase.messaging().getToken();
          if (fcmToken) {
            console.log("after fcmToken: ", fcmToken);
            await AsyncStorage.setItem('fcmToken', fcmToken);
          }
        }
      }

      async requestPermission() {
        firebase.messaging().requestPermission()
          .then(() => {
            this.getToken();
          })
          .catch(error => {
            console.log('permission rejected');
          });
      }

      async checkPermission() {
        firebase.messaging().hasPermission()
          .then(enabled => {
            if (enabled) {
              console.log("Permission granted");
              this.getToken();
            } else {
              console.log("Request Permission");
              this.requestPermission();
            }
          });
      }

      createNotificationChannel = async () => {
        // Build a android notification channel
        const channelExisting = await firebase.notifications().android.getChannel("reminder");
        console.log(channelExisting)
        if (channelExisting==null){
            console.log("masuk create")
            const channel = new firebase.notifications.Android.Channel(
                "reminder", // channelId
                "Reminders Channel", // channel name
                firebase.notifications.Android.Importance.High // channel importance
            ).setDescription("Used for getting reminder notification"); // channel description
            // Create the android notification channel
            firebase.notifications().android.createChannel(channel);
        }
    };


    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
       
        this.notificationListener = firebase.notifications().onNotification(async (notification) => {
            // const { title, body } = notification;
            await firebase.notifications().displayNotification(notification);
            console.log('notif', notification)
            // this.props.navigation.navigate("ListAprove");
        
        });
      
        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
       this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        const { title, body } = notificationOpen.notification;
        // this.showAlert(title, body);
        this.props.navigation.navigate("ListAprove");
    });
  
    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { title, body } = notificationOpen.notification;
        console.log('ttl');
        // this.showAlert(title, body);
        this.props.navigation.navigate("ListAprove");
    }else{
        console.log('su');
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
      console.log('fv');
    //   this.props.navigation.navigate("ListAprove");
    });
    } 
      
    showAlert(title, body) {
        Alert.alert(
          title, body,
          [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
          { cancelable: false },
        );
    }
  
    componentWillUnmount() {
        // this.notificationListener();
        // this.notificationOpenedListener();
        this.messageListener();

        AsyncStorage.getItem('fcmToken').then((value)=>{
            this.setState({
                tokenFB:value
            })
        })
      }

    componentDidMount() {
        
        //ios
        firebase.messaging().hasPermission()
      .then(enabled => {
        if (enabled) {
          firebase.messaging().getToken().then(token => {
            console.log("LOG: ", token);
          })
          // user has permissions
        } else {
          firebase.messaging().requestPermission()
            .then(() => {
              alert("User Now Has Permission")
            })
            .catch(error => {
              alert("Error", error)
              // User has rejected permissions  
            });
        }
      });
      this.createNotificationChannel();

        this.checkPermission();
        this.createNotificationListeners();
        
           
        AsyncStorage.getItem('fcmToken').then((value)=>{
            this.setState({
                tokenFB:value
            }, function(){
                console.log('cmnn dd mon', this.state.tokenFB)
            })
        })
        // this.cek() 
        

        
        AsyncStorage.getItem('username').then((value)=>{
            this.setState({
                username: value
            })
        })
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password: value
            })
        })
        this._onFocusListener = this.props.navigation.addListener(
            "didFocus",
            payload => {
                this.backHandler =BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
                this.setState({ registerToken: "" });
            }
        );

        const didBlurSubscription = this.props.navigation.addListener(
            'didBlur',
            payload => {
              console.log("masuk blur")
              this.backHandler.remove();
            }
          );
    }

    saveUerPass(){
        AsyncStorage.setItem('username', this.state.username)
        AsyncStorage.setItem('password', this.state.password)
    }

    exitApp() {
        console.log('exit')
        Alert.alert(
            'Confirmation',
            'Exit WARM Mobile?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false }
        );
    }

    onLoginPress = () => {
        // this.props.navigation.navigate("TabNav")
        this.setState({
            visibleDialogSubmit: true
        })
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        console.log(formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log(response)
                if (response.success == true ) {
                    this.saveUerPass()
                    AsyncStorage.setItem('data_user', (JSON.stringify(response.data))).then(() => {
                        this.setState({
                             password: '',
                            visibleDialogSubmit: false,
                            // visibleWelcome: true
                        },function(){
                            AsyncStorage.getItem('fcmtoken').then((value)=>{
                                this.setState({
                                    tokenFireBase:value
                                },function(){
                                    console.log('this.stae.login', this.state.tokenFB)
                                    this.onNextLogin()
                                })
                            })
                        })
                        this.props.navigation.navigate("SplashScreen")

                    })
                }else{
                    this.setState({
                        visibleDialogSubmit: false
                    })

                    Alert.alert('Cannot Log in', response.msg, [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error) => {
                this.setState({
                    visibleDialogSubmit: false
                })
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
         
    }

    onNextLogin(){
        AsyncStorage.setItem("idlogin", "1")
        var url = GlobalConfig.SERVERHOST + 'api/api/sendToken';
        var formData = new FormData();
        formData.append("username", this.state.username)
        formData.append("device_id", this.state.tokenFB)
        console.log('send token',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log(response)
                if (response.success == true ) {
                   console.log(response.username)
                }else{
                    
                }
            })
            .catch((error) => {
                this.setState({
                    visibleDialogSubmit: false
                })
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    render() {
        return (
            <Container style={styles.wrapper}>
                {}
                <ImageBackground
                     source={require('../res/images/warmBG.jpg')} 
                     style={{width: '100%', height: '100%',backgroundColor:colors.lightBlack, paddingTop:0}}
                     
                >
                    
                    <KeyboardAvoidingView 
                        style={styles.wrapper}
                        behavior="padding"
                       
                    >
                    
                        <StatusBar
                            // backgroundColor="transparent" 
                            translucent={true}
                            barStyle="light-content"
                        />
                        
                        <View style={styles.scrollViewWrapper}>
                        {/* <View
                            style={{
                                marginTop:50,
                                marginRight:15,
                                alignItems:'flex-end',
                                marginBottom:0
                            }}
                        >
                            <TouchableOpacity
                            
                                style={{marginTop:30, position:"absolute"}}
                                transparent
                                onPress={()=>this.exitApp()}
                            >
                                <Icon2
                                name="remove"
                                size={25}
                                color="rgba(255,0,0,0.5)"
                                />
                            </TouchableOpacity>
                            
                        </View> */}
                            <View style={styles.scrollView}>
                                <View style={{flex:1}}>
                                    <View style={{ borderColor: colors.black, flexDirection: "column", flex: 1, marginTop: 20, marginBottom:10}}>
                                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center"}}>
                                            <Image
                                                source={require('../res/images/LOGO.png')}
                                                style={{width:300, height:105}}
                                            />
                                        </View>
                                    </View>
                                    
                                </View> 

                                <View style={{flex:1}}>
                                    <Form style={{marginLeft:10, marginRight:0}}>
                                        <Item style={[styles.inputItem,{marginTop:0, marginBottom:10,  paddingVertical:25, borderRadius:5}]}>
                                            {}
                                            <Input returnKeyType='next'  style={styles.input} value={this.state.username}  placeholder="Username" placeholderTextColor={colors.gray09} 
                                                onChangeText={(text) => this.setState({ username: text })}
                                                
                                            />
                                        </Item>
                                        <Item last style={[styles.inputItem,{marginBottom: 25,  paddingVertical:25, borderRadius:5}]}>
                                            {}
                                            <View style={{flexDirection:'row',flex:1,height:40,borderRadius:8,marginRight:10}}>
                                                <Input returnKeyType='go' style={[styles.input]} secureTextEntry={this.state.secureText} value={this.state.password}  placeholder="Password" placeholderTextColor={colors.gray09} onChangeText={(text) => this.setState({ password: text })} />
                                                <View style={{ alignSelf:'center', }}> 
                                                    <Icon2
                                                        onPress={()=>this.setState({
                                                            secureText:this.state.secureText?false:true,
                                                            secureIcon:this.state.secureIcon=="eye"?"eye-slash":"eye"})
                                                            }
                                                        name={this.state.secureIcon}
                                                        color={colors.gray092}
                                                        style={{ fontSize: 20, paddingLeft: 0,alignSelf:'center',marginLeft:5,marginRight:10 }}
                                                    />
                                                </View>  
                                            </View>
                                        </Item>
                                        <View style={{ paddingRight: 40, paddingLeft: 40, marginBottom:30 }}>
                                            <Button
                                                block
                                                // onPress={() => this.onLoginPress()}
                                                onPress={() => this.onLoginPress()}
                                                style={styles.loginButton}
                                            >
                                                <Text style={styles.textButton}>LOGIN</Text>
                                            </Button>
                                        </View>
                                     </Form>
                                </View>

                            </View>
                            <View style={{ width: 270, position: "absolute" }}>
                                <Dialog
                                    visible={this.state.visibleDialogSubmit}
                                    dialogTitle={<DialogTitle title="Authenticating .." />}
                                >
                                    <DialogContent>
                                        {<ActivityIndicator size="large" color="#330066" animating />}
                                    </DialogContent>
                                </Dialog>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ImageBackground>
                <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                        visible={this.state.visibleWelcome}
                        dialogAnimation={
                            new SlideAnimation({
                                slideFrom: "bottom"
                            })
                        }
                    >
                        <DialogContent
                            style={{
                                backgroundColor: "white", height: 150, width: 300
                            }}
                        >
                            <View style={{}}>
                                <View style={{ alignItems: "center" }}>
                                    <Text style={{ marginTop: 20, fontWeight: "bold", fontSize: 17, color: 'black' }}>
                                        Selamat Datang,
                                    </Text>
                                    <Text style={{ fontWeight: "bold", fontSize: 17, color: 'black' }}>
                                        {this.state.nameUser}!
                                </Text>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: "row", marginTop: 20, marginBottom: 20, }}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                                        <Button
                                            small
                                            style={{
                                                width: 60,
                                                borderWidth: 1,
                                                backgroundColor: "#57CEC9",
                                                borderColor: "#57CEC9",
                                                borderRadius: 4,
                                                justifyContent: 'center',
                                            }}
                                            onPress={() => this.setState({ visibleWelcome: false })}
                                        >
                                            <Text style={{ color: "white", fontWeight: "bold", fontSize: 12 }}>CLOSE</Text>
                                        </Button>

                                        

                                    </View>
                                </View>
                            </View>
                            
                        </DialogContent>
                    </Dialog>
                </View>
                {}
                
            </Container>
           
        );
    }


    
}
