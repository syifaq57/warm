import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content,
    Textarea,
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../library/network/GlobalConfig'
import Modal from "react-native-modal";

import styles from "../../res/styles/Login";
import styles2 from "../../res/styles/Form";
import colors from "../../res/colors";
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SwipeRisk from '../Dashboard/ItemRiskRegister/SwipeRisk';
import dashstyles from "../../res/styles/Dashboard";

import ItemAprove from "./itemAprove";
import SubMenuAllunitDashboard from "../../library/component/SubMenuAllunitDashboard";


//Risk By


export default class RiskAprove extends Component {
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            visibleDialogSubmit: false,
            isModalVisible: false,
            visible:true,
            isModalVisibleRevise:false,
            isModalVisibleApprove:false,

            allRisk:[],
            riskNegatif:[],
            riskPositif:[],
            isLoadingSwipeRisk:true,
        };
    }
    
    navigateToScreen(route){
        // this.setState({
        //     isModalVisible:false
        // })
        this.props.navigation.navigate(route);
        
      }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    toggleModalAprove = () => {
        this.setState({ isModalVisibleApprove: !this.state.isModalVisibleApprove });
    };  
    toggleModalRevise = () => {
        this.setState({ isModalVisibleRevise: !this.state.isModalVisibleRevise });
    };  
    
    loadDataUser(){
        this.setState({
            isLoading:true
        })
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('resp data',response)
                if (response.success == true) {
                    this.setState({
                        
                        username:response.data.username,
                        karyawan: response.data.karyawan,
                        creator: response.data.creator,
                        sebagai:response.data.sebagai,
                        isLoading:false  
                    },function(){
                        var sebagaiSplit = this.state.sebagai.replace('a', '')
                        this.setState({
                            sebagai2:sebagaiSplit
                        })
                        this.loadRiskAprove();
                    })
                }else{
                    
                    this.setState({
                        isLoading:false
                    })
                    
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingAll:false
                })
                console.log(error)
            })
    }  

    loadRiskAprove(){
        AsyncStorage.getItem('id_proyek').then((id_proyek)=>{
            var url = GlobalConfig.SERVERHOST + 'api/api/getApproveDivision';
            var formData = new FormData();
            formData.append("username", this.state.username)
            formData.append("karyawan", this.state.karyawan)
            formData.append("bagian_proyek[0]", id_proyek.replace("'", "").replace("'", ""))
            formData.append("creator", this.state.creator)
            console.log('frmdta riskaprove', formData)
            fetch(url, {
                method: 'POST',
                body: formData
            }).then((response) => response.json())
            .then((response) => {
                console.log('resp data',response)
                if (response.success == true) {
                    var risk = this.state.allRisk
                    for(var i=0; i<response.data.length; i++){
                        for(var j=0; j<response.data[i].isi.length; j++){
                            risk.push(
                                {
                                    nama_risiko:response.data[i].isi[j].nama_risiko,
                                    nama_main:response.data[i].isi[j].nama_main,
                                    nama_sub:response.data[i].isi[j].nama_sub,
                                    nama_target:response.data[i].isi[j].nama_target,
                                    l_current:response.data[i].isi[j].l_current,
                                    c_current:response.data[i].isi[j].c_current,
                                    positive_negative:response.data[i].isi[j].positive_negative
    
                                }
                            )
                        }
                          
                    }
                    // this.setState({
                    //     tesrisk:risk
                    // }, function(){
                    //     console.log('tesrisk', this.state.tesrisk)
                    // })
                    var filterRiskN = risk.filter(function(riskN){
                        return riskN.positive_negative == 'negative'
                    })

                    var filterRiskP = risk.filter(function(riskN){
                        return riskN.positive_negative == 'positive'
                    })
                    this.setState({
                        riskNegatif: filterRiskN,
                        riskPositif: filterRiskP
                    }, function(){
                        console.log('risknegatif', this.state.riskNegatif)
                        this.setState({
                            isLoadingSwipeRisk:false
                        })
                    })
                    
                    
                }else{
                    
                    this.setState({
                        isLoading:false,
                        isLoadingSwipeRisk:false
                    })
                    
                }
            })
            .catch((error) => {
                
                // Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                //     text: 'Okay'
                // }])
                this.setState({
                    isLoadingAll:false,
                    isLoadingSwipeRisk:false
                })
                console.log(error)
            })
        })
        
    }

    onPressApprove(){
        
        AsyncStorage.getItem('id_proyek').then((id_proyek)=>{
            var url = GlobalConfig.SERVERHOST + 'api/api/approve_revise';
            var formData = new FormData();
            formData.append("aksi", 'approve')
            formData.append("approver", this.state.sebagai2)
            formData.append("id_bagian_proyek", id_proyek.replace("'", "").replace("'", ""))
            formData.append("username", this.state.username)
            formData.append("comments", 'mobile approve')
            
            console.log('data1',formData)
            fetch(url,{
                method:'POST',
                body:formData
            }).then((response) => response.json())
                .then((response)=>{
                    // console.log('dta', response.data)
                    if(response.success == true){
                        Alert.alert('Berhasil','Berhasil Menyimpan Data',[{
                            text: 'Okay'
                        }])
                        this.navigateToScreen('Dashboard')
                        
                    }else{
                        Alert.alert('Gagal','Gagal Menyimpan Data',[{
                            text: 'Okay'
                        }])
                    }
                })
                .catch((error) => {
                    Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [{
                        text: 'Okay'
                    }])
                    console.log(error)
            })
        })
        
    }
    onPressRevise(){
        AsyncStorage.getItem('id_proyek').then((id_proyek)=>{
        var url = GlobalConfig.SERVERHOST + 'api/api/approve_revise';
        var formData = new FormData();
        formData.append("aksi", 'revise')
        formData.append("approver", this.state.sebagai2)
        formData.append("id_bagian_proyek", id_proyek.replace("'", "").replace("'", ""))
        formData.append("username", this.state.username)
        formData.append("comments", this.state.comments)
        formData.append("appr", "")
        
        console.log('data2',formData)
        fetch(url,{
            method:'POST',
            body:formData
        }).then((response) => response.json())
            .then((response)=>{
                console.log('dta', response.data)
                if(response.success == true){
                    Alert.alert('Berhasil','Berhasil Menyimpan Data',[{
                        text: 'Okay'
                    }])
                    this.navigateToScreen('Dashboard')
                    
                }else{
                    Alert.alert('Gagal','Gagal Menyimpan Data',[{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error) => {
              
                Alert.alert('Cannot Save Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
        })    
    }

    componentDidMount(){
        AsyncStorage.getItem('username').then((value)=>{
            this.setState({
                username: value
            })
        })
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password: value
            },function(){
                this.loadDataUser()
                
            })
        })
        
    }
      
    render(){

        return(
            <Container style={styles.wrapper}>
                <Header
                    style={{ backgroundColor: '#AFDEF5', marginTop:Platform.OS === "ios" ? 0 : 15,}}
                    androidStatusBarColor={'#68C1EC'}
                    >
                    
                    <View style={{flex:6, flexDirection:"row"}}>
                        <TouchableOpacity
                            style={{ marginLeft:5, justifyContent:"center"}}   
                            onPress={()=>this.props.navigation.goBack()}
                            >
                            <Icon2 
                            
                                name="md-arrow-back"
                                size={25}
                                color={colors.gray09}
                            />
                            
                            
                        </TouchableOpacity>
                        <View style={{flex:1, width:"100%", justifyContent:"center", marginLeft:15}}>
                            <Text style={{color:colors.gray09, fontWeight:"bold",  fontSize:22}}>Risk Register</Text>
                        </View>   
                    </View>
                    <Right style={{flex:1, marginLeft:0, justifyContent:"center", marginRight:5}}>
                        
                        {/* <TouchableOpacity
                            onPress={this.toggleModal}>
                            <Icon
                                    style={{alignSelf:"flex-end"}}
                                    name="apps"
                                    size={30}
                                    color="white"
                                />
                        </TouchableOpacity> */}
                        <Modal
                            style={{marginHorizontal:0,marginBottom:0}}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}
                            onBackButtonPress={this.toggleModal}
                            onSwipeComplete={this.toggleModal}
                            swipeDirection="down"
                        >
                            <View style={{ flex:1, justifyContent:"flex-end", }}>
                                <View style={{backgroundColor:'white', borderTopLeftRadius:5, borderTopRightRadius:5,}}>
                                    <View style={{width:"100%",alignContent:"center", backgroundColor:colors.gray09, borderTopLeftRadius:4, borderTopRightRadius:4, alignItems:'center', padding:13, borderBottomColor:colors.gray03, borderBottomWidth:1 }}>
                                        <Text style={{fontSize:17, fontWeight:"bold", color:'white'}}>Dashboard</Text>
                                    </View>
                                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                                        <TouchableOpacity onPress={()=>(this.navigateToScreen("AllUnitDashboard"))} style={{flexDirection:"row"}}>
                                            <View style={{padding:10, flexDirection:"row"}}>
                                                <Image
                                                    source={require('../../res/images/chart.png')}
                                                    style={{width:30, height:30, marginRight:10}}
                                                />
                                                <View style={{justifyContent:"center", width:"100%"}}>
                                                    <Text style={{fontWeight:"bold"}}>
                                                        My Dashboard
                                                    </Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                                    <TouchableOpacity onPress={()=>(this.navigateToScreen("RiskRegister"))} style={{flexDirection:"row"}}>
                                            <View style={{padding:10, flexDirection:"row"}}>
                                                <Image
                                                    source={require('../../res/images/corporate.png')}
                                                    style={{width:30, height:30, marginRight:10}}
                                                />
                                                <View style={{justifyContent:"center", width:"100%"}}>
                                                    <Text style={{fontWeight:"bold"}}>
                                                        Risk Register
                                                    </Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                
                                </View>
                            
                            </View>
                        </Modal>
                        
                        
                    </Right>
                </Header>
                <View style={{
                    flexDirection:'row',
                    marginTop:0,
                    paddingTop:Platform.OS === "ios" ?10:0,
                    paddingBottom:15,
                    alignSelf:'center',
                    backgroundColor:colors.bgErm,
                    alignContent:'center',
                    borderBottomLeftRadius:25,
                    borderBottomRightRadius:25,
                    
                    }}>
                    <Button 
                        style={{
                            backgroundColor:'#4C7FF0', 
                            borderRadius:5, 
                            width:'45%', 
                            paddingHorizontal:10,
                            marginRight:5,
                            marginLeft:20,
                        }}                  
                        onPress={this.toggleModalAprove}
                    >
                        <View style={{flexDirection:'row', marginLeft:20, marginRight:20}}>
                            <Icon
                                style={{justifyContent:'center'}}
                                name='check'
                                size={25}
                                color='white'
                            />
                            <View style={{width:'100%', justifyContent:'center', marginLeft:2}}>
                                <Text style={{fontWeight:'bold', color:'white', fontSize:17}}>Approve</Text>
                            </View>  
                        </View>     
                    </Button>
                    <Button 
                        style={{
                            backgroundColor:'#C22323', 
                            borderRadius:5, 
                            width:'45%', 
                            paddingHorizontal:10,      
                            marginLeft:5,
                            marginRight:20,  
                        }}                  
                        onPress={this.toggleModalRevise}
                    >
                        <View style={{flexDirection:'row', marginLeft:25, marginRight:25}}>
                            <Icon
                                style={{justifyContent:'center'}}
                                name='close'
                                size={25}
                                color='white'
                            />
                            <View style={{width:'100%', justifyContent:'center', marginLeft:2}}>
                                <Text style={{fontWeight:'bold', color:'white', fontSize:17}}>Revise</Text>
                            </View>  
                        </View>                                      
                    </Button>
                </View>                        
                <Content>
                    <View
                        style={{
                            marginTop:10
                        }}
                    >
                        {this.state.isLoadingSwipeRisk?(
                            <View style={{marginTop:10}}>
                                <ActivityIndicator size='large'/>
                            </View>
                        ):(
                            <SwipeRisk 
                                dataN ={this.state.riskNegatif}
                                dataP ={this.state.riskPositif}
                            /> 
                        )}
                        
                    </View>
                </Content>

                {/* //modal approve */}
            <Modal
                isVisible={this.state.isModalVisibleApprove}
                onBackdropPress={this.toggleModalAprove}
                onBackButtonPress={this.toggleModalAprove}                        
                >
                <View style={{ borderRadius:8}}>
                    <View style={{backgroundColor:'#337AB7', justifyContent:'center',borderTopRightRadius:8, borderTopLeftRadius:8}}>
                        <View style={{flexDirection:'row', padding:10}}>
                            <Icon
                                name='check'
                                size={20}
                                color='white'
                            />
                            <Text style={{color:'white', fontWeight:'bold'}}>Approve</Text>
                        </View>
                    </View>
                    <View style={{backgroundColor:'white', borderBottomRightRadius:8, borderBottomLeftRadius:8}}>
                        <View style={{marginVertical:15, marginLeft:15}}>
                            <Text style={{fontSize:18}}>Approve Risk Register dan Risk Priority pada unit kerja ini?</Text>
                        </View>
                        <View style={{ marginBottom:20, marginTop:5, flexDirection:'row', alignItems:'center'}}>
                        <View style={{flex:3}}></View>
                        <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                            <Button style={{backgroundColor:'#337AB7', height:40, width:70, alignItems:'center', borderRadius:5}}
                                onPress={()=>this.onPressApprove()}
                            >
                                <View style={{marginLeft:6, marginRight:5}}>
                                    <Text style={{color:'white', textAlign:'center' }}>Approve</Text>
                                </View>
                            </Button>
                        </View>
                        <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                            <Button style={{backgroundColor:'#C4C4C4', height:40, width:70, alignItems:'center', borderRadius:5}}
                                onPress={this.toggleModalAprove}
                            >
                                <View style={{marginLeft:10, marginRight:0}}>
                                    <Text style={{color:'white', }}>Cancel</Text>
                                </View>
                            </Button>
                        </View>
                    </View>                        
                </View>
            </View>
            </Modal>

            {/* //modal Revise */}
            <Modal
                isVisible={this.state.isModalVisibleRevise}
                onBackdropPress={this.toggleModalRevise}
                onBackButtonPress={this.toggleModalRevise}
            >
                <View style={{ borderRadius:8}}>
                    <View style={{backgroundColor:'#C22323', justifyContent:'center',borderTopRightRadius:8, borderTopLeftRadius:8}}>
                        <View style={{flexDirection:'row', padding:10}}>
                            <Icon
                                name='close'
                                size={20}
                                color='white'
                            />
                                <Text style={{color:'white', marginLeft:5, fontWeight:'bold'}}>Revise</Text>
                        </View>
                    </View>
                    <View style={{backgroundColor:'white', borderBottomRightRadius:8, borderBottomLeftRadius:8}}>                        
                    <View style={{borderBottomWidth:1, borderBottomColor:colors.gray05}}>
                        <View style={{marginVertical:15, marginLeft:15}}>
                            <Text style={{fontSize:18}}>Revise risk register dan risk priority pada unit kerja ini ?</Text>
                        </View>
                    </View>
                    <View style={{marginHorizontal:15}}>
                        <View>
                            <Text style={[{marginTop:10},styles2.fontLabel]}>Komentar</Text>
                        </View>
                        <View>
                            <Textarea
                                style={styles2.textArea}
                                rowSpan={3}
                                bordered
                                placeholderTextColor={colors.gray02}
                                // value={this.state.INCIDENT_DESC}
                                placeholder="Type Something ..."
                                onChangeText={text =>
                                this.setState({ comments: text })
                                }
                            />
                        </View>
                    </View>
                    <View style={{ marginBottom:20, marginTop:5, flexDirection:'row', alignItems:'center'}}>
                    <View style={{flex:3}}></View>
                    <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                        <Button style={{ backgroundColor:'#C22323', height:40, width:70, alignItems:'center', borderRadius:5}}
                            onPress={()=>this.onPressRevise()}
                        >
                            <View style={{flexDirection:'row',marginLeft:6, marginRight:5}}>
                                <View
                                    style={{justifyContent:'center'}}
                                >
                                    <Icon
                                        name='close'
                                        size={15}
                                        color='white'
                                    />
                                </View>
                                    <Text style={{color:'white', textAlign:'center' }}>Revise</Text>
                                </View>
                                    </Button>
                                </View>
                                <View style={{flexDirection:'row', borderRadius:8,  alignItems:'center', marginRight:10}}>
                                    <Button style={{backgroundColor:'#C4C4C4', borderColor:colors.gray05, height:40, width:70, alignItems:'center', borderRadius:5}}
                                        onPress={this.toggleModalRevise}
                                    >
                                <View style={{marginLeft:10, marginRight:0}}>
                                    <Text style={{color:'white', }}>Cancel</Text>
                                </View>
                                    </Button>
                                </View>                
                            </View>
                        </View>
                    </View>
                </Modal>
            </Container>


            
        )
    }
}