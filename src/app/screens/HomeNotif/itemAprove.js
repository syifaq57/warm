import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from '../../library/network/GlobalConfig'
import Modal from "react-native-modal";

import styles from "../../res/styles/Login";
import colors from "../../res/colors";
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import dashstyles from "../../res/styles/Dashboard";
import Sidebar from "../../library/component/Sidebar";
import ButtonMenu from "../Dashboard/ItemDashboard/ButtonMenu";
import SubMenuAllunitDashboard from "../../library/component/SubMenuAllunitDashboard";


//Risk By


export default class itemAprove extends Component {
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            visibleDialogSubmit: false,
            isModalVisible: false,
            visible:true,
            isLoadingList:true,
            isVisibleEmpty:true,

            data_appr:[],
            data_approve:[
                {
                    id_proyek: "012345",
                    divisi : "Building Divison",
                    depart : "Apartemen Yukata"
                },
                {
                    id_proyek: "012345",
                    divisi : "Building Divison",
                    depart : "Apartemen Yukata"
                },
                {
                    id_proyek: "012345",
                    divisi : "Building Divison",
                    depart : "Apartemen Yukata"
                }
            ],
          
        };
    }

    loadDataUser(){
        this.setState({
            isLoading:true
        })
        var url = GlobalConfig.SERVERHOST + 'login/apiLogin';
        var formData = new FormData();
        formData.append("user", this.state.username)
        formData.append("pass", this.state.password)
        console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('resp data',response)
                if (response.success == true) {
                    this.setState({
                        
                        username:response.data.username,
                        karyawan: response.data.karyawan,
                        creator: response.data.creator,
                        bagian_proyek: response.data.bagian_proyek,
                        isLoading:false  
                    },function(){
                        
                        this.loadDataProyek();
                    })
                }else{
                    
                    this.setState({
                        isLoading:false
                    })
                    
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingAll:false
                })
                console.log(error)
            })
    }  
    
    loadDataProyek(){
        var url = GlobalConfig.SERVERHOST + 'api/api/getApproveDivision';
        var formData = new FormData();
        formData.append("username", this.state.username)
        formData.append("karyawan", this.state.karyawan)
        for(var i=0; i<this.state.bagian_proyek.length; i++){
            formData.append("bagian_proyek["+i+"]", this.state.bagian_proyek[i].id_proyek)
        }        
        // formData.append("periode", this.state.tahunSelect)       
        formData.append("creator", this.state.creator)
        console.log('frmdta3', formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log('datarespon',response)
                if (response.success == true) {
                    if(response.data.length == 0){
                        console.log('jmlah data', response.data.length)
                        this.setState({
                            isLoadingList:false,
                            isVisibleEmpty:true,

                        })
                    }else{
                        var data_appr = this.state.data_appr
                        for(var i=0; i<response.data.length; i++){
                            data_appr.push({
                                id_pry:response.data[i].id_bagian_proyek,
                                nama_pry:response.data[i].nama_bagian_proyek
                            })
                        }
                        this.setState({
                            data_appr :data_appr
                        }, function(){
                            console.log('dataappr', this.state.data_appr)
                            this.setState({
                                isLoadingList:false,
                                isVisibleEmpty:false,
                            })
                        })
                    }
                    

                }else{
                    
                    this.setState({
                        isLoadingList:false,
                        
                    })
                    Alert.alert('Error', 'Tidak bisa load data', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error) => {
                
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoadingAll:false
                })
                console.log(error)
        })
    
    }

    componentDidMount(){
        AsyncStorage.getItem('username').then((value)=>{
            this.setState({
                username: value
            })
        })
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password: value
            },function(){
                this.loadDataUser()
                
            })
        })
    }

    navigateToScreen(id_proyek,route){
       AsyncStorage.setItem('id_proyek', id_proyek).then(()=>{
            this.props.navigation.navigate(route);
       })
          
      }

      
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    render(){

        return(
           
            <View>
            {this.state.isLoadingList?(
                <View>
                    <ActivityIndicator/>
                </View>
            ):(
                <View>
                {this.state.isVisibleEmpty?(
                    <View style={{alignItems:'center', marginTop:10}}>
                        <Text>Data Kosong</Text>
                    </View>
                ):(
                    <View>
                    {this.state.data_appr.map((data, index)=>(
                        <Card key={index} style={{marginTop:10, backgroundColor:'rgba(192,192,192,0.3)', borderRadius:10, marginLeft:10, marginRight:10}}>                           
                            <TouchableOpacity
                                onPress={()=>(this.navigateToScreen(data.id_pry,'RiskAprove'))}
                                >
                            <View style={{flexDirection:'row', marginLeft:5}}>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <View style={{padding:10}}>
                                            <View style={{flexDirection:'row', marginBottom:5, marginTop:5}}>
                                                <Text style={{ fontSize:12, color:colors.red}}>	
                                                    Proyek ini perlu di Approve
                                                </Text>
                                            </View>
                                            <View style={{width:"100%", marginBottom:5}}>
                                                <Text style={{fontSize:10}}>Kode proyek</Text>
                                                <Text style={{fontSize:14}}>	
                                                    {data.id_pry}
                                                </Text>
                                                <Text style={{marginTop:5, fontSize:10}}>Nama bagian proyek</Text>
                                                <Text style={{fontSize:14}}>	
                                                    {data.nama_pry}
                                                </Text>
                                            </View>
                                            
                                        </View>
                                    </View>
                                    <View style={{justifyContent:'center', marginRight:20}}>
                                        <View
                                            style={{backgroundColor:'#A2A095', borderRadius:10, paddingHorizontal:10, paddingVertical:5}}
                                        >
                                            <Text style={{fontWeight:'bold'}}>!</Text>
                                        </View>
                                    </View>
                                
                                </View>
                            </TouchableOpacity>
                                
                        </Card>
                    ))}
                    
                    </View>
                )}
                
                </View>
            )}
              
            </View>
        )
    }
}