import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
    Dimensions
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
import Modal from "react-native-modal";
import styles from "../../res/styles/Login";
import colors from "../../res/colors";
import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ListStatus from "./ListStatus"; 

//Risk By


export default class AllUnitDashboard extends Component {
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            visibleDialogSubmit: false,
            isModalVisible: false,
            visible:true,
            
          
        };
    }
    navigateToScreen(route){
        this.setState({
            isModalVisible:false
        })
        this.props.navigation.navigate(route);
        
      }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    render(){

        return(
            <Container style={styles.wrapper}>
                <Header
                    style={{ backgroundColor: '#AFDEF5', marginTop:Platform.OS === "ios" ? 0 : 15,}}
                    androidStatusBarColor={'#68C1EC'}
                    >
                    
                    <View style={{flex:6, flexDirection:"row"}}>
                        <TouchableOpacity
                            style={{ marginLeft:5, justifyContent:"center"}}   
                            onPress={()=>this.props.navigation.goBack()}
                            >
                            <Icon2 
                            
                                name="md-arrow-back"
                                size={25}
                                color={colors.gray09}
                            />
                            
                            
                        </TouchableOpacity>
                        <View style={{flex:1, width:"100%", justifyContent:"center", marginLeft:15}}>
                            <Text style={{color:colors.gray09, fontWeight:"bold",  fontSize:22}}>Risk Status</Text>
                        </View>   
                    </View>
                    <Right style={{flex:1, marginLeft:0, justifyContent:"center", marginRight:5}}>
                        
                        {/* <TouchableOpacity
                            onPress={this.toggleModal}>
                            <Icon
                                    style={{alignSelf:"flex-end"}}
                                    name="apps"
                                    size={30}
                                    color={colors.gray09}
                                />
                        </TouchableOpacity> */}
                        <Modal
                            style={{marginHorizontal:0,marginBottom:0}}
                            isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}
                            onBackButtonPress={this.toggleModal}
                            onSwipeComplete={this.toggleModal}
                            swipeDirection="down"
                        >
                            <View style={{ flex:1, justifyContent:"flex-end", }}>
                                <View style={{backgroundColor:'white', borderTopLeftRadius:5, borderTopRightRadius:5,}}>
                                    <View style={{width:"100%",alignContent:"center", backgroundColor:colors.bgErm, borderTopLeftRadius:4, borderTopRightRadius:4, alignItems:'center', padding:13, borderBottomColor:colors.gray03, borderBottomWidth:1 }}>
                                        <Text style={{fontSize:17, fontWeight:"bold", color:colors.gray09}}>Dashboard</Text>
                                    </View>
                                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                                        <TouchableOpacity onPress={()=>(this.navigateToScreen("AllUnitDashboard"))} style={{flexDirection:"row"}}>
                                            <View style={{padding:10, flexDirection:"row"}}>
                                                <Image
                                                    source={require('../../../app/res/images/chart.png')}
                                                    style={{width:30, height:30, marginRight:10}}
                                                />
                                                <View style={{justifyContent:"center", width:"100%"}}>
                                                    <Text style={{fontWeight:"bold"}}>
                                                        My Dashboard
                                                    </Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                                    <TouchableOpacity onPress={()=>(this.navigateToScreen("RiskRegister"))} style={{flexDirection:"row"}}>
                                            <View style={{padding:10, flexDirection:"row"}}>
                                                <Image
                                                    source={require('../../res/images/corporate.png')}
                                                    style={{width:30, height:30, marginRight:10}}
                                                />
                                                <View style={{justifyContent:"center", width:"100%"}}>
                                                    <Text style={{fontWeight:"bold"}}>
                                                        Risk Register
                                                    </Text>
                                                </View>
                                                
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                
                                </View>
                            
                            </View>
                        </Modal>
                        
                        
                    </Right>
                </Header>
                
                    <View style={{flex:1, flexDirection:'column'}}>
                        <ListStatus/>   
                    </View>
            </Container>
        )
    }
}