import React, { Component } from "react";
import { View, Text, Image,StyleSheet,Dimensions,TouchableOpacity,AsyncStorage } from "react-native";
import {Card,Left,Right,Button} from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from "../../res/colors/index";


const styles = StyleSheet.create({
    layoutText: {
        backgroundColor: 'white',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'red'
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 11,
        color: 'black',
        marginTop: 10 , 
        marginBottom: 5

    },
    textCaption: {
        fontWeight: '400',
        fontSize: 12,
        color: 'black'
    }

})

export default class ItemListHazard extends Component{
    constructor(props){
        super(props);
        this.state={
            status:'-',
        };
    }
    navigateToScreen(idHazard,route){
        AsyncStorage.setItem('id_hazard', idHazard).then(() => { 
          this.props.navigation.navigate(route);
        })
    }

    componentDidMount(){
        this.setState({
            status:this.props.status
        },function(){
            var appr = this.state.status
            if(this.state.status == 'approve1'||'approve2'||'approve3'){
                var apprsplit = appr.replace('approve', 'Approve ')
                this.setState({
                    status:apprsplit
                })
            }
        })
    }

    render(){
        const imageObject = "https://smile.semenindonesia.com"+this.props.img;
        return(
            <TouchableOpacity
                >
                <View style={{marginLeft: 15, marginRight: 15,}}>
                <Card style={{  borderRadius: 8, flex:1,padding:15,borderColor:'white',borderWidth:0}}>
                    <View style={{flex:1,flexDirection:'row', marginLeft:0, marginRight:0}}>
                        <View style={{flex:1,flexDirection:'column', marginTop:5}}>
                        <Text style={{fontSize:10}}>Divisi</Text>
                        <Text style={{marginBottom:10}}>{this.props.divisi}</Text>
                        </View>
                        
                        <Text style={{alignSelf:'center',color:colors.greenDefault,fontWeight:'normal'}}>{this.state.status}</Text>
                        
                    </View>
                    <View style={{flex:1,flexDirection:'column'}}>
                        <Text style={{fontSize:10}}>Kode Departemen</Text>
                        <Text numberOfLines={2} style={{marginBottom:10}}>{this.props.kode_departemen}</Text>
                        <Text style={{fontSize:10}}>Nama Departemen</Text>
                        <Text style={{marginBottom:10}}>{this.props.nama_departement}</Text>                        
                    </View>
          
                </Card>
                </View>
            </TouchableOpacity>
        );
    }
}