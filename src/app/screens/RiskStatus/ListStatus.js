import React, {Component} from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    TouchableOpacity,
    FlatList,
    Keyboard
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Card,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Icon,
    CheckBox
} from "native-base";

import Icon2 from "react-native-vector-icons/MaterialIcons";
import CustomRadioButton from "../../library/component/CustomRadioButton"
import GlobalConfig from '../../../app/library/network/GlobalConfig'
import colors from "../../res/colors/index";
import ItemListStatus from '../../screens/RiskStatus/ItemListStatus';
import { UltimateListView, UltimateRefreshView } from 'react-native-ultimate-listview';



export default class ListHazard extends Component{
    constructor(props){
        super(props);
        this.state = {
            listOrderSort:[
              {NAMA_LIST:'No Dokumen',ID_OPTIONS_LIST:1},
              {NAMA_LIST:'Workgroup',ID_OPTIONS_LIST:3},
              {NAMA_LIST:'Penjelasan',ID_OPTIONS_LIST:8},
              {NAMA_LIST:'Pelapor',ID_OPTIONS_LIST:5}
            ],
              
            listOrderType:[
              {NAMA_LIST:'Ascending',ID_OPTIONS_LIST:'asc'},
              {NAMA_LIST:'Descending',ID_OPTIONS_LIST:'desc'}],
            sortColumn:1,
            sortColumn_NAME:'No Dokumen',
            sortDir:'desc',
            sortDir_NAME:'Descending',
            searchText:'',
            isLoading:false,
            visibleDialogSubmit: false,
            dataSource:[],
            length:10,
            search:'',
        };
    }

    // componentDidMount() {
    //   this._onFocusListener = this.props.navigation.addListener(
    //       "didFocus",
    //       payload => {
    //           AsyncStorage.getItem("SavedHazard").then((value)=>{
    //               if (value=="1"){ 
    //                   this.setState({isLoading:true},function(){this.setState({isLoading:false})})
    //                   console.log('masuk')
    //                   AsyncStorage.setItem("SavedHazard","0");
    //               }
    //           })
    //           this.keyboardDidShowListener = Keyboard.addListener(
    //               'keyboardDidShow',
    //               this._keyboardDidShow,
    //           );
    //           this.keyboardDidHideListener = Keyboard.addListener(
    //               'keyboardDidHide',
    //               this._keyboardDidHide,
    //           );
              
    //       }
    //   );

      

    //   const didBlurSubscription = this.props.navigation.addListener(
    //       'didBlur',
    //       payload => {
    //           this.keyboardDidShowListener.remove();
    //           this.keyboardDidHideListener.remove();
    //       }
    //   );
  
    // }

    // static navigationOptions = {
    //     header: null
    // };

    _keyboardDidShow = () => {
        this.setState({keyboardShow:true})
      // alert('Keyboard Shown');
    }

    _keyboardDidHide = () => {
        this.setState({keyboardShow:false})
        // alert('Keyboard Hidden');
    }

    loadSearch(){
        this.setState({isLoading:true},function(){this.setState({isLoading:false})})
    }
    
    filterData(){
      this.setState({isLoading:true},function(){this.setState({isLoading:false,visibleSort:false})})
    }

    onFetch = async(page = 0, startFetch, abortFetch) => {
        try{
          setTimeout(()=>{
            console.log((page-1)* 10)
            
            var url = GlobalConfig.SERVERHOST + 'api/api/get_status';
            var formData = new FormData();
            formData.append('start',((page-1)*10));
            formData.append("length", this.state.length)
            formData.append("search[value]", this.state.searchText)
            console.log('user tes',formData)
                fetch(url,{
                  method: 'POST',
                  body: formData
                }).then((response) => response.json()).then((responseData) => {
                    console.log('print', responseData.data)
                  startFetch(responseData.data.data, 10);
                }).catch((error)  => {
                  alert("Error Connection"+error);
              }).done(() => {});
            
          },1000)
  
        }catch(err){
          abortFetch();
          console.log(err);
        }
      }

   

      loadRiskStatus(){
        var url = GlobalConfig.SERVERHOST + 'api/api/get_status';
        var formData = new FormData();
        formData.append("start", this.state.start)
        formData.append("length", this.state.length)
        formData.append("search[value]", this.state.search)
        console.log('user tes',formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                console.log('datarespon',response)
                if (response.success == true) {
                    this.setState({
                        tablerisk:response.data.data,
                        
                    }, function(){
                        this.setState({
                            isLoading:false
                        })
                    })
                    
                }else{
                    Alert.alert('Error', 'Tidak bisa load data', [{
                        text: 'Okay'
                    }])
                    this.setState({
                        isLoading:false
                    })
                }
            })
            .catch((error) => {
               
                Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                this.setState({
                    isLoading:false
                })
                console.log(error)
            })
         
    }



      _renderRowView = (item, index, separator) => {
        return(
          <View style={{ backgroundColor: "#FEFEFE", width: "100%" }}>
    
              <ItemListStatus
                navigation={this.props.navigation}
                status={item.status}
                divisi={item.divisi}
                kode_departemen={item.kode_departemen}
                nama_departement={item.nama_departement}

              />
  
          </View>
        );
      };  


    render(){
        list = (
            <UltimateListView
            ref={(ref) => this._listView = ref}
            onFetch={this.onFetch}
            headerView={this.renderHeaderView}
            item={this._renderRowView}
            refreshableTitlePull="Pull To Refresh"
            refreshableMode="basic" //basic | advanced
            
            ></UltimateListView>
           );
        

        return(
            <View style={{ marginLeft: 0, marginRight: 0,height:'100%'  }}>
                <View style={{paddingHorizontal:15,paddingVertical:10, flexDirection:'row',marginTop:5}}>
                    <View style={{flexDirection:'row',flex:1,height:40,backgroundColor:colors.gray06,borderRadius:8,marginRight:10}}>
                        <Input 
                            style={{ fontSize: 11,paddingLeft:15,height:40 }}
                            placeholder="Type something here"
                            value={this.state.searchText}
                            onSubmitEditing={()=>this.loadSearch()}
                            onEndEditing={()=>console.log("canceled")}
                            onBlur={()=>console.log("canceled")}
                            
                            returnKeyType={'search'}
                            onChangeText={text =>
                            this.setState({ searchText: text })
                            }
                        />
                        {this.state.keyboardShow&&(
                            <View style={{ alignSelf:'center', }}>
                                <Icon2
                                    onPress={()=>this.setState({searchText:''})}
                                    name="close"
                                    style={{ fontSize: 15, paddingLeft: 0,alignSelf:'center',marginLeft:5,marginRight:10 }}
                                />
                            </View>
                        )}
                        
                    </View>  
                </View>
                
                
                
                {this.state.isLoading?(
                    <View>
                        <ActivityIndicator/>
                    </View>
                ):(
                     <View style={{ flex: 1, flexDirection: "column" }}>{list}</View>   
                )}
               
            </View>
        )
    }
}