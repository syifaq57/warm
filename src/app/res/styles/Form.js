import { StyleSheet } from "react-native";
import colors from "../colors";

let labelTextSize = 12;
let headingTextSize = 15;

export default {
    fontLabel: {
        marginBottom:5,fontSize:10
    },
    fontTextInput: {
        
        marginBottom: 5,
        paddingLeft:10,
        fontSize: 11,
        borderWidth:1,
        height:35,
        borderRadius:5,
        fontWeight:'bold',
        borderColor:colors.gray05,
        width:'70%'
    },
    textArea:{
        paddingLeft:10,
        marginBottom: 5,
        fontSize: 11,
        borderWidth:1,
        borderRadius:5,
        fontWeight:'bold',
        borderColor:colors.gray05,
        backgroundColor:'white'
    },
    datePicker:{
        dateIcon: {
            position: 'absolute',
            right: 0,
            top: 4,
            marginLeft: 0
        },
        dateInput:{
            borderWidth:0
        },
        dateText:{
            alignSelf:'flex-start',
            marginLeft:10,
            fontSize:11,
            fontWeight:'bold'
        },
        dateTouchBody:{
            borderColor:colors.gray05,
            borderWidth:1,
            borderRadius:5
        },
        placeholderText:{
            alignSelf:'flex-start',
            marginLeft:10,
            fontSize:11,
            fontWeight:'bold',
            color:colors.gray02
        }
        },
        customStyles : {
            stepIndicatorSize: 25,
            currentStepIndicatorSize:30,
            separatorStrokeWidth: 2,
            currentStepStrokeWidth: 3,
            stepStrokeCurrentColor: '#fe7013',
            stepStrokeWidth: 3,
            stepStrokeFinishedColor: '#fe7013',
            stepStrokeUnFinishedColor: '#aaaaaa',
            separatorFinishedColor: '#fe7013',
            separatorUnFinishedColor: '#aaaaaa',
            stepIndicatorFinishedColor: '#fe7013',
            stepIndicatorUnFinishedColor: '#ffffff',
            stepIndicatorCurrentColor: '#ffffff',
            stepIndicatorLabelFontSize: 13,
            currentStepIndicatorLabelFontSize: 13,
            stepIndicatorLabelCurrentColor: '#fe7013',
            stepIndicatorLabelFinishedColor: '#ffffff',
            stepIndicatorLabelUnFinishedColor: '#aaaaaa',
            labelColor: '#999999',
            labelSize: 13,
            currentStepLabelColor: '#fe7013',
        },
        submitButton: {
            marginBottom: 0,
            backgroundColor: '#0DD684',
            borderColor: '#0DD684',
            marginRight: 40,
            borderRadius: 4,
            width: "100%"
        }
};

