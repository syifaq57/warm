import React, { Component } from "react";
import { View, Text, Image,StyleSheet,Dimensions,TouchableOpacity,AsyncStorage,Platform,FlatList,Alert,ActivityIndicator } from "react-native";
import {Card,Left,Right,Button,Textarea,CardItem} from 'native-base'
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from "react-native-vector-icons/MaterialIcons";
import colors from "../../res/colors";
// import { TouchableOpacity } from "react-native-gesture-handler";
import GlobalConfig from "../network/GlobalConfig";


class ListItem extends React.PureComponent {
    render() {
      return (
        <View style={{ backgroundColor: "#FEFEFE" }}>
          <TouchableOpacity
            style={{
              
              justifyContent: "center",
              alignItems: "center"
            }}
            
            onPress={() => this.props.setSelectedLabel(this.props.data)}
            
          >
            <CardItem
              style={{
                borderRadius: 0,
                marginTop: 4,
                backgroundColor: colors.gray,
                paddingBottom:7,
                paddingTop:7,
                // height:30
                
              }}
            >
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                    
                  backgroundColor: colors.gray,
                  
                }}
              >
                <View>
                  <Text style={{ fontSize: 12 }}>
                    {this.props.displayData!=undefined?this.props.data[this.props.displayData]:this.props.data}
                  </Text>
                </View>
              </View>
            </CardItem>
          </TouchableOpacity>
        </View>
      );
    }
  }

export default class SearchableDropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading:false,
            token:'',
            visibleSearchList: false,
            selectedItem:this.props.selected==undefined?this.props.placeholder:this.props.selected,
            searchWord:'',
            data:this.props.data,
            // dataAll:[]
        };
    }

    onClickSearch(){
        if (this.state.visibleSearchList) {
            this.setState({
                visibleSearchList: false
            });
        } else {
            this.setState({
                visibleSearchList: true
            });
        }
        
    }

    componentDidMount() {
        if (this.props.items!=undefined){
            this.setState({
                data:this.props.items,
                dataAll:this.props.items
            })
        }else{
            AsyncStorage.getItem('token').then((value)=>{
                this.setState({token: value})
                this.loadData(value)
            })
        }
        
    }

    loadData(value){
        this.setState({
            isLoading:true
        })
        var url = GlobalConfig.SERVERHOST + this.props.api;
        var formData = new FormData();
        // formData.append("token", value)
        if (this.props.bodyApi!=undefined)
            for (var i=0;i<this.props.bodyApi.length;i++){
                formData.append(this.props.bodyApi[i].name, this.props.bodyApi[i].value)
            }
        // formData.append("password", this.state.password)
        console.log(formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
        
            .then((response) => {
                console.log(response)
                this.setState({data:response,dataAll:response,isLoading:false})
            })
            .catch((error) => {
                this.setState({
                    isLoading:false
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    onChangeText(text){
        this.setState({
            searchWord:text
        })
        if (this.props.displayData!=undefined){
            var displayData=this.props.displayData
            // console.log(displayData)
            // console.log(Object.keys(displayData))
            var newArray = this.state.dataAll.filter((el)=> {
                return  el[displayData].toLowerCase().includes(text.toLowerCase())  // Changed this so a home would match
            });
            // var filter= function(value,name){
            //     return  value[name].toLowerCase().includes(text.toLowerCase())
            // }
        }else{
            var newArray = this.state.dataAll.filter( (el)=> {
                return el.toLowerCase().includes(text.toLowerCase())  // Changed this so a home would match
            });
        }
        
        this.setState({data:newArray})
    }

    onChangeTextSearchApi(text){
        this.setState({
            searchWord:text
        })
        var url = GlobalConfig.SERVERHOST + this.props.api;
        var formData = new FormData();
        formData.append("token", this.state.token)
        formData.append("search", text)
        // formData.append("password", this.state.password)
        console.log(formData)
        fetch(url, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((response) => {
                // console.log(response)
                this.setState({data:response})
            })
            .catch((error) => {
                this.setState({
                    isLoading:false
                })
                Alert.alert('Cannot Load Data', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    }

    setSelectedLabel(data) {
        console.log(data)
        this.setState({
            visibleSearchList:false,
            selectedItem:this.props.displayData!=undefined? data[this.props.displayData]:data
        })
        this.props.selectedMethod(data)
      }

    _renderItem = ({ item }) => (
        <ListItem
          displayData={this.props.displayData}
          data={item}
          setSelectedLabel={text => this.setSelectedLabel(text)}
        />
      );

    render() {
    return (
    <View style={{flex: 1, flexDirection: "column",}}>
        <View style={{ flex: 1, flexDirection: "column",}}>
            <Button
            
            block
            style={{
                justifyContent: "flex-start",
                flex: 1,
                borderWidth: Platform.OS === "ios" ? 1 : 1,
                borderColor:
                Platform.OS === "ios" ? colors.lightGray : colors.gray05,
                paddingLeft: 10,
                backgroundColor: "white",
                height: 50,
                borderRadius:5,
                flexDirection:'row',flex:1,
                shadowOffset: { height: 0, width: 0 },
                shadowOpacity: 0,
                elevation:0
            }}
            onPress={() => this.onClickSearch()}
            >
            
            <Text numberOfLines={1} style={{ fontSize: 11,flex:1,fontWeight:'bold',color:this.state.selectedItem===this.props.placeholder?colors.gray02:"black" }}>
                {this.state.selectedItem}
            </Text>
            <View style={{ alignSelf:'center'}}>
                <Icon
                    name="caret-down"
                    style={{ fontSize: 20, paddingLeft: 0,alignSelf:'center',marginLeft:5,marginRight:15,color:colors.redDefault}}
                />
            </View>
            </Button>
        </View>
        {this.state.visibleSearchList && 
            (
            <View
            style={{
                height: this.props.height!=undefined?this.props.height:300,
                flexDirection: "column",
                borderWidth: 1,
                padding: 10,
                backgroundColor: colors.gray,
                borderBottomLeftRadius:5,borderBottomRightRadius:5,
                borderColor: "#E6E6E6"
            }}
            >{this.state.isLoading?(
                <ActivityIndicator/>
            ):(
            <View>
                {(this.props.visibleSearch!=false&&this.props.visibleSearch==undefined)&&(
                    <Textarea
                value={this.state.searchWord}
                style={{
                    marginLeft: 5,
                    marginRight: 5,
                    fontSize: 11,
                    backgroundColor:'white'
                }}
                bordered
                rowSpan={1.5}
                onChangeText={text => {
                    if (this.props.searchByApi){
                        this.onChangeTextSearchApi(text)
                    }else{
                        this.onChangeText(text)
                    }       
                }}
                placeholder="Ketik kata pencarian ..."
                />
                )}
                
            </View>)}
            <View style={{flex:1,}}
            >
            {!this.state.isLoading&&(
                <FlatList
                nestedScrollEnabled={true}
                data={this.state.data}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                />
            )}
            </View>
        </View>
        )}
      </View>
    );
  }
}
