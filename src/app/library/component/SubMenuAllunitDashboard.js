import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    StyleSheet,
    TouchableOpacity,
    
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Ionicons';
import colors from "../../res/colors/index";
import styles from "../../res/styles/Login";
import dashstyles from "../../res/styles/Dashboard";


export default class SubMenuAllunitDashboard extends Component{
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            isModalVisible:false,
        };
        
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };
    
      navigateToScreen(route){
      
        this.props.navigation.navigate(route);
        
      }

      static navigationOptions = {
        header: null
    };

    render(){
        return(
            <View style={{ flex:1, justifyContent:"flex-end", }}>
                <View style={{backgroundColor:'white', borderTopLeftRadius:5, borderTopRightRadius:5,}}>
                    <View style={{width:"100%",alignContent:"center", backgroundColor:colors.gray09, borderTopLeftRadius:4, borderTopRightRadius:4, alignItems:'center', padding:13, borderBottomColor:colors.gray03, borderBottomWidth:1 }}>
                        <Text style={{fontSize:17, fontWeight:"bold", color:'white'}}>Dashboard</Text>
                    </View>
                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                        <TouchableOpacity onPress={()=>(this.navigateToScreen("AllUnitRiskRegister"))} style={{flexDirection:"row"}}>
                            <View style={{padding:10, flexDirection:"row"}}>
                                <Image
                                    source={require('../../res/images/corporate.png')}
                                    style={{width:30, height:30, marginRight:10}}
                                />
                                <View style={{justifyContent:"center", width:"100%"}}>
                                    <Text style={{fontWeight:"bold"}}>
                                        My Dashboard
                                    </Text>
                                </View>
                                
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{borderBottomColor:colors.gray03, borderBottomWidth:1}}>
                    <TouchableOpacity onPress={()=>(this.navigateToScreen("RiskRegister"))} style={{flexDirection:"row"}}>
                            <View style={{padding:10, flexDirection:"row"}}>
                                <Image
                                    source={require('../../res/images/chart.png')}
                                    style={{width:30, height:30, marginRight:10}}
                                />
                                <View style={{justifyContent:"center", width:"100%"}}>
                                    <Text style={{fontWeight:"bold"}}>
                                        Risk Register
                                    </Text>
                                </View>
                                
                            </View>
                        </TouchableOpacity>
                    </View>
                
                </View>
            
          </View>
            
            
        )
    }
}

