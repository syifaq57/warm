import React, { Component } from "react";
import {
    Text,
    Image,
    StatusBar,
    KeyboardAvoidingView,
    ScrollView,
    View,
    TextInput,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    BackHandler,
    Platform,
    DeviceEventEmitter,
    PushNotificationIOS,
    ImageBackground,
    TouchableOpacity,
} from "react-native";
import {
    Container,
    Header,
    Form,
    Item,
    Label,
    Input,
    Footer,
    Left,
    Right,
    Button,
    Body,
    Title,
    Card,
    CheckBox,
    Content
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton,
} from "react-native-popup-dialog";
// import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Ionicons';
import colors from "../../res/colors/index";
import styles from "../../res/styles/Login";
import dashstyles from "../../res/styles/Dashboard";

export default class Sidebar extends Component{
    constructor(props){
        super(props);
        this.state={
            username: "",
            password: "",
            visibleDialogSubmit: false,
            isModalVisible: false,
            visible:true,
        };
        
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };
    
    render(){
        return(
            <View style={{flex:1, flexDirection:"column"}}>
                <View style={{flex:1, width:"100%", borderBottomColor:colors.gray10, borderBottomWidth:2}}>
                    <View style={dashstyles.profil}>
                        <Icon2
                            name="md-person"
                            size={50}
                            color={colors.bluegray}
                            // backgroundColor={colors.gray10}
                        />
                    </View>
                    <View style={{flex:1, width:"100%", marginTop:10, marginBottom:10}}>
                        <Text style={{fontSize:22, color:"white", alignSelf:"center"}}>Admin</Text>
                    </View>
                </View>
                <View style={{flex:3}}>
                <View style={{borderBottomColor:colors.gray10, borderBottomWidth:1, marginLeft:0, marginRight:0}}>
                        <TouchableOpacity>
                            <View style={{flexDirection:"row", height:40,}}>
                                <View style={{flex:1, marginLeft:15, justifyContent:"center"}}>
                                    <Icon2
                                        name="ios-settings"
                                        size={25}
                                        color={colors.bluegray}
                                    />
                                </View>
                                <View style={{flex:5, width:"100%", justifyContent:"center"}}>
                                    <Text style={{fontSize:15, fontWeight:"bold", color:"white", alignSelf:"flex-start"}}>Account Settings</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{borderBottomColor:colors.gray10, borderBottomWidth:1, marginLeft:0, marginRight:0}}>
                        <TouchableOpacity>
                            <View style={{flexDirection:"row", height:40,}}>
                                <View style={{flex:1, marginLeft:15, justifyContent:"center"}}>
                                    <Icon2
                                        name="md-power"
                                        size={25}
                                        color={colors.red}
                                    />
                                </View>
                                <View style={{flex:5, width:"100%", justifyContent:"center"}}>
                                    <Text style={{fontSize:15, fontWeight:"bold", color:"white", alignSelf:"flex-start"}}>Log out</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}