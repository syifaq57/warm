import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    StatusBar,
    ScrollView,
    FlatList,
    AsyncStorage,
    ActivityIndicator
} from "react-native";
import {
    createAppContainer,
    NavigationActions,
    NavigationAction,    
} from "react-navigation";

import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';


// import {
//     Container,
//     Header,
//     Title,
//     Content,
//     Footer,
//     FooterTab,
//     Text,
//     Button,
//     Left,
//     Right,
//     Body,
//     Item,
//     Card,
//     CardItem,
//     Input,
//     Thumbnail,
//     Fab,
// } from "native-base";
import colors from './src/app/res/colors'
import Icon from 'react-native-vector-icons/FontAwesome';

import SplashScreen from "./src/app/library/component/SplashScreen";
import Login from "./src/app/screens/Login";
import Dashboard from "./src/app/screens/Dashboard/Home/Dashboard";
import AllUnitDashboard from "./src/app/screens/Dashboard/Home/AllUnitDashboard";
import RiskRegister from "./src/app/screens/Dashboard/Home/RiskRegister";
import HomeProfile from "./src/app/screens/Profile/Home/HomeProfile";
import ListAprove from "./src/app/screens/HomeNotif/ListAprove";
import ItemAprove from "./src/app/screens/HomeNotif/itemAprove"
import RiskAprove from "./src/app/screens/HomeNotif/RiskAprove";
import HomeRiskStatus from "./src/app/screens/RiskStatus/HomeRiskStatus";
import ItemSubDetail from './src/app/screens/Dashboard/ItemRiskRegister/ItemSubDetail';
import SwipeRisk from './src/app/screens/Dashboard/ItemRiskRegister/SwipeRisk';
// import HomeHistory from "./src/app/screens/History/Home/HomeHistory";
// import HomeProfile from "./src/app/screens/Profile/Home/HomeProfile";

var TabNav = createBottomTabNavigator(
    {
        Dashboard: {
            screen: Dashboard,
            navigationOptions: {
                title: "Dashboard"
            }
        },
        
        HomeProfile: {
            screen: HomeProfile,
            navigationOptions: {
                title: "Account Setting"
            }
        },
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                let iconType;
                if (routeName === 'Dashboard') {
                    iconName = 'home';
                }
                    else if (routeName === 'HomeProfile') {
                    iconName = 'cog';
                }    
                
                return <Icon name={iconName} size={20} color={tintColor} />;
            },
            headerVisible: false,
        }),
        initialRouteName: "Dashboard",
        tabBarOptions: {
            activeTintColor: colors.gray09,
            inactiveTintColor: 'gray',
            color: colors.gray09,
            showIcon: true,
            showLabel: true
        },
        animationEnabled: false,
        swipeEnabled: true,
        headerMode: 'none',

    }
);



const NavStack = createStackNavigator({

    SplashScreen: {
        screen: SplashScreen
    },
    Login: {
        screen: Login
    },
    TabNav: {
        screen: TabNav
    },
    HomeProfile: {
        screen: HomeProfile
    },
    AllUnitDashboard:{
        screen: AllUnitDashboard
    },
    RiskRegister:{
        screen: RiskRegister
    },
    ListAprove:{
        screen: ListAprove
    },
    ItemAprove:{
        screen: ItemAprove
    },
    RiskAprove:{
        screen: RiskAprove
    },
    HomeRiskStatus:{
        screen: HomeRiskStatus
    },
    SwipeRisk:{
        screen: SwipeRisk
    },
    ItemSubDetail:{
        screen:ItemSubDetail
    },

      
    
},
{
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
});


const DrawerLayout = createAppContainer(NavStack);
export default DrawerLayout;
